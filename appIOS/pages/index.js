var React = require('react-native');
var styles = require('./../styles/index');
var Login = require('./Login');
var Register = require('./Register');
var {
    View,
    Text,
    Image,
    TouchableHighlight
} = React;
import RNShakeEventIOS from 'react-native-shake-event-ios';
var strings = require('./../utils/LocalizedStrings');

var Index = React.createClass({
    createAccount:function () {
      const { navigator } = this.props;
      if(navigator) {
         navigator.push({
             name: 'Register',
             component: Register
         })
      }
    },
    Signin:function () {
      const { navigator } = this.props;
      if(navigator) {
         navigator.push({
             name: 'Login',
             component: Login
         })
      }
    },
    // componentWillMount:function () {
    //   RNShakeEventIOS.addEventListener('shake', () => {
    //     console.log('Device shake!');
    //   });
    // },
    // componentWillUnmount:function () {
    //   RNShakeEventIOS.removeEventListener('shake');
    // },
    render: function () {
        return (
          <View style={styles.container}>
            <Image source={require('image!indexbg')} style={styles.bgImage}>
              <Image source={require('image!logo')} style={styles.logoImage} resizeMode="cover" />
              <View style={styles.subContainer}>
                  <TouchableHighlight onPress={this.createAccount} style={styles.button} underlayColor='#99d9f4'>
                      <Text style={[styles.buttonText,{fontSize:14}]}>{strings.btnCreateAccount}</Text>
                  </TouchableHighlight>
                  <TouchableHighlight onPress={this.Signin} style={styles.signinButton} underlayColor='rgb(161, 163, 165)'>
                      <Text style={[styles.buttonLogin,{fontSize:14}]}>{strings.btnSignIn}</Text>
                  </TouchableHighlight>
              </View>
            </Image>
          </View>
        );
    }
});
module.exports = Index;
