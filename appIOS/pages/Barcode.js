var React = require('react-native');
var styles = require('./../styles/Barcode');
var BarcodeScanner = require('./BarcodeScanner');
var ScrollableTabView = require('react-native-scrollable-tab-view');
var TabBar = require('./Component/TabBar');
var Favorites = require('./Favorites');
var Cart = require('./Cart');
var OrderConfirm = require('./OrderConfirm');
var OrderDetail = require('./OrderDetail');
var HistoryList = require('./HistoryList');
var UserCenter = require('./UserCenter');
var AddressList = require('./AddressList');

var Category = require('./Category');
var Products = require('./../pages/Products');
var MarkerSelection = require('./Component/MarkerSelection');
var ProductsDetail = require('./../pages/ProductsDetail');

// https://github.com/brentvatne/react-native-scrollable-tab-view
import store from './../redux/store';
import { postsGoodsUrl, postsCartInfo, postsFavorites, getPaymentInfo, postPlaymentResult,postsDeleteCartInfo } from './../redux/actions/productsAction';

var {
    View,
    Text,
    Image,
    Animated,
    Easing,
    TextInput,
    Dimensions,
    ScrollView,
    AsyncStorage,
    AlertIOS,
    TouchableHighlight,
    TouchableOpacity,
    NativeModules,
    Platform,
    NetInfo,
    Switch,
    WebView,
    DeviceEventEmitter
} = React;
let { width, height } = Dimensions.get('window');
var Icon = require('react-native-vector-icons/Ionicons');
var strings = require('./../utils/LocalizedStrings');
import ActionSheet from 'react-native-actionsheet';

const buttons = [strings.alertCancel, 'English', '简体中文'];
const CANCEL_INDEX = 0;
const DESTRUCTIVE_INDEX = 3;

var Barcode = React.createClass({
    getInitialState: function () {
        return {
            toolbarTitle: strings.lblTitleTab1,
            imageUrl: require('image!splash'),
            name: '',
            desc: "",
            htmlDesc: "",
            realPrice: 0,
            totalNum: 1,
            cartTotalNum: 0,
            fadeAnim: new Animated.Value(width),
            fadeWebViewAnim: new Animated.Value(width),

            ServerData: {},
            favoritesIcon: "ios-heart-outline",
            automaticFolding: true,
            isScanBar: false,
            txtEdit: strings.btnEdit
        };
    },
    componentDidMount: function() {
      JSON.backIndex = 3
    },
    backLogin: function(){
      const { navigator } = this.props;
      if(navigator) {
         navigator.pop();
      }
    },
    openDetailByFavorites: function(id, automaticFolding, totalNum){
        this.setState({
          automaticFolding: automaticFolding,
          totalNum: totalNum
        });
        var me = this;

        store.dispatch(postsGoodsUrl(ServerURL + "/api/" + id + "/" + window.GLOBAL.user_id + "/")).then((json) =>{
            var productData = store.getState().productsReducer.ServerData;
            var reg = /<[^<>]+>/g;
            if(productData.length == 0){
                AlertIOS.alert(strings.alertPrompt,strings.alertNoProd);
            }else {
                me.setState({
                    imageUrl: {uri: ServerURL.replace("/en","") + productData.image},
                    name: productData.name,
                    desc: productData.description.replace(reg,''),
                    htmlDesc: productData.description,
                    realPrice: productData.price,
                    favoritesIcon : productData.wish_status ? "ios-heart" : "ios-heart-outline",
                    ServerData: productData,
                    isScanBar: false
                });
                Animated.timing(
                   me.state.fadeAnim,
                   {toValue: 0,duration: 500,easing: Easing.linear},
                ).start();
             }
        }).catch((error)=>{
          AlertIOS.alert("提示","对不起，数据请求失败：" + error.message);
        }).done();
    },
    openProductsDetail: function(data){
        var me = this;
        //切换语言，临时解决方案
        var dataURL = data;
        switch (strings.getLanguage()) {
          case "en":
            if(dataURL.includes("/en/") == false){
              dataURL = dataURL.replace(":8000/",":8000/en/");
            }
            break;
          case "zh":
            if(dataURL.includes("/en/") == true){
              dataURL = dataURL.replace(":8000/en/",":8000/");
            }
            break;
          default:
        }
        store.dispatch(postsGoodsUrl(dataURL)).then((json) =>{
            var productData = store.getState().productsReducer.ServerData;
            var reg = /<[^<>]+>/g;
            if(productData.length == 0){
                AlertIOS.alert(strings.alertPrompt,strings.alertNoProd);
            }else {
                me.setState({
                    imageUrl: {uri: ServerURL.replace("/en","") + productData.image},
                    name: productData.name,
                    desc: productData.description.replace(reg,''),
                    htmlDesc: productData.description,
                    realPrice: productData.price,
                    favoritesIcon : productData.wish_status ? "ios-heart" : "ios-heart-outline",
                    ServerData: productData,
                    isScanBar: true,
                });
                Animated.timing(
                   me.state.fadeAnim,
                   {toValue: 0,duration: 500,easing: Easing.linear},
                ).start();
            }


        }).catch((error)=>{
          AlertIOS.alert("提示","对不起，数据请求失败：" + error.message);
        }).done();

    },
    setTabViewPage: function(){
        this.refs.scrollableTabView.goToPage(0);
    },
    changeTab: function (index) {
        window.GLOBAL.tabIndex = index.i;
        switch (index.i) {
          case 0:
            this.setState({toolbarTitle: strings.lblTitleTab1,isShowEdit: false});
            this.refs.products.btnReloadData();
            Animated.timing(
               this.state.fadeAnim,
               {toValue: width,duration: 500},
            ).start();
            Animated.timing(
               this.state.fadeWebViewAnim,
               {toValue: width,duration: 500},
            ).start();
            break;
          case 1:
            this.setState({toolbarTitle: strings.lblTitleTab2,isShowEdit: false});
            window.GLOBAL.isOpenCamera = true;

            if(index.ref == undefined && window.GLOBAL.isOpenCameraView == false){
                window.GLOBAL.isOpenCameraView = true;
                const { navigator } = this.props;
                if(navigator) {
                   navigator.push({
                       name: 'BarcodeScanner',
                       component: BarcodeScanner,
                       params: {
                         openProductsDetail: this.openProductsDetail,
                         setTabViewPage: this.setTabViewPage
                       }
                   })
                }
            }
            Animated.timing(
               this.state.fadeAnim,
               {toValue: width,duration: 500},
            ).start();
            Animated.timing(
               this.state.fadeWebViewAnim,
               {toValue: width,duration: 500},
            ).start();
            break;
          case 2:
            this.setState({toolbarTitle: strings.lblTitleTab3,isShowEdit: false});
            this.refs.favorites.refreshData();
            Animated.timing(
               this.state.fadeAnim,
               {toValue: width,duration: 500},
            ).start();
            Animated.timing(
               this.state.fadeWebViewAnim,
               {toValue: width,duration: 500},
            ).start();
            break;
          case 3:
            this.setState({toolbarTitle: strings.lblTitleTab4,isShowEdit: true});
            this.refs.cart.refreshData(false);
            Animated.timing(
               this.state.fadeAnim,
               {toValue: width,duration: 500},
            ).start();
            Animated.timing(
               this.state.fadeWebViewAnim,
               {toValue: width,duration: 500},
            ).start();
            break;
          case 4:
            this.setState({toolbarTitle: strings.lblTitleTab5,isShowEdit: false});
            Animated.timing(
               this.state.fadeAnim,
               {toValue: width,duration: 500},
            ).start();
            Animated.timing(
               this.state.fadeWebViewAnim,
               {toValue: width,duration: 500},
            ).start();
            break;
          default:
        }
    },
    orderBuyBtn:function(){
      var datas = this.refs.productsDetail.getTypes();
      var me = this;
      if(datas.isType == true){
          var objData = {};
          if(datas.items.length <= 0){
              objData = {
                user: window.GLOBAL.user_id,
                product: this.state.ServerData.id,
                quantity: this.refs.productsDetail.getTotalNum()
              };
          }else{
              objData = {
                user: window.GLOBAL.user_id,
                product: this.state.ServerData.id,
                quantity: this.refs.productsDetail.getTotalNum(),
                attribute: datas.items
              };
          }
          store.dispatch(postsCartInfo(objData)).then((json) =>{
              if(json.ServerData.add_cart_item == true){
                  AlertIOS.alert(strings.alertPrompt,strings.toastAddedToCart);
                  me.refs.cart.refreshData(false);
                  // me.refs.scrollableTabView.goToPage(3);

                  if(this.state.automaticFolding == true){
                    Animated.timing(
                       this.state.fadeAnim,
                       {toValue: width,duration: 500},
                    ).start();
                  }
              }else{
                  AlertIOS.alert(strings.alertPrompt,strings.toastCartFailed);
              }
          }).catch((error)=>{
              AlertIOS.alert("提示","对不起，数据请求失败：" + error.message);
          }).done();
      }else{
          AlertIOS.alert(strings.alertPrompt,strings.toastRelevantType);
      }
    },
    btnBuyNow: function(){
      var realPrice = (this.state.realPrice * this.refs.productsDetail.getTotalNum()).toFixed(2);
      var obj = {
          product_image: this.refs.productsDetail.getProductImage(),
          name: this.refs.productsDetail.getName(),
          price: this.state.realPrice,
          quantity: this.refs.productsDetail.getTotalNum()
      };
      this.commodityClearing(realPrice,this.refs.productsDetail.getTotalNum().toString(),obj,this,true)
    },
    backBarcode: function(){
      window.GLOBAL.isOpenCamera = true;
      Animated.timing(
         this.state.fadeAnim,
         {toValue: width,duration: 500},
      ).start();
      if(this.state.isScanBar == true){
         this.setTabViewPage();
      }
    },
    backProductDesc: function(){
      Animated.timing(
         this.state.fadeWebViewAnim,
         {toValue: width,duration: 500},
      ).start();
    },
    favoritesInfo:function(){
      var me = this;
      var obj = {
        user_id: window.GLOBAL.user_id,
        product_id: this.state.ServerData.id,
        follow: !this.state.ServerData.wish_status
      }
      this.state.ServerData.wish_status = !this.state.ServerData.wish_status;

      store.dispatch(postsFavorites(obj)).then((json) =>{
          if(json.ServerData.add_status == "successful"){
              this.setState({
                favoritesIcon: "ios-heart"
              });
              me.refs.favorites.refreshData();
              AlertIOS.alert(strings.alertPrompt,strings.toastFavoriteSuccess);
          }else if(json.ServerData.add_status == "delete"){
              this.setState({
                favoritesIcon: "ios-heart-outline"
              });
              me.refs.favorites.refreshData();
              AlertIOS.alert(strings.alertPrompt,strings.toastCancelFavorite);
          }
      }).catch((error)=>{
          AlertIOS.alert("提示","对不起，数据请求失败：" + error.message);
      }).done();
    },
    commodityClearing: function(grandTotal,totalNum,pIds,obj,isBuyNow){
      var paramsObj = {
        totalNum: totalNum,
        grandTotal: grandTotal,
        obj: obj,
        isBuyNow: isBuyNow,
        pIds: pIds
      };
      fnNavigatorPage(this,'OrderConfirm',OrderConfirm,paramsObj);
    },
    orderDetail: function(orderObj){
      var paramsObj = {
        orderObj: orderObj
      };
      fnNavigatorPage(this,'OrderDetail',OrderDetail,paramsObj);
    },
    openCategoryDetail: function(id){
      this.openDetailByFavorites(id,true,1);
    },
    wechartPaymentTest: function(){
      //注册 APP
      // NativeModules.WeChatAndroid.registerApp("sdf",(err,registerOK) => {
      //   debugger;
      // });
      //打开微信
      // NativeModules.WeChatAndroid.openWXApp((err,res) => {
      //   debugger;
      // });
      //是否安装微信
      // NativeModules.WeChatAndroid.isWXAppInstalled((err,isInstalled) => {
      //   debugger;
      // });
      //是否支持微信 API
      // NativeModules.WeChatAndroid.isWXAppSupportAPI((err,isSupport) => {
      //   debugger;
      // });
      //获取支持的 API
      // NativeModules.WeChatAndroid.getWXAppSupportAPI((err,supportAPI) => {
      //   debugger;
      // });
      //认证登录，返回
      // NativeModules.WeChatAndroid.sendAuthReq(null,null,(err,authReqOK) => {
      //   debugger;
      //   // 处理登录回调结果
      //   DeviceEventEmitter.addListener('finishedAuth',function(event){
      //       debugger;
      //       var success = event.success;
      //       if(success){
      //        AlertIOS.alert(
      //         ' code = ' + JSON.stringify(event.code) +
      //         ' state = ' + JSON.stringify(event.state)
      //
      //        );
      //       }else{
      //        AlertIOS.alert('授权失败',AlertIOS.alert.SHORT);
      //       }
      //   });
      // });

      // var payOptions = {
      //   appId: 'wxd930ea5d5a258f4f',
      //   nonceStr: '5K8264ILTKCH16CQ2502SI8ZNMTM67VS',
      //   packageValue: 'Sign=WXPay',
      //   partnerId: '1900000109',
      //   prepayId: 'WX1217752501201407033233368018',
      //   timeStamp: '1412000000',
      //   sign: 'C380BEC2BFD727A4B6845133519F3AD6',
      // };
      // NativeModules.WeChatAndroid.registerApp("wxd930ea5d5a258f4f",(err,registerOK) => {
      //     NativeModules.WeChatAndroid.weChatPay(payOptions,(err,sendReqOK) => {
      //       debugger;
      //       DeviceEventEmitter.emit('finishedPay',sendReqOK);
      //     });
      // });
      //
      //
      // //  处理支付回调结果
      // DeviceEventEmitter.addListener('finishedPay',function(event){
      //    var success = event.success;
      //    if(success){
      //     // 在此发起网络请求由服务器验证是否真正支付成功，然后做出相应的处理
      //    }else{
      //     AlertIOS.alert("提示",'支付失败');
      //    }
      // });
    },
    setLanguage: function(val){
      if(val == 1){
          strings.setLanguage('en');
          if(this.state.txtEdit == '编 辑'){
              this.setState({
                txtEdit: strings.btnEdit
              },...this.state);
          }else{
              this.setState({
                txtEdit: strings.btnFinish
              },...this.state);
          }
          ServerURL = i18nIndex[1];
      }else if(val == 2){
          strings.setLanguage('zh');
          if(this.state.txtEdit == 'Edit'){
              this.setState({
                txtEdit: strings.btnEdit
              },...this.state);
          }else{
              this.setState({
                txtEdit: strings.btnFinish
              },...this.state);
          }
          ServerURL = i18nIndex[0];
      }else{
        return;
      }
      if(window.GLOBAL.tabIndex == undefined){window.GLOBAL.tabIndex = 0};
      switch (window.GLOBAL.tabIndex) {
        case 0:
          this.setState({toolbarTitle: strings.lblTitleTab1});
          this.refs.products.btnReloadData();
          break;
        case 1:
          this.setState({toolbarTitle: strings.lblTitleTab2});
          break;
        case 2:
          this.setState({toolbarTitle: strings.lblTitleTab3});
          this.refs.favorites.refreshData();
          break;
        case 3:
          this.setState({toolbarTitle: strings.lblTitleTab4});
          this.refs.cart.refreshData(false);
          break;
        case 4:
          this.setState({toolbarTitle: strings.lblTitleTab5});
          break;
        default:
      }
    },
    openLanguageMenu:function(){
      this.ActionSheet.show();
    },
    renderDrawer: function () {
        return (
            <DrawerLayout backLogin={this.backLogin}/>
        );
    },
    showWebViewDesc:function(){
      Animated.timing(
         this.state.fadeWebViewAnim,
         {toValue: 0,duration: 500,easing: Easing.linear},
      ).start();
    },
    setCartTotalNum: function(totalNum){
      this.setState({
        cartTotalNum: totalNum
      })
    },
    btnEditClick: function(){
        if(this.state.txtEdit == strings.btnEdit){
            this.setState({txtEdit: strings.btnFinish});
        }else{
            this.setState({txtEdit: strings.btnEdit});
        }
    },
    deleteCartItems: function(totalNum,pIds){
      if(totalNum <= 0){ AlertIOS.alert(strings.alertPrompt,strings.toastCartEmpty); return;}

      AlertIOS.alert(
          strings.alertPrompt,
          strings.formatString(strings.alertDeleteCartItem,totalNum),
          [
            {text: strings.alertCancel},
            {text: strings.alertOk, onPress: () => {
              var objs = {pIds: pIds};

              store.dispatch(postsDeleteCartInfo(objs)).then((json) =>{
                  if(json.ServerData.cart_item_delete == true){
                      AlertIOS.alert(strings.alertPrompt,strings.toastDeletedSuccess);
                      this.refs.cart.refreshData(true);
                  }else{
                      AlertIOS.alert(strings.alertPrompt,strings.toastDeletedFailed);
                  }
              }).catch((error)=>{
                  AlertIOS.alert(strings.alertPrompt,"对不起，数据请求失败：" + error.message);
              }).done();
            }},
          ]
      );
    },
    onAddressManager: function(){
        const { navigator } = this.props;
        if(navigator) {
           navigator.push({
               name: 'AddressList',
               component: AddressList,
           });
        }
    },
    onShowHistoryList: function(){
        const { navigator } = this.props;
        if(navigator) {
           navigator.push({
               name: 'HistoryList',
               component: HistoryList,
           });
        }
    },
    openScanCode: function(){
        this.refs.scrollableTabView.goToPage(1);
    },
    render: function () {
        var btnEdit = null;
        if(this.state.isShowEdit == true){
            btnEdit =
              <View style={styles.viewBtnEditLayout}>
                <TouchableOpacity onPress={this.btnEditClick}>
                    <View style={styles.viewBtnEdit}>
                        <Text style={styles.txtBtnEdit}>{this.state.txtEdit}</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.openScanCode} style={{marginLeft: 8,marginTop:-3}}>
                    <Image source={require('image!scancode')} style={{width:30,height:30}} />
                </TouchableOpacity>
              </View>
        }else{
            btnEdit =
            <View style={styles.viewBtnEditLayout}>
              <TouchableOpacity onPress={this.openScanCode} style={{marginTop:-3}}>
                  <Image source={require('image!scancode')} style={{width:30,height:30}} />
              </TouchableOpacity>
            </View>
        }
        return (
            <View style={{flex:1}}>
                <View style={styles.viewToolbar}>
                  <Text style={styles.txtBGToolbar}>{this.state.toolbarTitle}</Text>
                  {btnEdit}
                </View>
                <ScrollableTabView ref="scrollableTabView" initialPage={0} style={styles.scrollableTabView} onChangeTab={this.changeTab} tabBarPosition="bottom" renderTabBar={() => <TabBar cartTotalNum={this.state.cartTotalNum}/>}>
                    <View tabLabel={strings.tab1 + "|ios-home"} style={styles.tabView}>
                        <Products ref="products" openCategoryDetail={this.openCategoryDetail}/>
                    </View>
                    <View tabLabel={strings.tab2 + "|navicon-round"} style={styles.tabView}>
                        <Category ref="category" openProductsDetail={this.openProductsDetail}/>
                    </View>
                    <ScrollView tabLabel={strings.tab3 + "|ios-heart-outline"} style={styles.tabView}>
                        <Favorites ref="favorites" openDetailByFavorites={this.openDetailByFavorites}/>
                    </ScrollView>
                    <View tabLabel={strings.tab4 + "|bag"} style={styles.tabView}>
                        <Cart ref="cart" deleteCartItems={this.deleteCartItems} isDeleteStatus={this.state.txtEdit} commodityClearing={this.commodityClearing} openDetailByFavorites={this.openDetailByFavorites} setCartTotalNum={this.setCartTotalNum}/>
                    </View>
                    <UserCenter ref="userCenter" openLanguageMenu={this.openLanguageMenu} backLogin={this.backLogin} onShowHistoryList={this.onShowHistoryList} onAddressManager={this.onAddressManager} tabLabel={strings.tab5 + "|person"}/>
                </ScrollableTabView>

                <Animated.View style={[styles.floatView,{left: this.state.fadeAnim}]}>
                    <ProductsDetail ref="productsDetail" ServerData={this.state.ServerData} orderBuyBtn={this.orderBuyBtn} btnBuyNow={this.btnBuyNow} showWebViewDesc={this.showWebViewDesc} backBarcode={this.backBarcode} favoritesInfo={this.favoritesInfo} imageUrl={this.state.imageUrl} desc={this.state.desc} favoritesIcon={this.state.favoritesIcon}/>
                </Animated.View>
                <Animated.View style={[styles.floatWebView,{left: this.state.fadeWebViewAnim}]}>
                    <View style={styles.viewToolbarIcon}>
                      <TouchableOpacity onPress={this.backProductDesc} style={[styles.greenButton,{marginTop:20,position:'absolute'}]} activeOpacity={0.3}>
                          <Image source={require('image!leftarrow1')} />
                      </TouchableOpacity>
                      <Text style={{marginTop:30,marginBottom:15,alignSelf:'center'}} numberOfLines={strings.titleDetailLength}>{this.state.name.substr(0,18)}</Text>
                    </View>
                    <WebView
                      automaticallyAdjustContentInsets={false}
                      contentInset={{top: 0, right: 0, bottom: 0, left: 0}}
                      source={{html: this.state.htmlDesc}}
                      opaque={false}
                      javaScriptEnabled={true} />
                </Animated.View>

                <ActionSheet
                    ref={(o) => this.ActionSheet = o}
                    title={strings.titleLanguage}
                    options={buttons}
                    cancelButtonIndex={CANCEL_INDEX}
                    destructiveButtonIndex={DESTRUCTIVE_INDEX}
                    onPress={(val)=>{this.setLanguage(val)}}
                />
            </View>
        );
    }
});

module.exports = Barcode;
