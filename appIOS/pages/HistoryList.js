'use strict';
var React = require('react-native');
var styles = require('./../styles/HistoryList');
var {
    View,
    Text,
    Image,
    ScrollView,
    AlertIOS,
    NetInfo,
    Animated,
    RefreshControl,
    Dimensions,
    NativeModules,
    TouchableHighlight,
    TouchableOpacity
} = React;
import store from './../redux/store';
import { getOrderList,postsGoodsUrl,getPaymentInfo,postPlaymentResult } from './../redux/actions/productsAction';
let { width, height } = Dimensions.get('window');
var strings = require('./../utils/LocalizedStrings');
var Icon = require('react-native-vector-icons/Ionicons');
var OrderComment = require('./../pages/OrderComment');
var OrderDetail = require('./../pages/OrderDetail');

const Row = React.createClass({
  btnGotoSubmit: function(){
    const { navigator } = this.props;
    if(navigator) {
       navigator.push({
         name: 'OrderComment',
         component: OrderComment
       })
    }
  },
  btnGotoPlayment: function(id,btnPayment){
    store.dispatch(postsGoodsUrl('http://120.25.72.158:8000/orders/get-api-detail/' + id + '/')).then((json) =>{
        if(json.ServerData.length >= 1){
          btnPayment(json.ServerData[0]);
        }
    }).catch((error)=>{
        AlertIOS.alert("提示","对不起，数据请求失败：" + error.message);
    }).done();
  },
  render: function() {
    if(this.props.data.paid == true){
        return (
         <TouchableHighlight onPress={()=>{this.props.orderDetail(this.props.data)}} underlayColor='#F7F7F7'>
            <View style={styles.row}>
              <View style={styles.viewContent}>
                  <Text style={styles.text}>
                    {strings.lblOrderId + ' ID: ' + this.props.data.ID}
                  </Text>
                  <Text style={styles.text}>
                    {this.props.data.date}
                  </Text>
                  <Text style={styles.text}>
                    {this.props.data.money}
                  </Text>
                  <View style={styles.viewProcessStatus}>
                    <Text style={styles.processStatus}>{this.props.data.status}</Text>
                  </View>
                  <TouchableHighlight onPress={()=>{this.props.btnGotoComment(this.props.data.id)}} style={[styles.btnProcessStatus,{backgroundColor: '#A6B0F3'}]} underlayColor='#909CEF'>
                      <Text style={styles.processStatus}>{strings.lblComment}</Text>
                  </TouchableHighlight>
              </View>
            </View>
          </TouchableHighlight>
        );
    }else {
        return (
         <TouchableHighlight onPress={()=>{this.props.orderDetail(this.props.data)}} underlayColor='#F7F7F7'>
            <View style={styles.row}>
              <View style={styles.viewContent}>
                  <Text style={styles.text}>
                    {strings.lblOrderId + ' ID: ' + this.props.data.ID}
                  </Text>
                  <Text style={styles.text}>
                    {this.props.data.date}
                  </Text>
                  <Text style={styles.text}>
                    {this.props.data.money}
                  </Text>
                  <View style={styles.viewProcessStatus}>
                    <Text style={styles.processStatus}>{this.props.data.status}</Text>
                  </View>
                  <TouchableHighlight onPress={()=>{this.btnGotoPlayment(this.props.data.ID,this.props.btnPayment)}} style={[styles.btnProcessStatus,{backgroundColor: '#baa071'}]} underlayColor='#909CEF'>
                      <Text style={[styles.processStatus,{color: 'white'}]}>{strings.lblToPay}</Text>
                  </TouchableHighlight>
              </View>
            </View>
          </TouchableHighlight>
        );
    }

  },
});
var HistoryList = React.createClass({
    getInitialState: function () {
      return {
        isRefreshing: false,
        isTip: false,
        loaded: 0,
        rowData: Array.from(new Array(0)),
        orderInfo:{
          id: '',
          postal_code: '',
          email: '',
          address: '',
          count: 0,
          totalPrice: '0.00'
        }
      }
    },
    componentWillMount:function() {
      this.setState({isTip: false});
      var me = this;
      store.dispatch(getOrderList(window.GLOBAL.user_id)).then((json) =>{
        var arrays = [];
        for (var i = 0; i < json.ServerData.length; i++) {
            var dateTime = json.ServerData[i].updated.replace("T"," ");
            dateTime = dateTime.substr(0,dateTime.indexOf('.')); var price = 0;

            for (var j = 0; j < json.ServerData[i].item.length; j++) {
                price += parseFloat(json.ServerData[i].item[j].price);
            }

            var obj = {
              id: json.ServerData[i].id,
              dateTime: dateTime,
              price: price,
              paid: json.ServerData[i].paid,
              status: (json.ServerData[i].paid == true) ? strings.lblPaid : strings.lblUnPaid,
              data: json.ServerData[i]
            }
            arrays.push(obj);
        }
        me.refreshData(arrays);
      }).catch((error)=>{
          AlertIOS.alert("提示","对不起，数据请求失败：" + error.message);
      }).done();
    },
    btnLaunchPayment: function(){
      NetInfo.isConnected.fetch().then(isConnected => {
          if(isConnected == true){
              var me = this;
              store.dispatch(getPaymentInfo('http://120.25.72.158:8369/payHunter-1.0-SNAPSHOT/aliPay')).then((json) =>{
                  // var dataState = store.getState();
                  debugger;
                  NativeModules.AlipayRequestConfig.pay(json.ServerData.data,this.paymentCallback);
                  // this.paymentCallback({resultStatus: 9000,memo: '处理成功',result: '{partner="2088221420923333"&seller_id="creativestar@cssinco.cn"&out_trade_no="20160329150156"&subject="支付宝支付"&body="用于测试支付宝快捷支付测试！"&total_fee="0.01"&notify_url="http://218.77.183.189:8090/merry/notify_url.jsp"&service="mobile.securitypay.pay"&payment_type="1"&_input_charset="utf-8"&it_b_pay="1d"&return_url="http://www.tianjiandao.com"&success="true"&sign_type="RSA"&sign="ecPYpWTTcqy7Ydsv+rRq2KRAoPMKPZyV4zljvw0rxebV5RMRybAnNaaPRqCq5lltvtoJ8sDDePU2uuUMxoKXkPaP32mikLojxPBeVP6PVSEt4WNHipRz2f5u0Izim6Caxk5J4Nh51/jK6bKGZNPBWLNQL/VJHwSkyPlBIbHStAY="}'});
              }).catch((error)=>{
                  AlertIOS.alert("提示","对不起，数据请求失败：" + error.message);
              }).done();
          }else{
              AlertIOS.alert("提示","对不起，当前无法连接网络（offline）");
          }
      });
    },
    paymentCallback: function(result){
        console.log(result);
        debugger;

        if(result.resultStatus == 9000){
            var strResult = {
              id: this.state.orderInfo.id,
              paid: 1
            };
            var me = this;
            store.dispatch(postPlaymentResult(ServerURL + '/orders/order-paid-api/',strResult)).then((json) =>{
                if(json.ServerData.change_status == true){
                    AlertIOS.alert(strings.alertPrompt,strings.lblSuccessfulpayment);
                    me.btnReloadData();
                }else{
                    AlertIOS.alert(strings.alertPrompt,strings.lblSystemBusy);
                }
            }).catch((error)=>{
                AlertIOS.alert("提示","对不起，数据请求失败：" + error.message);
            }).done();
        }else if(result.resultStatus == 6002){
            AlertIOS.alert(strings.alertPrompt,strings.lblNetworkError);
        }else if(result.resultStatus == 6001){
            AlertIOS.alert(strings.alertPrompt,strings.lblUserCanceled);
        }else{
            AlertIOS.alert(strings.alertPrompt,strings.lblSystemSorry + result.memo + "。");
        }
    },
    btnPayment: function(data){
      var count = 0; var totalPrice = "";
      for (var i = 0; i < data.item.length; i++) {
        count += data.item[i].quantity;
        totalPrice += data.item[i].price;
      }
      this.setState({
        orderInfo: {
          id: data.id,
          postal_code: data.postal_code,
          email: data.email,
          address: data.address,
          count: count,
          totalPrice: totalPrice
        }
      });
      this.btnLaunchPayment();
    },
    btnReloadData: function(){
      this.setState({isTip: true});

      var me = this;
      store.dispatch(getOrderList(window.GLOBAL.user_id)).then((json) =>{
        var arrays = [];
        for (var i = 0; i < json.ServerData.length; i++) {
            var dateTime = json.ServerData[i].updated.replace("T"," ");
            dateTime = dateTime.substr(0,dateTime.indexOf('.')); var price = 0;

            for (var j = 0; j < json.ServerData[i].item.length; j++) {
                price += parseFloat(json.ServerData[i].item[j].price);
            }

            var obj = {
              id: json.ServerData[i].id,
              dateTime: dateTime,
              price: price,
              paid: json.ServerData[i].paid,
              status: (json.ServerData[i].paid == true) ? strings.lblPaid : strings.lblUnPaid,
              data: json.ServerData[i]
            }
            arrays.push(obj);
        }
        me.refreshData(arrays);
      }).catch((error)=>{
          AlertIOS.alert("提示","对不起，数据请求失败：" + error.message);
      }).done();
    },
    refreshData: function(arrays){
      if(arrays.length <= 0){
        if (this.state.isTip == true){
          AlertIOS.alert(strings.alertPrompt,strings.lblEmptyData);
        }
      }else{
        this.setState({
          rowData: Array.from(arrays).map(
            (val, i) => ({
                ID: val.id,
                date: val.dateTime,
                money: '$ ' + val.price,
                paid: val.paid,
                status: val.status,
                data: val.data
            })
          )
        });
      }
    },
    backPage: function(){
      const { navigator } = this.props;
      if(navigator) {
         navigator.pop();
      }
    },
    _onRefresh() {
      this.setState({isRefreshing: true,isTip: true});
      var me = this;

      setTimeout(() => {
        me.btnReloadData();

        me.setState({
          // loaded: this.state.loaded + 1,
          isRefreshing: false
        });
      }, 2500);
    },
    btnGotoComment: function(orderId){
        var paramsObj = {
            orderObj: orderId
        };
        fnNavigatorPage(this,'OrderComment',OrderComment,paramsObj);
    },
    orderDetail: function(orderObj){
        var paramsObj = {
            orderObj: orderObj
        };
        fnNavigatorPage(this,'OrderDetail',OrderDetail,paramsObj);
    },
    render: function () {
      const rows = this.state.rowData.map((row, index) => {
        return <Row key={index} data={row} btnGotoComment={this.btnGotoComment} btnPayment={this.btnPayment} orderDetail={this.orderDetail} btnGotoSubmit={this.props.btnGotoSubmit} btnGotoPlayment={this.props.btnGotoPlayment}/>;
      });
      return(
        <View style={styles.OrderConfirm}>
            <View style={{height: 55,borderColor:'#E0E0E0',borderBottomWidth:1}}>
            <TouchableOpacity onPress={this.backPage} style={[styles.greenButton,{margin:0,marginTop:25,position:'absolute'}]} activeOpacity={0.3}>
                    <Image source={require('image!leftarrow1')} />
            </TouchableOpacity>
              <Text style={{marginTop:30,marginBottom:15,alignSelf:'center'}}>{strings.tab5}</Text>
            </View>
            <ScrollView
              style={styles.scrollview}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.isRefreshing}
                  onRefresh={this._onRefresh}
                  tintColor="#ff0000"
                  title={strings.historyListLoading}
                  colors={['#62ACEC', '#BDC730', '#D86565']}
                  progressBackgroundColor="rgb(250,250,250)"
                />
              }>
              {rows}
            </ScrollView>
        </View>
      )
    }
});
module.exports = HistoryList;
