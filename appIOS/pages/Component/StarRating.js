'use strict';

var React = require('react-native');

var {
  StyleSheet,
  View
} = React;

var FontAwesome = require('react-native-vector-icons/FontAwesome');
var Button = require('react-native-button');

var StarRating = React.createClass({
  propTypes: {
    disabled: React.PropTypes.bool,
    maxStars: React.PropTypes.number,
    rating: React.PropTypes.number.isRequired,
    selectedStar: React.PropTypes.func,
    style: View.propTypes.style,
    starSize: React.PropTypes.number,
    starColor: React.PropTypes.string,
  },
  getDefaultProps: function() {
    return {
      disabled: false,
      maxStars: 5,
      rating: 0,
      starColor: 'black',
    };
  },
  getInitialState: function () {
    // Round rating down to nearest .5 star
    return {
      maxStars: this.props.maxStars,
      isDecimal: 1
    };
  },
  componentWillMount:function (){
    if (this.props.rating.toString().includes('.') == false){
      this.setState({isDecimal: 1});
    }else if (this.props.rating.toString().includes('.') == true){
      this.setState({isDecimal: 0.5});
    }
  },
  pressStarButton: function (rating) {
    if (rating.toString().includes('.') == false){
      rating = rating - 0.5;
      this.props.selectedStar(rating);
      if (!this.props.disabled) {
        this.setState({
          rating: rating,
          isDecimal: 0.5
        });
      }
    }else if (rating.toString().includes('.') == true){
      rating = rating + 0.5;
      this.props.selectedStar(rating);
      if (!this.props.disabled) {
        this.setState({
          rating: rating,
          isDecimal: 1
        });
      }
    }
  },
  getValue: function(){
      return this.state.rating;
  },
  render: function () {
    var starsLeft = this.props.rating;
    var starButtons = [];
    for (var i = 0; i < this.state.maxStars; i++) {
      var starIcon = 'star-o';
      if (starsLeft >= 1) {
        starIcon = 'star';
      } else if (starsLeft === 0.5) {
        starIcon = 'star-half-o';
      }
      starButtons.push(
        <Button
          activeOpacity={0.60}
          disabled={this.props.disabled}
          key={i + this.state.isDecimal}
          onPress={this.pressStarButton.bind(this, (i + this.state.isDecimal))}
          style={{
            height: this.props.starSize,
            width: this.props.starSize,
          }}
        >
          <FontAwesome
            name={starIcon}
            size={this.props.starSize}
            color={this.props.starColor}
            style={{
              height: this.props.starSize,
              width: this.props.starSize,
            }}
          />
        </Button>
      );
      starsLeft--;
    }
    return (
      <View style={styles.starRatingContainer}>
        {starButtons}
      </View>
    );
  },
});

var styles = StyleSheet.create({
  starRatingContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  starButton: {

  },
  star: {

  },
});

module.exports = StarRating;
