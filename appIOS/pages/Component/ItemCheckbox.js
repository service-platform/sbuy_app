'use strict';

var React = require('react-native');
var FontAwesome = require('react-native-vector-icons/FontAwesome');

var {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
} = React;

var ItemCheckbox = React.createClass({
  propTypes: {
    onCheck: React.PropTypes.func,
    icon: React.PropTypes.string,
    size: React.PropTypes.number,
    backgroundColor: React.PropTypes.string,
    color: React.PropTypes.string,
    iconSize: React.PropTypes.string,
    checked: React.PropTypes.bool,
    style: React.PropTypes.object,
    data: React.PropTypes.string
  },

  getDefaultProps: function() {
    return {
      onCheck: null,
      icon: "check",
      size: 30,
      backgroundColor: 'white',
      color: '#4C63BD',
      iconSize: 'normal,',
      checked: false,
      style: null,
      data: ''
    };
  },

  getInitialState: function () {
    return {
      checked: false,
      bg_color: this.props.backgroundColor,
    };
  },

  getIconSize: function() {
    if (this.props.iconSize == 'small') {
      return this.props.size * 0.5;
    } else if (this.props.iconSize == 'normal') {
      return this.props.size * 0.6;
    } else {
      return this.props.size * 0.7;
    }
  },

  completeProgress: function() {
    if (this.state.checked) {
      this.setState({
          checked: false,
          bg_color: this.props.backgroundColor,
      });
      if (this.props.onCheck) {
          this.props.onCheck(false,this.props.data);
      }
    } else {
      this.setState({
          checked: true,
          bg_color: this.props.color,
      });
      if (this.props.onCheck) {
          this.props.onCheck(true,this.props.data);
      }
    }
  },

  componentDidMount: function() {
    if (this.props.checked) {
      this.completeProgress();
    }
  },
  getValue: function() {
    return this.state.checked;
  },
  setValue: function(value) {
    if (value == true) {
        this.setState({
            checked: true,
            bg_color: this.props.color,
        });
    } else if (value == false) {
        this.setState({
            checked: false,
            bg_color: this.props.backgroundColor,
        });
    }
  },

  render: function() {
    return(
      <View style={this.props.style}>
        <TouchableOpacity onPress={this.completeProgress}>
          <View style={[styles.getCircleCheckStyle,{width: this.props.size,height: this.props.size,backgroundColor: this.state.bg_color,borderColor: this.props.color,borderRadius: (this.props.size / 2)}]}>
              <FontAwesome
                name={this.props.icon}
                size={this.getIconSize()-2}
                style={[styles.getCircleIconStyle,{color: this.props.backgroundColor,width: this.getIconSize()-3,height: this.getIconSize()-2}]}
              />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
});

var styles = StyleSheet.create({
  getCircleCheckStyle: {
    width: 20,
    height: 20,
    backgroundColor: 'transparent',
    borderColor: 'gray',
    borderWidth: 2,
    borderRadius: 20/2,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 3,
    paddingLeft: 3,
    paddingRight: 3
  },
  getCircleIconStyle: {
    color: 'white',
    flex: 1,
    width: 20,
    height: 20,
  },
});
module.exports = ItemCheckbox;
