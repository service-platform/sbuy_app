var React = require('react-native');
var styles = require('./../styles/Barcode');
var MarkerSelection = require('./Component/MarkerSelection');
var Icon = require('react-native-vector-icons/Ionicons');
import WeChat from 'react-native-wechat-ios';
var {
    Text,
    View,
    Image,
    Alert,
    AlertIOS,
    ScrollView,
    ToolbarAndroid,
    TouchableOpacity,
    TouchableHighlight,
    NativeAppEventEmitter
} = React;
var SetNumber = require('./Component/SetNumber');
var strings = require('./../utils/LocalizedStrings');
var returnTypes = {
    isType: false,
    items: []
};

var ProductsDetail = React.createClass({
    getInitialState: function () {
        return {}
    },
    getTypes: function(){
        if(returnTypes.isType == true){
            return returnTypes;
        }else{
            var isType = true; var index = 0;
            for (var item in this.props.ServerData.attributes) {
                if(this.refs['type_'+ index].getValue() == ""){
                    isType = false; break;
                }
                index++;
            }

            if(isType == true){
                var index = 0; var datas = [];
                for (var item in this.props.ServerData.attributes) {
                    datas.push(this.refs['type_'+ index].getValue().value);
                    index++;
                }
                returnTypes = {
                    isType: isType,
                    items: datas
                }
                return returnTypes;
            }else{
                returnTypes = {
                    isType: false,
                    items: []
                }
                return returnTypes;
            }
        }
    },
    componentDidMount: function() {
        NativeAppEventEmitter.addListener(
          'didRecvMessageResponse',
          (response) => {
              if(response.errCode == -2){
                  AlertIOS.alert(strings.alertPrompt,strings.weChatCancelShare)
              }else if(response.errCode == 0){
                  AlertIOS.alert(strings.alertPrompt,strings.weChatShareSucess)
              }else{
                  AlertIOS.alert(JSON.stringify(response))
              }
          });
    },
    shouldComponentUpdate: function() {
        var index = 0;
        for (var item in this.props.ServerData.attributes) {
            this.refs['type_'+ index].initiValue();
            index++;
        }
        return true;
    },
    getTotalNum: function(){
        return this.refs.setNumber.getValue();
    },
    getName: function(){
        return this.props.ServerData.name;
    },
    getProductImage: function(){
        return this.props.imageUrl.uri;
    },
    setTotalNum: function(num){
        this.refs["setNumber"].setValue(num);
    },
    weChatShare: function(){
        //分享网页
        WeChat.registerApp("wxbc8cf90b64f6de9b", (res) => {
            if(res == true){
              // AlertIOS.alert(this.props.imageUrl.uri.toString())
              WeChat.sendLinkURL({
                  link: ServerURL + '/shop/' + this.props.ServerData.id + '/' + this.props.ServerData.slug + '/',
                  tagName: '钱隆',
                  title: this.props.desc.toString().substr(0,250),
                  desc: '特意分享给你',
                  thumbImage: 'https://dn-qianlonglaile.qbox.me/static/pcQianlong/images/buy_8e82463510d2c7988f6b16877c9a9e39.png',
                  // thumbImage: this.props.imageUrl.uri.toString(),
                  scene: 1
              });
            }
        });
    },
    render: function () {
        if(isEmptyObject(this.props.ServerData) == true){
            returnTypes = {
                isType: false,
                items: []
            }
            return(<View></View>);
        }else{
              if(this.props.ServerData.attributes != null){
                var arrays = [];
                for (var item in this.props.ServerData.attributes) {
                    var obj = {
                        name: item,
                        items: []
                    }
                    for (var i = 0; i < this.props.ServerData.attributes[item].length; i++) {
                        var objSubItem = {
                          name: this.props.ServerData.attributes[item][i][1],
                          value: this.props.ServerData.attributes[item][i][0]
                        }
                        obj.items.push(objSubItem);
                    }
                    arrays.push(obj);
                }
                returnTypes = {
                    isType: false,
                    items: []
                }
                return (
                    <View style={[styles.floatView]}>
                        <View style={{height: 55}}>
                            <TouchableOpacity onPress={this.props.backBarcode} style={[styles.greenButton,{marginTop:20,position:'absolute'}]} activeOpacity={0.3}>
                               <Image source={require('image!leftarrow1')} />
                            </TouchableOpacity>
                            <Text style={{marginTop:30,marginBottom:15,alignSelf:'center'}}>{this.props.ServerData.name.substr(0,18)}</Text>
                            <TouchableOpacity onPress={this.props.favoritesInfo} style={[styles.greenButton,{top:20,right:8,position:'absolute'}]} activeOpacity={0.5}>
                               <Icon name={this.props.favoritesIcon} size={35} style={[styles.greenButtonText,{color: (this.props.favoritesIcon == 'ios-heart-outline') ? 'gray' : '#EE5959',margin:1}]}/>
                            </TouchableOpacity>
                        </View>
                        <ScrollView style={styles.animatedScrollView}>
                            <View style={styles.subContainer}>
                                <Image source={this.props.imageUrl} resizeMode="contain" style={styles.imgDetail} />
                            </View>
                            <View style={styles.viewShare}>
                                <TouchableOpacity onPress={this.weChatShare} style={[styles.redButton,{width: 70}]} activeOpacity={0.8}>
                                    <View style={{flexDirection: 'row',justifyContent:'center'}}>
                                        <Icon name="aperture" size={25} style={[styles.greenButtonText,{marginRight: 5}]}/>
                                        <Text style={[styles.greenButtonText,{fontSize: 14}]}>{strings.btnWeChatShare}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            {
                                  arrays.map((item,index)=>{
                                      return(<MarkerSelection ref={'type_' + index} key={index} data={item.items} title={item.name + "："} />)
                                  })
                            }
                            <View style={styles.txtView}>
                                <Text style={styles.price}>￥:{this.props.ServerData.price}</Text>
                                <SetNumber ref="setNumber" value={1}/>
                            </View>
                            <View style={styles.txtView}>
                                <Text style={styles.desc}>{this.props.desc}</Text>
                                <TouchableOpacity onPress={this.props.showWebViewDesc} activeOpacity={0.8}>
                                  <Text style={styles.lblShowMoreDetails}>{strings.lblShowMoreDetails}</Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                        <View style={styles.viewBottom}>
                            <TouchableOpacity onPress={this.props.orderBuyBtn} style={styles.redButton} activeOpacity={0.8}>
                                <Text style={[styles.greenButtonText,{fontSize: 18}]}>{strings.btnOrder}</Text>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={this.props.btnBuyNow} style={[styles.redButton,{backgroundColor:'#baa071',borderColor: '#baa071'}]} activeOpacity={0.8}>
                                <Text style={[styles.greenButtonText,{fontSize: 18}]}>{strings.btnBuyNow}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                );
            }else{
              //没有类型的时候
              returnTypes = {
                  isType: true,
                  items: []
              }
              return (
                <View style={[styles.floatView]}>
                    <View style={{height: 55}}>
                      <TouchableOpacity onPress={this.props.backBarcode} style={[styles.greenButton,{marginTop:20,position:'absolute'}]} activeOpacity={0.3}>
                          <Image source={require('image!leftarrow1')} />
                      </TouchableOpacity>
                      <Text style={{marginTop:30,marginBottom:15,alignSelf:'center'}}>{this.props.ServerData.name.substr(0,18)}</Text>
                      <TouchableOpacity onPress={this.props.favoritesInfo} style={[styles.greenButton,{top:20,right:8,position:'absolute'}]} activeOpacity={0.5}>
                          <Icon name={this.props.favoritesIcon} size={35} style={[styles.greenButtonText,{color: (this.props.favoritesIcon == 'ios-heart-outline') ? 'gray' : '#EE5959',margin:1}]}/>
                      </TouchableOpacity>
                    </View>
                    <ScrollView style={styles.animatedScrollView}>
                        <View style={styles.subContainer}>
                            <Image source={this.props.imageUrl} resizeMode="contain" style={styles.imgDetail} />
                        </View>
                        <View style={styles.viewShare}>
                            <TouchableOpacity onPress={this.weChatShare} style={[styles.redButton,{width: 70}]} activeOpacity={0.8}>
                                <View style={{flexDirection: 'row',justifyContent:'center'}}>
                                    <Icon name="aperture" size={25} style={[styles.greenButtonText,{marginRight: 5}]}/>
                                    <Text style={[styles.greenButtonText,{fontSize: 14}]}>{strings.btnWeChatShare}</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.txtView}>
                            <Text style={styles.price}>￥:{this.props.ServerData.price}</Text>
                            <SetNumber ref="setNumber" value={1}/>
                        </View>
                        <View style={styles.txtView}>
                            <Text style={styles.desc}>{this.props.desc}</Text>
                            <TouchableOpacity onPress={this.props.showWebViewDesc} activeOpacity={0.8}>
                              <Text style={styles.lblShowMoreDetails}>{strings.lblShowMoreDetails}</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                    <View style={styles.viewBottom}>
                        <TouchableOpacity onPress={this.props.orderBuyBtn} style={styles.redButton} activeOpacity={0.8}>
                            <Text style={[styles.greenButtonText,{fontSize: 18}]}>{strings.btnOrder}</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={this.props.btnBuyNow} style={[styles.redButton,{backgroundColor:'#baa071',borderColor: '#baa071'}]}  activeOpacity={0.8}>
                            <Text style={[styles.greenButtonText,{fontSize: 18}]}>{strings.btnBuyNow}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
              );
            }
        }

    }
});
module.exports = ProductsDetail;
