var React = require('react-native');
var styles = require('./../styles/Login');
var Barcode = require('./Barcode');
var TimerMixin = require('react-timer-mixin');
var Icon = require('react-native-vector-icons/Ionicons');
var t = require('tcomb-form-native');
var Form = t.form.Form;
//更加精细的判断
var Person = t.struct({
  // a required string(t.maybe(t.String) : 可选字段)
  Email: t.refinement(t.String, function (n) {
    var regx = /^(\w)+(\.\w+)*@(\w)+((\.\w+)+)$/;
    return regx.test(n);
  }),
  // an required string
  Password: t.refinement(t.String, function (n) {
    var regx = /^[A-Za-z0-9]{6,}$/;
    return regx.test(n);
  }),
});

var {
    View,
    Text,
    Image,
    ScrollView,
    TextInput,
    AlertIOS,
    Dimensions,
    TouchableHighlight,
    AsyncStorage,
    TouchableOpacity,
    DeviceEventEmitter
} = React;
import store from './../redux/store';
import { postsLogin } from './../redux/actions/loginAction';
var strings = require('./../utils/LocalizedStrings');
var { width, height } = Dimensions.get('window');

var Login = React.createClass({
    mixins: [TimerMixin],
    getInitialState: function () {
        return {
            isShowPassword: true,
            iconName: 'ios-eye-outline',
            editable: true,
            formError: {
              emailErrorState: false,
              passwordErrorState: false,
            },
            formBlur: {
              emailBlurState: false,
              passwordBlurState: false,
            },
            formValues: null,
            keyboardSpace: 0
        };
    },
    componentDidMount: function() {
        // Keyboard events监听
　　     DeviceEventEmitter.addListener('keyboardWillShow', this.updateKeyboardSpace)
        //访问表单内部字段
        this.refs.form.getComponent('Email').refs.input.focus();

        //如果有账户密码，自动登录
        var me = this;
        AsyncStorage.getItem("user")
        .then((value) => {
          if(value != null){
              var obj = JSON.parse(value);
              me.setState({
                editable: false,
                formValues:{
                    Email: obj.Email,
                    Password: obj.Password
                }
              },...this.state)
              me.loginButton();
          }
        })
        .catch((error) => {debugger;}).done();
    },
    loginButton: function(){
        //this.refs.form.validate().isValid() [true:没有错误 | false:有至少一个错误]
        var me = this;
        if(this.refs.form.validate().isValid() == false){
            AlertIOS.alert(strings.alertPrompt,strings.toastTipCorrectAccount);
        }else{
          var obj = {
              email: this.state.formValues.Email,
              password: this.state.formValues.Password
          };
          me.setState({
            editable: false,
          },...this.state)
          store.dispatch(postsLogin(obj)).then((json)=>{
              var serverData = store.getState().loginReducer.ServerData;

              if(serverData.login_status == true){
                  window.GLOBAL.user_id = serverData.id;
                  AsyncStorage.setItem("user",JSON.stringify(this.state.formValues))
                  .then(() => {})
                  .catch((error) => {debugger;}).done();

                  this.setState({editable: false},...this.state);
                  this.setTimeout(
                    () => {
                      const { navigator } = me.props;
                      if(navigator) {
                         navigator.push({
                             name: 'Barcode',
                             component: Barcode
                         })
                      }
                      me.setState({editable: true});
                    },
                    500
                  );
              }else{
                  me.setState({
                    editable: true,
                  },...this.state)
                  AlertIOS.alert("提示","对不起，用户名 或 密码错误。");
              }
          }).catch((error)=>{
              me.setState({
                editable: true,
              },...this.state)
              AlertIOS.alert("提示","对不起，数据请求失败：" + error.message);
          }).done();
        }
    },
    facebookButton: function(){
        //获取所有值
        var value = this.refs.form.getValue();
        if (value) {
          console.log(value);
          // 清除所有值
          this.setState({ formValues: null });
        }
        this.setState({isShowPassword: true})
    },
    onChange: function(value,fieldName) {
      //表单全部一起验证
      // this.refs.form.validate();
      //把值，写入到 state 对象中
      // this.setState({formValues: value});
      //针对每一个，逐个进行验证
      switch (fieldName[0]) {
        case "Email":
            this.setState({
              formValues: value,
              formError: {
                ...this.state.formError,
                emailErrorState: !/^(\w)+(\.\w+)*@(\w)+((\.\w+)+)$/.test(value.Email)
              }
            });
            this.refs.form.getComponent('Email').removeErrors();
            return;
        case "Password":
            this.setState({
              formValues: value,
              formError: {
                ...this.state.formError,
                passwordErrorState: !/^[A-Za-z0-9]{6,}$/.test(value.Password)
              }
            });
            this.refs.form.getComponent('Password').removeErrors();
            return;
        default:
          break;
      }
      this.refs.form.render();
      // debugger;
    },
    scrollViewTo:function(str){
       if(this.state.keyboardSpace != 0){
          var offsetY = (height - this.state.keyboardSpace) - 305;
   　　　　let scrollLength = 0;
   　　　　if (str == 'Email') {
   　　　　　　scrollLength = offsetY;
   　　　　}else if (str == 'Password') {
            scrollLength = (offsetY + 40);
         }
   　　　 this.refs.scroll.scrollTo({x: 0, y: scrollLength});
       }
　　 },
    updateKeyboardSpace: function(frames){
       const keyboardSpace = frames.endCoordinates.height; //获取键盘高度
       this.setState({
    　　　　keyboardSpace: keyboardSpace,
       });
    },
    render: function () {
        // optional rendering options (see documentation)
        var options = {
          // auto: 'placeholders', //自动填充提示 ['placeholders' | 'none' <默认>]
          // order: ['Email','age', 'Password'],  //字段排序
          fields: {
            Email:{
              label: strings.lblEmail,
              editable: this.state.editable,
              keyboardType: 'email-address',
              placeholder: strings.lblPlaceholderEmail,
              hasError: this.state.formError.emailErrorState,
              hasBlur: this.state.formBlur.emailBlurState,
              error: strings.lblEmailError,  //可以自定义：<i>A custom error message</i>
              onBlur: () => {
                this.setState({
                  formBlur: {
                    ...this.state.formBlur,
                    emailBlurState: false
                  }
                });
              },
              onFocus: () => {
                this.setState({
                  formBlur: {
                    ...this.state.formBlur,
                    emailBlurState: true
                  }
                });

                this.scrollViewTo("Email");
              },
              onEndEditing:() => {
                this.refs.scroll.scrollTo({x: 0, y: 0});
              },
              clearButtonMode: 'always'
            },
            Password:{
              label: strings.lblPassword,
              editable: this.state.editable,
              maxLength: 12,
              secureTextEntry: this.state.isShowPassword,
              isShowPassword: true,
              placeholder: strings.lblPlaceholderPassword,
              hasError: this.state.formError.passwordErrorState,
              hasBlur: this.state.formBlur.passwordBlurState,
              error: strings.lblPasswordError,
              onBlur: ()=>{
                this.setState({
                  formBlur: {
                    ...this.state.formBlur,
                    passwordBlurState: false
                  }
                });
              },
              onFocus: () => {
                this.setState({
                  formBlur: {
                    ...this.state.formBlur,
                    passwordBlurState: true
                  }
                });

                this.scrollViewTo("Password");
              },
              onEndEditing: () => {
                this.refs.scroll.scrollTo({x: 0, y: 0});
              }
            }
          }
        };

        return (
            <ScrollView ref='scroll' contentContainerStyle={styles.container} keyboardShouldPersistTaps={true}>
                <Image source={require('image!background')} style={styles.imageBackground}/>
                <View style={styles.viewContainer} onStartShouldSetResponderCapture={(e) => {
      　　　　　　　　const target = e.nativeEvent.target;
      　　　　　　　　if (target !== React.findNodeHandle(this.refs.form.getComponent('Email').refs.input) && target !== React.findNodeHandle(this.refs.form.getComponent('Password').refs.input)) {
      　　　　　　　　　　this.refs.form.getComponent('Email').refs.input.blur();
      　　　　　　　　　　this.refs.form.getComponent('Password').refs.input.blur();
      　　　　　　　　}
                }}>
                  <Form
                        ref="form"
                        type={Person}
                        options={options}
                        value={this.state.formValues}
                        onChange={this.onChange}
                  />
                  <View style={{flexDirection:'column'}}>
                      <TouchableHighlight onPress={this.loginButton} style={styles.button} underlayColor='#99d9f4'>
                          <Text style={styles.buttonText}>{strings.btnSignIn}</Text>
                      </TouchableHighlight>
                  </View>
                </View>
            </ScrollView>
            );
    }
});
module.exports = Login;
