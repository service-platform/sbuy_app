import React from 'react';
import {
    Image,
    StyleSheet,
    Text,
    View,
    AlertIOS,
    Switch,
    TextInput,
    Dimensions,
    TouchableHighlight,
    TouchableOpacity
} from 'react-native';
import Button from 'apsl-react-native-button';
var Icon = require('react-native-vector-icons/Ionicons');
var {height, width} = Dimensions.get('window');
import store from './../redux/store';
import { postsAddAddress,getUpdateAddressInfo,getGetAddressList } from './../redux/actions/productsAction';
var strings = require('./../utils/LocalizedStrings');

var AddAddress = React.createClass({
    getInitialState: function () {
      return {
        receiver: '', // 收货人
        phone: '', // 手机号码
        delivery_address: '', // 收货地址
        defaultAddress: false, // 默认为收货地址(1为是，0为不是)
        isDisabled: false,
        red: '#f23020'
      }
    },
    backPage: function(){
      const { navigator } = this.props;
      if(navigator) {
         navigator.pop();
      }
    },
    componentWillMount: function(){
        store.dispatch(getGetAddressList(window.GLOBAL.user_id)).then((json) =>{
            if (json.ServerData.length <= 0){
                this.setState({defaultAddress: true});
            }
        }).catch((error)=>{}).done();

        if(this.props.mark == 'Update'){
            this.setState({
              receiver: this.props.receiver,
              phone: this.props.phone,
              delivery_address: this.props.delivery_address,
              defaultAddress: (this.props.default == 1)?true:false,
              isDisabled: false,
            });
        }else{
            this.setState({
              isDisabled: true,
            });
        }
    },
    btnSaveOrUpdate: function(){
        if(this.props.mark == 'Add'){
            if(this.state.receiver == ''){
                AlertIOS.alert(strings.alertPrompt,strings.toastReceiverNotEmpty); return;
            }
            if(/^[1][358][0-9]{9}$/.test(this.state.phone) == false){
                AlertIOS.alert(strings.alertPrompt,strings.toastPhoneIncorrect); return;
            }
            if(this.state.delivery_address == ''){
                AlertIOS.alert(strings.alertPrompt,strings.toastShippingAddressNotEmpty); return;
            }

            var objData = {
                user: window.GLOBAL.user_id,
                receiver: this.state.receiver,
                phone: this.state.phone,
                delivery_address: this.state.delivery_address,
                default: (this.state.defaultAddress == true)?1:0,
            }
            var me = this;
            store.dispatch(postsAddAddress(objData)).then((json) =>{
                if(json.ServerData.add_address != undefined){
                    AlertIOS.alert(strings.alertPrompt,"对不起，添加收货地址失败！"); return;
                }else{
                    AlertIOS.alert(strings.alertPrompt,strings.alertAddAddressOK);
                    me.backPage(); me.props.refreshData();

                    me.props.setAddressData({
                      addressId: json.ServerData[0].id,
                      receiver: json.ServerData[0].receiver,
                      delivery_address: json.ServerData[0].delivery_address,
                    });
                }
            }).catch((error)=>{
                AlertIOS.alert(strings.alertPrompt,"对不起，数据请求失败：" + error.message);
            }).done();
        }else{
            var objData = {
                user: window.GLOBAL.user_id,
                id: this.props.id,
                receiver: this.state.receiver,
                phone: this.state.phone,
                delivery_address: this.state.delivery_address,
                default: (this.state.defaultAddress == true)?1:0,
            }
            var me = this;
            store.dispatch(getUpdateAddressInfo(objData)).then((json) =>{
                if(json.ServerData.update_address != undefined){
                    AlertIOS.alert(strings.alertPrompt,"对不起，修改收货地址失败！"); return;
                }else{
                    AlertIOS.alert(strings.alertPrompt,strings.alertUpdateAddressOk);
                    me.backPage(); me.props.refreshData();
                    me.props.setAddressData({
                      addressId: json.ServerData[0].id,
                      receiver: json.ServerData[0].receiver,
                      delivery_address: json.ServerData[0].delivery_address,
                    });
                }
            }).catch((error)=>{
                AlertIOS.alert(strings.alertPrompt,"对不起，数据请求失败：" + error.message);
            }).done();
        }
    },
    isNotEmpty: function(){
        if(this.state.receiver == '' || this.state.phone == '' || this.state.delivery_address == ''){
            return false;
        }else{
            if(/^[1][358][0-9]{9}$/.test(this.state.phone) == false){
                return false;
            }
            return true;
        }
    },
    render: function(){
        var toolbarTitle = strings.lblAddAddressTitle;
        if(this.props.mark == 'Update'){
            toolbarTitle = strings.lblUpdateAddressTitle;
        }else{
            toolbarTitle = strings.lblAddAddressTitle;
        }
        return (
            <View style={styles.container}>
                <View style={{height: 55,borderColor:'#E0E0E0',borderBottomWidth:1}}>
                  <TouchableOpacity onPress={this.backPage} style={[styles.greenButton,{margin:0,marginTop:25,position:'absolute'}]} activeOpacity={0.3}>
                          <Image source={require('image!leftarrow1')} />
                  </TouchableOpacity>
                  <Text style={{marginTop:30,marginBottom:15,alignSelf:'center'}}>{toolbarTitle}</Text>
                </View>
                <View style={styles.viewField}>
                    <View style={styles.viewFieldContent}>
                        <Text style={styles.txtFieldName}>{strings.lblAddReceiver}</Text>
                        <TextInput style={styles.inputField} placeholder={strings.placeholderReceiver} onChangeText={(text) => this.setState({receiver: text,isDisabled: !this.isNotEmpty()})} value={this.state.receiver}/>
                    </View>
                    <View style={styles.viewFieldContent}>
                        <Text style={styles.txtFieldName}>{strings.lblAddressPhone}</Text>
                        <TextInput style={styles.inputField} placeholder={strings.placeholderPhone} onChangeText={(text) => {
                          this.setState({phone: text});
                          this.setState({isDisabled: !this.isNotEmpty()});
                        }} value={this.state.phone} keyboardType="numeric"/>
                    </View>
                    <View style={styles.viewFieldContent}>
                        <Text style={styles.txtFieldName}>{strings.lblAddShippingAddress}</Text>
                        <TextInput style={styles.inputField} placeholder={strings.placeholderAddress}  onChangeText={(text) => this.setState({delivery_address: text,isDisabled: !this.isNotEmpty()})} value={this.state.delivery_address} />
                    </View>
                    <View style={styles.viewFieldContent}>
                        <Text style={styles.txtFieldName}>{strings.lblIsDefaultAddress}</Text>
                        <Switch style={{marginRight: 10,marginTop:5}} onValueChange={(text) => this.setState({defaultAddress: text})} value={this.state.defaultAddress}/>
                    </View>
                    <View style={styles.viewBtnContent}>
                        <Button onPress={this.btnSaveOrUpdate} style={styles.button} underlayColor='#169E82' isDisabled={this.state.isDisabled} textStyle={styles.buttonText} disabledStyle={[styles.button,{backgroundColor:'#CBCBCB'}]}>
                            {strings.btnSaveAndOk}
                        </Button>
                    </View>
                </View>
            </View>
        );
    }
});
module.exports = AddAddress;

var styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#F9F9F9'
    },
    greenButton: {
      height: 20,
      width: 25,
      flex: 1,
      marginTop:6,
      marginBottom: 10,
      marginLeft: 7,
      alignSelf: 'stretch',
      justifyContent: 'center'
    },
    greenButtonText: {
      color: 'white',
      alignSelf: 'center'
    },
    viewField: {
        flexDirection: 'column',
        height: height - 63
    },
    viewFieldContent:{
        flexDirection: 'row',
        borderColor: '#E5E5E5',
        borderBottomWidth: 1,
        height: 40
    },
    txtFieldName:{
        color: '#212121',
        flex: 1,
        marginLeft: 10,
        marginTop: 11
    },
    inputField:{
        flex: 3,
        backgroundColor: 'transparent',
        fontSize: 14
    },
    viewBtnContent:{
        position: 'absolute',
        backgroundColor: '#F8F8F8',
        borderColor: '#E0E0E0',
        borderTopWidth: 1,
        height: 40,
        width: width,
        bottom: 0,
        paddingTop: 5,
    },
    button: {
      height: 36,
      flexDirection: 'row',
      backgroundColor: '#1D1D1D',
      borderWidth: 0,
      marginBottom: 10,
      alignSelf: 'center',
      justifyContent: 'center',
      alignItems:'center',
      borderRadius: 0,
      paddingLeft: 48,
      paddingRight: 48,
    },
    buttonText: {
      fontSize: 16,
      color: 'white',
      alignSelf: 'center'
    },
});
