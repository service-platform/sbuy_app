'use strict';
var React = require('react-native');
var styles = require('./../styles/DrawerLayout');
var Share = require('react-native-share');
var ImagePicker = require('@remobile/react-native-image-picker');
var Camera = require('@remobile/react-native-camera');
var PushNotification = require('react-native-push-notification');
var httpCache = require('react-native-http-cache');
import ActionSheet from 'react-native-actionsheet';

const CANCEL_INDEX = 0;
const DESTRUCTIVE_INDEX = 3;
var {
    View,
    Text,
    Image,
    Alert,
    TextInput,
    StyleSheet,
    BackAndroid,
    Animated,
    AlertIOS,
    PushNotificationIOS,
    TouchableHighlight,
} = React;
var strings = require('./../utils/LocalizedStrings');

var DrawerLayout = React.createClass({
    getInitialState: function() {
        return {
            bounceValue: new Animated.Value(0),
            image: require('image!user_logo')
        };
    },
    selectPic:function (val) {
      var me = this;
      if (val == 1) {
          var options = {maximumImagesCount: 1, quality: 90};
          ImagePicker.getPictures(options, function(results) {
              me.setState({
                  image: {uri: results[0], isStatic: true}
              });
          }, function (error) {
              Alert.alert('Error', error,[{text: 'Cancel'}]);
          });
      } else if (val == 2) {
          var options = {
              quality: 80,
              allowEdit: true,
              destinationType: Camera.DestinationType.DATA_URL,
          };
          Camera.getPicture(options, (imageData) => {
              me.setState({
                image: {uri:'data:image/jpeg;base64,' + imageData}
              });
          });
      }
    },
    mySBuy: function(){

    },
    clearCache: function(){
      httpCache.getSize()
      .then((value) =>{
          Alert.alert(
            strings.lblClearCache,
            strings.formatString(strings.alertTipClear,value),
            [
              {text: strings.alertCancel},
              {text: strings.alertClear, onPress: () => {
                httpCache.clear()
                .then((value) =>{
                    AlertIOS.alert(strings.alertPrompt,strings.toastClearSuccess);
                }).catch((error) => {
                    AlertIOS.alert(strings.alertPrompt,strings.toastClearFailure);
                }).done();
              }},
            ]
          );
      }).catch((error) => {debugger;}).done();
    },
    shareAPP: function(){
      Share.open({
         share_text: "这是一个分享链接",
         share_URL: "http://www.s-buy.com",
         title: strings.alertShareTitle
       },function(e) {
         console.log(e);
       });
    },
    exitSBuy:function(){
        var me = this;
        Alert.alert(
          strings.lblExit,
          strings.alertTipExit,
          [
            {text: strings.alertCancel},
            {text: strings.lblExit, onPress: () => {
                JSON.backIndex = 1;
                me.props.backLogin();
            }},
          ]
        );
    },
    PushNotif: function(){
      PushNotification.localNotificationSchedule({
          /* iOS and Android properties */
          message: "Congratulations, winning the lottery!" // (required)
          // date: new Date(Date.now() + (60 * 1000)) // in 60 secs
      });
    },
    openSelectPicMenu:function(){
      this.ActionSheet.show();
    },
    render: function () {
        PushNotificationIOS.requestPermissions( { alert: true, badge: true, sound: true });
        const buttons = [strings.alertCancel, strings.lblMenuLocationImg, strings.lblOpenCamera];
        return (
            <View style={styles.container}>
                <View style={styles.viewHeader}>
                    <View style={styles.viewHeaderLayout}>
                        <Image
                            source={require('image!search')}
                            style={styles.imgHeader} />

                        <TextInput
                            style={styles.txtHeader}
                            placeholderTextColor="rgb(211,211,212)"
                            onChangeText={(text) => this.setState({barCode: text})}
                            placeholder={strings.lblSearch}/>
                    </View>
                </View>
                <View style={[styles.rowSplitLine,{marginBottom:10}]}></View>
                <View style={styles.viewHeaderIcon}>
                    <View style={styles.viewHeaderIconLayout}>
                            <TouchableHighlight onPress={this.openSelectPicMenu} style={styles.btnHeaderImg} underlayColor='#E2E2E244'>
                                <Image
                                    source={this.state.image}
                                    style={styles.imgHeaderIcon} />
                            </TouchableHighlight>
                            <Text style={styles.txtHeaderName}>
                                Richard The King
                            </Text>

                    </View>
                </View>
                <View style={styles.rowSplitLine}></View>
                <View style={styles.viewMenuItem}>
                    <TouchableHighlight onPress={this.mySBuy} style={styles.btnMenuItem} underlayColor='#E2E2E244'>
                        <View style={styles.viewMenuLayout}>
                              <Image
                                  source={require('image!setting')}
                                  style={styles.menuIcon} />

                              <Text style={styles.txtMenuItem}>
                                  {strings.lblMySBuy}
                              </Text>
                        </View>
                    </TouchableHighlight>
                </View>
                <View style={styles.rowSplitLine}></View>

                <View style={styles.viewMenuItem}>
                    <TouchableHighlight onPress={this.clearCache} style={styles.btnMenuItem} underlayColor='#E2E2E244'>
                        <View style={styles.viewMenuLayout}>
                            <Image
                                source={require('image!setting')}
                                style={styles.menuIcon} />

                            <Text style={styles.txtMenuItem}>
                                {strings.lblClearCache}
                            </Text>
                        </View>
                    </TouchableHighlight>
                </View>
                <View style={styles.rowSplitLine}></View>

                <View style={styles.viewMenuItem}>
                    <TouchableHighlight onPress={this.shareAPP} style={styles.btnMenuItem} underlayColor='#E2E2E244'>
                        <View style={styles.viewMenuLayout}>
                            <Image
                                source={require('image!setting')}
                                style={styles.menuIcon} />

                            <Text style={styles.txtMenuItem}>
                                {strings.lblShareAPP}
                            </Text>
                        </View>
                    </TouchableHighlight>
                </View>
                <View style={styles.rowSplitLine}></View>

                <View style={styles.viewMenuItem}>
                    <TouchableHighlight onPress={()=>{}} style={styles.btnMenuItem} underlayColor='#E2E2E244'>
                        <View style={styles.viewMenuLayout}>
                            <Image
                                source={require('image!setting')}
                                style={styles.menuIcon} />

                            <Text style={styles.txtMenuItem}>
                                {strings.lblAboutAPP}
                            </Text>
                        </View>
                    </TouchableHighlight>
                </View>

                <View style={styles.rowSplitLine}></View>
                <View style={styles.viewMenuItem}>
                    <TouchableHighlight onPress={this.PushNotif} style={styles.btnMenuItem} underlayColor='#E2E2E244'>
                        <View style={styles.viewMenuLayout}>
                            <Image
                                source={require('image!setting')}
                                style={styles.menuIcon} />

                            <Text style={styles.txtMenuItem}>
                                {strings.lblPushNotifications}
                            </Text>
                        </View>
                    </TouchableHighlight>
                </View>
                <View style={styles.rowSplitLine}></View>
                <View style={styles.viewMenuItem}>
                    <TouchableHighlight onPress={this.exitSBuy} style={styles.btnMenuItem} underlayColor='#E2E2E244'>
                        <View style={styles.viewMenuLayout}>
                            <Image
                                source={require('image!setting')}
                                style={styles.menuIcon} />

                            <Text style={styles.txtMenuItem}>
                                {strings.lblExit}
                            </Text>
                        </View>
                    </TouchableHighlight>
                </View>
                <ActionSheet
                    ref={(o) => this.ActionSheet = o}
                    title={strings.titlePicSource}
                    options={buttons}
                    cancelButtonIndex={CANCEL_INDEX}
                    destructiveButtonIndex={DESTRUCTIVE_INDEX}
                    onPress={(val)=>{this.selectPic(val)}}
                />
            </View>
        );
    }
});

module.exports = DrawerLayout;
