﻿import React from 'react';
var ItemCheckbox = require('./Component/ItemCheckbox');
import {
    StyleSheet,
    Text,
    View,
    Image,
    AlertIOS,
    TextInput,
    Dimensions,
    TouchableHighlight,
    TouchableOpacity
} from 'react-native';
var {height, width} = Dimensions.get('window');
var strings = require('./../utils/LocalizedStrings');

var SelectInvoice = React.createClass({
    getInitialState: function () {
      return {
          NoInvoice: true,
          IndividualInvoice: false,
          CompanyInvoices: false,
          companyName: ''
      }
    },
    componentWillMount: function() {
        if(this.props.invoiceStatus == strings.lblNoInvoice){
            this.setState({
                NoInvoice: true,
                IndividualInvoice: false,
                CompanyInvoices: false,
                companyName: ''
            })
        }else if(this.props.invoiceStatus == strings.lblIndividualInvoice){
            this.setState({
                NoInvoice: false,
                IndividualInvoice: true,
                CompanyInvoices: false,
                companyName: ''
            })
        }else{
            this.setState({
                NoInvoice: false,
                IndividualInvoice: false,
                CompanyInvoices: true,
                companyName: this.props.invoiceName
            })
        }
    },
    onSelectItem: function(value){
        try{
            if(value == 1){
                if(this.refs["NoInvoice"].getValue() == true){
                    this.setState({
                        NoInvoice: true,
                        IndividualInvoice: false,
                        CompanyInvoices: false
                    });
                }else{
                    this.setState({
                        NoInvoice: false,
                        IndividualInvoice: false,
                        CompanyInvoices: false
                    });
                }
                this.refs["IndividualInvoice"].setValue(false);
                this.refs["CompanyInvoices"].setValue(false);
            }else if(value == 2){
                if(this.refs["IndividualInvoice"].getValue() == true){
                    this.setState({
                        NoInvoice: false,
                        IndividualInvoice: true,
                        CompanyInvoices: false
                    });
                }else{
                    this.setState({
                        NoInvoice: false,
                        IndividualInvoice: false,
                        CompanyInvoices: false
                    });
                }
                this.refs["NoInvoice"].setValue(false);
                this.refs["CompanyInvoices"].setValue(false);
            }else if(value == 3){
                if(this.refs["CompanyInvoices"].getValue() == true){
                    this.setState({
                        NoInvoice: false,
                        IndividualInvoice: false,
                        CompanyInvoices: true
                    });
                }else{
                    this.setState({
                        NoInvoice: false,
                        IndividualInvoice: false,
                        CompanyInvoices: false
                    });
                }
                this.refs["NoInvoice"].setValue(false);
                this.refs["IndividualInvoice"].setValue(false);
            }
        }catch(e){}
    },
    selectedValue: function(){
        if(this.state.NoInvoice == true){
            this.backPage();
            this.props.selectedInvoices(1);
        }else if(this.state.IndividualInvoice == true){
            this.backPage();
            this.props.selectedInvoices(2);
        }else if(this.state.CompanyInvoices == true){
            if(this.state.companyName == ''){
                AlertIOS.alert(strings.alertPrompt,strings.toastCompanyNameNotEmpty);
                return;
            }
            this.backPage();
            this.props.selectedInvoices(3,this.state.companyName);
        }else{
            AlertIOS.alert(strings.alertPrompt,strings.toastLeastOne);
        }
    },
    backPage: function(){
        const { navigator } = this.props;
        if(navigator) {
           navigator.pop();
        }
    },
    render:function (){
        var jsxCompanyName = null;
        if(this.state.CompanyInvoices == true){
            jsxCompanyName = <View style={{ backgroundColor:"#fff",paddingLeft:47,marginBottom: 10}}>
                <View style={styles.companyView}>
                    <TextInput style={styles.input} value={this.state.companyName} onChangeText={(text) => this.setState({companyName: text})} placeholder={strings.inputCompanyName}/>
                </View>
            </View>
        }
        return (
            <View style={styles.container}>
                <View style={{height: 55,borderColor:'#E0E0E0',borderBottomWidth:1}}>
                     <TouchableOpacity onPress={this.backPage} style={[styles.greenButton,{margin:0,marginTop:25,position:'absolute'}]} activeOpacity={0.3}>
                         <Image source={require('image!leftarrow1')} />
                     </TouchableOpacity>
                     <Text style={{marginTop:30,marginBottom:15,alignSelf:'center'}}>{strings.lblToolbarTitle}</Text>
                </View>
                <View style={styles.content}>
                    <ItemCheckbox ref="NoInvoice" size={25} checked={this.state.NoInvoice} color="#baa071" onCheck={()=>this.onSelectItem(1)} style={{marginRight: 10}}/>
                    <Text style={styles.commonText}>{strings.tipNoInvoice}</Text>
                    <View style={styles.line}></View>
                </View>
                <View style={styles.content}>
                    <ItemCheckbox ref="IndividualInvoice" size={25} checked={this.state.IndividualInvoice} color="#baa071" onCheck={()=>this.onSelectItem(2)} style={{marginRight: 10}}/>
                    <Text style={styles.commonText}>{strings.tipIndividualInvoice}</Text>
                    <View style={styles.line}></View>
                </View>
                <View style={styles.content}>
                    <ItemCheckbox ref="CompanyInvoices" size={25} checked={this.state.CompanyInvoices} color="#baa071" onCheck={()=>this.onSelectItem(3)} style={{marginRight: 10}}/>
                    <Text style={styles.commonText}>{strings.tipCompanyInvoices}</Text>
                    <View style={styles.line}></View>
                </View>
                {jsxCompanyName}

                <View style={styles.viewOkAndTip}>
                    <TouchableHighlight onPress={this.selectedValue} underlayColor="#BBA988">
                        <View style={styles.btnEnsure}>
                            <Text style={styles.textEnsure}>{strings.alertOk}</Text>
                        </View>
                    </TouchableHighlight>
                    <Text style={styles.text}>
                        {strings.tipInvoiceInfo}
                    </Text>
                </View>
            </View>
        )
    }
});
module.exports = SelectInvoice;

var styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        backgroundColor: '#fff',
        height: height
    },
    greenButton: {
      height: 27,
      width: 50,
      flex: 1,
      marginTop:3,
      borderRadius: 4,
      marginBottom: 10,
      marginLeft: 7,
      alignSelf: 'stretch',
      justifyContent: 'center'
    },
    greenButtonText: {
      color: 'white',
      alignSelf: 'center'
    },
    viewOkAndTip:{
        paddingLeft: 10,
        paddingRight: 10
    },
    companyView:{
        borderColor: '#B3B3B3',
        borderWidth: 1,
        height: 30,
        marginRight: 10
    },
    btnEnsure:{
        backgroundColor:"#baa071",
        height: 36,
        paddingTop: 6
    },
    textEnsure:{
        color:'#fff',
        marginTop:4,
        alignSelf:'center',
        fontSize: 18
    },
    content:{
        paddingTop:20,
        flexDirection: 'row',
        paddingBottom:20,
        paddingLeft:20,
        backgroundColor:'#fff'
    },
    commonText:{
        color:"#232326",
        width: width-100,
        marginTop: 5
    },
    line:{
        width: width-60,
        height: 1,
        backgroundColor:'#C7C7C7',
        position:'absolute',
        top: 50,
        left:50
    },
    input:{
        backgroundColor: 'transparent',
        height: 30,
        fontSize: 15,
        paddingLeft: 5
    },
    text:{
        marginTop:10
    }
});
