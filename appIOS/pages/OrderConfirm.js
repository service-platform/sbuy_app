var React = require('react-native');
var styles = require('./../styles/OrderConfirm');
var AddressList = require('./AddressList');
var SelectInvoice = require('./SelectInvoice');

var {
    View,
    Text,
    Image,
    AlertIOS,
    NetInfo,
    TextInput,
    Dimensions,
    ScrollView,
    AsyncStorage,
    NativeModules,
    TouchableHighlight,
    TouchableOpacity
} = React;
let { width, height } = Dimensions.get('window');
var Icon = require('react-native-vector-icons/Ionicons');

import store from './../redux/store';
import { postsOrderInfo,postsBuyNow,getPaymentInfo,postPlaymentResult,postsGoodsUrl,getGetAddressList } from './../redux/actions/productsAction';
var strings = require('./../utils/LocalizedStrings');
import ModalPicker from 'react-native-modal-picker'

var OrderConfirm = React.createClass({
    getInitialState: function () {
      return {
        total: 0,
        grandTotal: '0',
        txtAddress: "",
        deliveryType: 1,
        paymentType: 1,
        paymentString: '支付宝',
        deliveryString: '快递',
        orderId: "",
        arrayList: [],
        addressId: 0,
        receiver: '',
        delivery_address: '',
        invoiceStatus: strings.lblNoInvoice,
        invoiceName: ''
      }
    },
    getDefaultProps: function() {
        return {
            grandTotal: '0',
        };
    },
    propTypes: {
        grandTotal: React.PropTypes.string.isRequired,
    },
    refreshAddress: function(mark,objData){
      if(mark == true){
          this.setState({
            addressId: 0,
            receiver: '',
            delivery_address: '',
          });
          store.dispatch(getGetAddressList(window.GLOBAL.user_id)).then((json) =>{
              for (var i = 0; i < json.ServerData.length; i++) {
                  if (json.ServerData[i].default == 1){
                    this.setState({
                        addressId: json.ServerData[i].id,
                        receiver: json.ServerData[i].receiver,
                        delivery_address: json.ServerData[i].delivery_address
                    });
                    break;
                  }
              }
          }).catch((error)=>{
              AlertIOS.alert(strings.alertPrompt,"对不起，数据请求失败：" + error.message);
          }).done();
      }else{
          this.setState({
            addressId: objData.addressId,
            receiver: objData.receiver,
            delivery_address: objData.delivery_address,
          });
      }
    },
    componentWillMount:function() {
      this.setState({grandTotal: this.props.grandTotal});
      //获取总数量
      this.setState({
          total: this.props.totalNum
      });
      this.refreshAddress(true);
      if(this.props.isBuyNow == true){
        var arrayList = [];
        arrayList.push(this.props.pIds);
        this.setState({
            arrayList: arrayList
        });
      }else if(this.props.isBuyNow == false){
        var me = this;
        store.dispatch(postsGoodsUrl(ServerURL + "/cart/get-cart-api/" + window.GLOBAL.user_id)).then((json) =>{
            if(isEmptyObject(json.ServerData) == false){
                var arrayList = []; var arrays = json.ServerData;

                for(var item in arrays){
                    for (var i = 0; i < arrays[item].length; i++) {
                        for (var j = 0; j < me.props.pIds.length; j++) {
                            if(me.props.pIds[j] === arrays[item][i].id){
                                arrayList.push(arrays[item][i]); break;
                            }
                        }
                    }
                }

                me.setState({
                    arrayList: arrayList
                });
            }
        }).catch((error)=>{
            AlertIOS.alert(strings.alertPrompt,"对不起，数据请求失败：" + error.message);
        }).done();
      }

      //获取街道信息
      return;
      navigator.geolocation.getCurrentPosition(
          (initialPosition) => {
              var latitude = initialPosition.coords.latitude
              var longitude = initialPosition.coords.longitude;

              //北京市海淀区中关村大街27号1101-08室
              // longitude = 116.322987;
              // latitude = 39.983424;

              fetch('http://api.map.baidu.com/geocoder/v2/?ak=61f8bd72d68aef3a7b66537761d29d82&callback=renderReverse&location=' + latitude + ',' + longitude + '&output=json&pois=0')
              .then(response => response.text()) //返回 非 JSON 字符串
              .then(text => {
                  var processText = text.replace("renderReverse&&renderReverse(","").substr(0,text.replace("renderReverse&&renderReverse(","").length - 1);
                  var obj = JSON.parse(processText);
                  if(obj.result.addressComponent.country != "中国"){
                    AlertIOS.alert(strings.alertPrompt,"位置信息不在中国，或不可用。");
                  }else{
                    me.setState({
                        txtAddress: obj.result.addressComponent.country + "、" + obj.result.addressComponent.province + "、" + obj.result.addressComponent.city + "、" + obj.result.addressComponent.district + "、" + obj.result.addressComponent.street + " " + obj.result.addressComponent.street_number
                    });
                  }
              })
              .catch((e)=>{});
          },
          (error) => {
            AlertIOS.alert(strings.alertPrompt,"GPS 不可用，请打开。");
          }
        );
    },
    paymentCallback: function(result){
      console.log(result);

      if(result.resultStatus == 9000){
          if(this.state.orderId == ""){
              var objData = {
                  uid: window.GLOBAL.user_id,
                  pid: this.props.obj.state.ServerData.id,
                  quantity: this.props.totalNum,
                  paid: 1
              };
              store.dispatch(postsBuyNow(objData)).then((json) =>{
                  AlertIOS.alert(strings.alertPrompt,strings.lblSuccessfulpayment);
                  this.backPage();
              }).catch((error)=>{
                  AlertIOS.alert(strings.alertPrompt,"对不起，数据请求失败：" + error.message);
              }).done();
          }else{
              var strResult = {
                id: this.state.orderId,
                paid: 1
              };
              store.dispatch(postPlaymentResult(ServerURL + '/orders/order-paid-api/',strResult)).then((json) =>{
                  if(json.ServerData.change_status == true){
                      AlertIOS.alert(strings.alertPrompt,strings.lblSuccessfulpayment);
                      this.backPage();
                  }else{
                      AlertIOS.alert(strings.alertPrompt,strings.lblSystemBusy);
                  }
              }).catch((error)=>{
                  AlertIOS.alert("提示","对不起，数据请求失败：" + error.message);
              }).done();
          }
      }else if(result.resultStatus == 6002){
          if(this.state.orderId == ""){
            var objData = {
                uid: window.GLOBAL.user_id,
                pid: this.props.obj.state.ServerData.id,
                quantity: this.props.totalNum,
                paid: 0
            };
            store.dispatch(postsBuyNow(objData)).then((json) =>{
                if(json.ServerData.length >= 1){
                    this.setState({
                      orderId: json.ServerData[0].id
                    })
                }
                AlertIOS.alert(strings.alertPrompt,strings.lblNetworkError);
            }).catch((error)=>{
                AlertIOS.alert(strings.alertPrompt,"对不起，数据请求失败：" + error.message);
            }).done();
          }
      }else if(result.resultStatus == 6001){
          AlertIOS.alert(strings.alertPrompt,strings.lblUserCanceled);
      }else{
          if(this.state.orderId == ""){
            var objData = {
                uid: window.GLOBAL.user_id,
                pid: this.props.obj.state.ServerData.id,
                quantity: this.props.totalNum,
                paid: 0
            };
            store.dispatch(postsBuyNow(objData)).then((json) =>{
                if(json.ServerData.length >= 1){
                    this.setState({
                      orderId: json.ServerData[0].id
                    });
                }
                AlertIOS.alert(strings.alertPrompt,strings.lblSystemSorry + result.memo + "。");
            }).catch((error)=>{
                AlertIOS.alert(strings.alertPrompt,"对不起，数据请求失败：" + error.message);
            }).done();
          }
      }
    },
    btnPayment: function(){
      if(this.props.isBuyNow == true){
        if(this.state.addressId == 0){
            AlertIOS.alert(strings.alertPrompt,'请选择配送地址'); return;
        }
        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected == true){
                var me = this;
                store.dispatch(getPaymentInfo('http://120.25.72.158:8369/payHunter-1.0-SNAPSHOT/aliPay')).then((json) =>{
                    NativeModules.AlipayRequestConfig.pay(json.ServerData.data,this.paymentCallback);
                    // var testStr = 'resultStatus={9000};memo={处理成功};result={partner="2088221420923333"&seller_id="creativestar@cssinco.cn"&out_trade_no="20160329150156"&subject="支付宝支付"&body="用于测试支付宝快捷支付测试！"&total_fee="0.01"&notify_url="http://218.77.183.189:8090/merry/notify_url.jsp"&service="mobile.securitypay.pay"&payment_type="1"&_input_charset="utf-8"&it_b_pay="1d"&return_url="http://www.tianjiandao.com"&success="true"&sign_type="RSA"&sign="ecPYpWTTcqy7Ydsv+rRq2KRAoPMKPZyV4zljvw0rxebV5RMRybAnNaaPRqCq5lltvtoJ8sDDePU2uuUMxoKXkPaP32mikLojxPBeVP6PVSEt4WNHipRz2f5u0Izim6Caxk5J4Nh51/jK6bKGZNPBWLNQL/VJHwSkyPlBIbHStAY="}';
                    // this.paymentCallback(testStr);
                }).catch((error)=>{
                    AlertIOS.alert(strings.alertPrompt,"对不起，数据请求失败：" + error.message);
                }).done();
            }else{
                AlertIOS.alert(strings.alertPrompt,"对不起，当前无法连接网络（offline）");
            }
        });
      }else{
          if(this.state.addressId == 0){
              AlertIOS.alert(strings.alertPrompt,'请选择配送地址'); return;
          }
          var me = this;
          var objData = {
              uid: window.GLOBAL.user_id,
              address_id: this.state.addressId,
              pIds: this.props.pIds
          };
          store.dispatch(postsOrderInfo(objData)).then((json) =>{
            if(json.ServerData[0].paid == false){
              this.props.obj.refreshData(true);
              this.backPage();
            }
          }).catch((error)=>{
              AlertIOS.alert(strings.alertPrompt,"对不起，数据请求失败：" + error.message);
          }).done();
      }
    },
    backPage: function(){
      const { navigator } = this.props;
      if(navigator) {
         navigator.pop();
      }
    },
    AddressManager: function(){
      const { navigator } = this.props;
      if(navigator) {
         navigator.push({
             name: 'AddressList',
             component: AddressList,
             params:{
               refreshAddressData: this.refreshAddress
             }
         });
      }
    },
    SelectInvoice: function(){
      const { navigator } = this.props;
      if(navigator) {
         navigator.push({
             name: 'SelectInvoice',
             component: SelectInvoice,
             params:{
               invoiceStatus: this.state.invoiceStatus,
               invoiceName: this.state.invoiceName,
               selectedInvoices: this.selectedInvoices
             }
         });
      }
    },
    selectedInvoices: function(index,Name){
      if(index == 1){
        this.setState({
            invoiceStatus: strings.lblNoInvoice
        })
      }else if(index == 2){
        this.setState({
            invoiceStatus: strings.lblIndividualInvoice
        })
      }else if(index == 3){
        //Name： 公司名称
        this.setState({
            invoiceName: Name,
            invoiceStatus: strings.lblCompanyInvoices
        })
      }
    },
    render: function () {
      let index = 0;
      const dataPayment = [
          { key: index++, label: '微信' },
          { key: index++, label: '现金' },
          { key: index++, label: '支付宝' },
          { key: index++, label: '银联网银' }
      ];
      const dataDelivery = [
          { key: index++, label: '带走' },
          { key: index++, label: '自取' },
          { key: index++, label: '快递' }
      ];

      var backgroundColor = this.props.isBuyNow ? "#1D1D1D": "#1D1D1D";
      var borderColor = this.props.isBuyNow ? "#1D1D1D": "#1D1D1D";
      var underlayColor = this.props.isBuyNow ? "#1D1D1D": "#1D1D1D";

      var addressView = null;
      if(this.state.addressId == 0){
          addressView = null
      }else{
          addressView = <View style={{marginBottom: 5}}>
              <Text style={{color: 'black'}}>
                  {strings.lblReceiver}<Text style={{color: '＃717171'}}>{this.state.receiver}</Text>{'\r\n'}
                  {strings.lblShippingAddress}<Text style={{color: '＃717171'}}>{this.state.delivery_address}</Text>
              </Text>
          </View>
      }
      return(
        <View style={styles.OrderConfirm}>
            <View style={styles.viewToolbar}>
            <TouchableOpacity onPress={this.backPage} style={[styles.greenButton,{margin:0,marginTop:25,position:'absolute'}]} activeOpacity={0.3}>
                    <Image source={require('image!leftarrow1')} />
            </TouchableOpacity>
              <Text style={styles.txtToolbar}>{strings.lblOrderConfirm}</Text>
            </View>

            <ScrollView style={{borderTopWidth: 1,borderColor: '#E7EAEC',height: (height -100)}}>
                <View style={styles.viewDeliveryMethod}>
                    <Text style={{flex:2,color:'#383838'}}>{strings.lblDeliveryMethod}</Text>
                    <ModalPicker
                        data={dataDelivery}
                        cancelText={strings.alertCancel}
                        selectStyle={[styles.greenButton1,{right:10}]}
                        selectTextStyle={{color:'#baa071'}}
                        initValue={this.state.deliveryString}
                        onChange={(option)=>{ this.setState({deliveryString: option.label}) }} />

                    <Icon name="ios-arrow-forward" size={25} style={{marginRight: 10,marginTop:-4}}/>
                </View>

                <TouchableHighlight onPress={this.AddressManager} underlayColor="#B3B3B3" style={styles.btnAddress}>
                    <View style={[styles.viewDeliveryMethod1,{marginLeft: 0,marginTop:0,marginRight:0,marginBottom:0}]}>
                        <View style={styles.viewDeliveryMethod3}>
                            <Text style={{flex:2,color:'#383838'}}>{strings.lblDeliveryAddress}</Text>
                            <Text style={{position: 'absolute',right: 30}}>{strings.lblSelectDeliveryAddress}</Text>
                            <Icon name="ios-arrow-forward" size={25} style={{marginRight: 10,marginTop:-4}}/>
                        </View>
                        {addressView}
                    </View>
                </TouchableHighlight>

                <View style={styles.viewDeliveryMethod1}>
                    <Text style={{color:'#383838'}}>{strings.lblGoodsList}</Text>
                    <View style={{}}>
                    {
                        this.state.arrayList.map((item,index) => {
                          return (
                            <View key={index} style={styles.row}>
                                <Image
                                    source={{uri: item.product_image}}
                                    resizeMode="contain"
                                    style={styles.cellImage} />
                                <View style={styles.textContainer}>
                                    <Text style={styles.movieTitle} numberOfLines={1}>
                                        {item.name}
                                    </Text>
                                    <View style={styles.viewNumRadius}>
                                      <Text style={styles.movieRating} numberOfLines={1}>
                                          {item.price}
                                      </Text>
                                    </View>
                                    <View style={styles.viewNumQuantity}>
                                      <Text style={styles.movieRating} numberOfLines={1}>
                                          {'x' + item.quantity}
                                      </Text>
                                    </View>
                                </View>
                            </View>
                          );
                        })
                    }
                    </View>
                </View>

                <View style={styles.viewDeliveryMethod}>
                    <Text style={{flex:2,color:'#383838'}}>{strings.lblPaymentType}</Text>
                    <ModalPicker
                        data={dataPayment}
                        cancelText={strings.alertCancel}
                        selectStyle={styles.greenButton1}
                        initValue={this.state.paymentString}
                        onChange={(option)=>{ this.setState({paymentString: option.label}) }} />
                    <Icon name="ios-arrow-forward" size={25} style={{marginRight: 10,marginTop:-4}}/>
                </View>

                <View style={styles.viewDeliveryMethod}>
                    <Text style={{flex:2,color:'#383838'}}>优惠卷</Text>
                    <Text style={{position: 'absolute',right: 30}}>暂不使用优惠卷</Text>
                    <Icon name="ios-arrow-forward" size={25} style={{marginRight: 10,marginTop:-4}}/>
                </View>
                <TouchableHighlight onPress={this.SelectInvoice} underlayColor="#B3B3B3" style={styles.btnAddress}>
                    <View style={[styles.viewDeliveryMethod2,{marginLeft: 0,marginTop:0,marginRight:0,marginBottom:0}]}>
                        <Text style={{flex:2,color:'#383838'}}>{strings.lblInvoice}</Text>
                        <Text style={{position: 'absolute',right: 30}}>{this.state.invoiceStatus}</Text>
                        <Icon name="ios-arrow-forward" size={25} style={{marginRight: 10,marginTop:-4}}/>
                    </View>
                </TouchableHighlight>
                <View>
                    <Text style={styles.txtInfo}>
                        {strings.lblTotal}
                        <Text style={styles.txtRedInfo}>{this.state.total.replace('（','').replace('）','')} </Text>
                        {strings.lblTotalAmount}
                        <Text style={styles.txtRedInfo}>{this.props.grandTotal} </Text>
                        {strings.lblAmountUnit}
                    </Text>
                </View>
            </ScrollView>
            <View style={styles.addorder}>
            <TouchableHighlight onPress={this.btnPayment} style={[styles.button,{backgroundColor: backgroundColor,borderColor: borderColor}]} underlayColor={underlayColor}>
                <Text style={styles.buttonText}>{this.props.isBuyNow ? strings.btnBuyNow: strings.lblSubmitOrder}</Text>
            </TouchableHighlight>
              </View>
        </View>
      );
    }
});
module.exports = OrderConfirm;
