var React = require('react-native');
var styles = require('./../styles/BarcodeScanner');
import Camera from 'react-native-camera'
var Icon = require('react-native-vector-icons/Ionicons');
var {
    Text,
    View,
    Image,
    AlertIOS,
    Dimensions,
    NativeModules,
    TouchableOpacity
} = React;
var strings = require('./../utils/LocalizedStrings');
let { width, height } = Dimensions.get('window');
var QRDecoder = NativeModules.React_native_qr_decoder;
var ImagePicker = require('@remobile/react-native-image-picker');
var intervalTimer = null;
var Barcode = React.createClass({
    getInitialState: function () {
        return {
            torchMode: 'off',
            cameraType: 'back',
            scanningStripTop: 200,  //190 - 410
        };
    },
    barcodeReceived(e) {
        //e.type、e.data
        this.camera.stopCapture();
        window.GLOBAL.isOpenCameraView = false;

        if(window.GLOBAL.isOpenCamera == true){
          window.GLOBAL.isOpenCamera = false;
          // this.props.setTabViewPage();
          this.props.openProductsDetail(e.data + "api/");

          const { navigator } = this.props;
          if(navigator) {
              navigator.pop();
          }
        }
        // this.props.setTabViewPage();
    },
    exitCamera: function(){
      clearInterval(intervalTimer);

      const { navigator } = this.props;
      if(navigator) {
          navigator.pop();
      }
      window.GLOBAL.isOpenCameraView = false;
      this.props.setTabViewPage();
    },
    componentWillMount: function(){
      var me = this;
      intervalTimer = setInterval(function(){
        if(me.state.scanningStripTop <= 355){
          me.setState({
            scanningStripTop: (me.state.scanningStripTop + 2)
          });
        }else{
          me.setState({
            scanningStripTop: 205
          });
        }
      },5);
    },
    QRCodeFromPhoto: function(){
      var options = {maximumImagesCount: 1, quality: 100}; var me = this;

      ImagePicker.getPictures(options, function(results) {
          var path = results[0].replace("file://","");

          // From local File
          QRDecoder.get(path, (error, qrcode) => {
              if (error) {
                  AlertIOS.alert("提示",error.message);
              } else {
                  me.camera.stopCapture();
                  window.GLOBAL.isOpenCameraView = false;

                  if(window.GLOBAL.isOpenCamera == true){
                      window.GLOBAL.isOpenCamera = false;
                      me.props.openProductsDetail(qrcode);

                      const { navigator } = me.props;
                      if(navigator) {
                          navigator.pop();
                      }
                      me.props.setTabViewPage();
                  }
              }
          });
      }, function (error) {
          Alert.alert('Error', error);
      });
    },
    render: function () {
        return (
          <View style={styles.container}>
              <Camera
                ref={(cam) => {this.camera = cam;}}
                style={{ flex: 1}}
                type={Camera.constants.Type.back}
                onBarCodeRead = {this.barcodeReceived}>
              </Camera>

              <Image source={require('image!masklayer')} style={styles.imgMaskLayer}/>
              <View style={styles.viewToolbar}>
                  <TouchableOpacity onPress={this.exitCamera} style={styles.btnBack} activeOpacity={0.5}>
                      <Icon name="chevron-left" size={30} style={styles.viewIcon}/>
                  </TouchableOpacity>
                  <Text style={styles.txtToolbar}>{strings.toolbarTxtQR}</Text>
                  <TouchableOpacity onPress={this.QRCodeFromPhoto} style={styles.btnFromPhoto} activeOpacity={0.5}>
                      <Icon name="image" size={30} style={styles.viewIcon}/>
                  </TouchableOpacity>
              </View>
              <Image source={require('image!scanningstrip')} resizeMode="stretch" style={[styles.imgScanningStrip,{top: this.state.scanningStripTop}]} />
          </View>

        )
    }
});

module.exports = Barcode;
