'use strict';
var React = require('react-native');
var styles = require('./../styles/OrderComment');
var Camera = require('@remobile/react-native-camera');
var ImagePicker = require('@remobile/react-native-image-picker');
import Button from 'apsl-react-native-button';
var {
    View,//div
    Text,//label span
    Image,
    Switch,
    TextInput,
    Dimensions,
    ScrollView,
    AlertIOS,
    TouchableHighlight,
    TouchableOpacity
} = React;
import ActionSheet from 'react-native-actionsheet';

var StarRating = require('./Component/StarRating');
var Icon = require('react-native-vector-icons/Ionicons');
let { width, height } = Dimensions.get('window');
var strings = require('./../utils/LocalizedStrings');

const CANCEL_INDEX = 0;
const DESTRUCTIVE_INDEX = 3;

var OrderComment = React.createClass({
    getInitialState: function () {
        return {
            source: [],
            describe: '',
            descStarRating: 0,
            serviceRating: 0,
            logisticsRating: 0,
            isAnonymous: false,
            isDisabled: false,
            starRatingColor: 'rgb(255, 153, 0)',
            txtDetailBorderColor: '#DADADA',
            toolbarActions: [
              {title: strings.lblBtnReset,show: 'always'}
            ]
        }
    },
    componentDidMount: function() {
      //Sqlite.selectCommentInfo(this.props.orderId,this);
    },
    openPictureType: function(val){
      var me = this;
      if (val == 1) {
          var options = {
              quality: 80,
              allowEdit: true,
              destinationType: Camera.DestinationType.DATA_URL,
          };
          var source = [];
          Camera.getPicture(options, (imageData) => {
              var obj = {source: {uri:'data:image/jpeg;base64,' + imageData}}
              source.push(obj);

              me.setState({
                  source: source
              });
          });
      } else if (val == 2) {
          var options = {maximumImagesCount: 10, quality: 90};
          ImagePicker.getPictures(options, function(results) {
              var source = [];
              for (var i = 0; i < results.length; i++) {

                  var obj = {source: {uri: results[i],isStatic: true}}
                  source.push(obj);
              }
              me.setState({
                  source: source
              });
          }, function (error) {
              console.log('Error：' + error.message);
          });
      }
    },
    addImages: function(){
      if(this.state.isDisabled == true){return;}
      this.ActionSheet.show();
    },
    backPage: function(){
      const { navigator } = this.props;
      if(navigator) {
         navigator.pop();
      }
    },
    btnCommentsSubmit: function(){
      var data = {
        orderId: this.props.orderId,
        descTally: this.state.descStarRating,
        desc: this.state.describe,
        sellerService: this.state.serviceRating,
        logisticsService: this.state.logisticsRating,
        isAnonymous: (this.state.isAnonymous)?1:0
      }
      //Sqlite.addCommentInfo(data,this);
    },
    onActionSelected: function(index){
       if(index == 0){
          this.setState({
            source: [],
            describe: '',
            descStarRating: 0,
            serviceRating: 0,
            logisticsRating: 0,
            isAnonymous: false,
          })
       }
    },
    setCommentDate: function(data){
      this.setState({
        describe: data.desc,
        descStarRating: data.descTally,
        serviceRating: data.sellerService,
        logisticsRating: data.logisticsService,
        isAnonymous: data.isAnonymous,
        isDisabled: true,
        starRatingColor: 'rgb(189, 189, 189)',
        toolbarActions: []
      })
    },
    render: function () {
      const buttons = [strings.alertCancel, strings.lblPhotograph, strings.lblChooseAnAlbum];
      return(
        <View style={styles.container}>
            <View style={{height: 55}}>
              <TouchableOpacity onPress={this.backPage} style={[styles.greenButton,{margin:0,marginTop:25,position:'absolute'}]} activeOpacity={0.3}>
                  <Image source={require('image!leftarrow1')} />
              </TouchableOpacity>
              <Text style={{marginTop:30,marginBottom:15,alignSelf:'center'}}>{strings.lblOrderComment}</Text>
            </View>

            <View style={{borderTopWidth: 1,borderColor: '#E7EAEC'}}>
            <ScrollView>
              <View style={[styles.viewDescription]}>
                  <Image source={{uri: 'http://uniqlo.scene7.com/is/image/UNIQLO/goods_62_151313?$pdp-medium$'}} style={styles.imgProduct}/>
                  <View style={styles.subContainer}>
                      <Text style={styles.txtInfo}>{strings.lblConsistentDescription}</Text>
                      <View style={styles.viewStarRating}>
                        <StarRating
                          maxStars={5}
                          rating={this.state.descStarRating}
                          disabled={this.state.isDisabled}
                          starSize={15}
                          starColor={this.state.starRatingColor}
                          selectedStar={(val)=>{this.setState({descStarRating: val})}}
                        />
                      </View>
                  </View>
              </View>
              <View style={[styles.viewText,{borderColor: this.state.txtDetailBorderColor}]}>
                <TextInput style={styles.txtInput} editable={!this.state.isDisabled} multiline={true} placeholder={strings.lblDescriptionText} numberOfLines={4} onChangeText={(text) => this.setState({describe: text})} value={this.state.describe} onBlur={()=>{this.setState({txtDetailBorderColor: '#DADADA'})}} onFocus={()=>{this.setState({txtDetailBorderColor: '#1AB394'})}}/>
              </View>
              <View style={styles.viewDescription}>
                <View style={styles.viewImageList}>
                    {
                        this.state.source.map(function(result,index){
                            return (<Image key={index} source={result.source} style={styles.imgPicture}/>)
                        })
                    }
                    <TouchableHighlight onPress={this.addImages} underlayColor='#C7C7C7'>
                        <View style={styles.viewAdd}>
                          <Icon name="ios-plus-empty" size={110} style={styles.iconAdd}/>
                        </View>
                    </TouchableHighlight>
                </View>
              </View>
              <View style={styles.viewDescription}>
                  <View style={styles.subContainer}>
                      <Text style={styles.txtInfo}>{strings.lblSellerServices}</Text>
                      <View style={styles.viewStarRating}>
                        <StarRating
                          maxStars={5}
                          rating={this.state.serviceRating}
                          disabled={this.state.isDisabled}
                          starSize={15}
                          starColor={this.state.starRatingColor}
                          selectedStar={(val)=>{this.setState({serviceRating: val})}}
                        />
                      </View>
                  </View>
              </View>
              <View style={styles.viewDescription}>
                  <View style={styles.subContainer}>
                      <Text style={styles.txtInfo}>{strings.lblLogisticsServices}</Text>
                      <View style={styles.viewStarRating}>
                        <StarRating
                          maxStars={5}
                          rating={this.state.logisticsRating}
                          disabled={this.state.isDisabled}
                          starSize={15}
                          starColor={this.state.starRatingColor}
                          selectedStar={(val)=>{this.setState({logisticsRating: val})}}
                        />
                      </View>
                  </View>
              </View>
              <View style={styles.viewDescription}>
                  <View style={styles.subContainer}>
                      <Text style={styles.txtInfo}>{strings.lblAnonymousComments}</Text>
                      <Switch onValueChange={(value) => this.setState({isAnonymous: value})} style={[styles.viewStarRating,{width:50}]} value={this.state.isAnonymous} disabled={this.state.isDisabled}/>
                  </View>
              </View>
              <View style={styles.viewDescriptionBtn}>
                <Button onPress={this.btnCommentsSubmit} style={styles.button} isDisabled={this.state.isDisabled} textStyle={styles.buttonText}>
                    {strings.lblCommentsSubmitted}
                </Button>
              </View>
            </ScrollView>
            </View>
            <ActionSheet
                ref={(o) => this.ActionSheet = o}
                title={strings.titlePicSource}
                options={buttons}
                cancelButtonIndex={CANCEL_INDEX}
                destructiveButtonIndex={DESTRUCTIVE_INDEX}
                onPress={(val)=>{this.openPictureType(val)}}
            />
        </View>
      )
    }
});

module.exports = OrderComment;
