var React = require('react-native');
var styles = require('./../styles/Cart');
var Icon = require('react-native-vector-icons/Ionicons');
import Button from 'apsl-react-native-button';
var TimerMixin = require('react-timer-mixin');
var ItemCheckbox = require('./Component/ItemCheckbox');

var {
    View,
    Text,
    Image,
    Alert,
    ListView,
    ScrollView,
    AsyncStorage,
    AlertIOS,
    TouchableHighlight,
} = React;
import store from './../redux/store';
import { postsGoodsUrl,postsCartInfo,postsUpdateCartInfo } from './../redux/actions/productsAction';
var strings = require('./../utils/LocalizedStrings');

var Cart = React.createClass({
    mixins: [TimerMixin],
    getInitialState: function () {
        return {
            dataSource: new ListView.DataSource({
                getSectionData: function (dataBlob, sectionID) {
                    debugger;
                    return dataBlob[sectionID];
                },
                getRowData: function(dataBlob, sectionID, rowID) {
                    var sectionID = sectionID.split('|')[0];
                    return dataBlob[sectionID][rowID];
                },
                rowHasChanged: (r1, r2) => r1 !== r2,
                sectionHeaderHasChanged: (s1, s2) => s1 !== s2
            }),
            grandTotal: '0.00',
            isDisabled: true,
            totalNum: 0,
            totalProductNum: 0,
            totalProductTypeNum: 0,
            listValues: {},
            pIds: []
        };
    },
    componentDidMount: function() {
      this.refreshData(false);
    },
    refreshData: function(isReturn){
      if(isReturn == false){
        store.dispatch(postsGoodsUrl(ServerURL + "/cart/get-cart-api/" + window.GLOBAL.user_id)).then((json) =>{
            if(isEmptyObject(json.ServerData) == true){
                this.setState({
                    isFirstLoading: false,
                    isDisabled: true,
                    grandTotal: '0.00',
                    totalNum: '',
                    totalProductNum: 0,
                    totalProductTypeNum: 0,
                    dataSource: this.getDataSource([],0,0)
                });
                this.props.setCartTotalNum(0);
            }else{
                var arrays = json.ServerData; var totalNum = 0;
                for(var item in arrays){
                    for (var i = 0; i < arrays[item].length; i++) {
                        totalNum += parseInt(arrays[item][i].quantity);
                    }
                }

                //构建数据结构
                var listValues = {}; var index = 0; var sectionIdentities = []; var rowIdentities = [];
                for(var item in arrays){
                    sectionIdentities.push(item + "|" + index); var rows = [];
                    if(this.state.listValues['s_' + index] == undefined){
                        listValues['s_' + index] = true;
                    }else{
                        listValues['s_' + index] = this.state.listValues['s_' + index];
                    }

                    for (var i = 0; i < arrays[item].length; i++) {
                        if(this.state.listValues['s_' + index + '_r_' + arrays[item][i].id] == undefined){
                            listValues['s_' + index + '_r_' + arrays[item][i].id] = true;
                        }else{
                            listValues['s_' + index + '_r_' + arrays[item][i].id] = this.state.listValues['s_' + index + '_r_' + arrays[item][i].id];
                        }
                        rows.push(i);
                    }
                    rowIdentities.push(rows)
                    index++;
                }

                // 重构：购物车数组
                var arraysCart = arrays;
                if(totalNum <= 0){
                    this.setState({
                        isFirstLoading: false,
                        dataSource: this.getDataSource(arraysCart,sectionIdentities,rowIdentities),
                        listValues: {},
                        totalProductNum: totalNum,
                        totalProductTypeNum: totalNum
                    });
                }else {
                    this.setState({
                        isFirstLoading: false,
                        dataSource: this.getDataSource(arraysCart,sectionIdentities,rowIdentities),
                        listValues: listValues,
                        totalProductNum: totalNum,
                        totalProductTypeNum: totalNum
                    });
                }
                this.finalCalc();
                this.props.setCartTotalNum(totalNum);
            }
        }).catch((error)=>{
            AlertIOS.alert("提示","对不起，数据请求失败：" + error.message);
        }).done();
      }else if(isReturn == true){
        this.setTimeout(
            () => {
              this.refreshData(false);
            },
            500
        );
      }
    },
    subtractionBtn: function(id,totalNum){
        if(totalNum <= 1){
            Alert.alert(
              strings.alertPrompt,
              strings.alertDeleteCart,
              [
                {text: strings.alertCancel, onPress: () => {}},
                {text: strings.alertOk, onPress: () => {
                    var objData = {
                      id: id,
                      quantity: 0,
                    };
                    store.dispatch(postsUpdateCartInfo(objData)).then((json) =>{
                        if(json.ServerData.cart_item_update == true){
                            this.refreshData(false);
                        }
                    }).catch((error)=>{
                        AlertIOS.alert(strings.alertPrompt,"对不起，数据请求失败：" + error.message);
                    }).done();
                }}
              ]
            )
        }else if(totalNum > 1){
            var objData = {
              id: id,
              quantity: (totalNum - 1),
            };
            store.dispatch(postsUpdateCartInfo(objData)).then((json) =>{
                if(json.ServerData.cart_item_update == true){
                    this.refreshData(false);
                }
            }).catch((error)=>{
                AlertIOS.alert(strings.alertPrompt,"对不起，数据请求失败：" + error.message);
            }).done();
        }
    },
    additionBtn: function(id,totalNum){
        var objData = {
          id: id,
          quantity: (totalNum + 1),
        };
        store.dispatch(postsUpdateCartInfo(objData)).then((json) =>{
            if(json.ServerData.cart_item_update == true){
                this.refreshData(false);
            }
        }).catch((error)=>{
            AlertIOS.alert(strings.alertPrompt,"对不起，数据请求失败：" + error.message);
        }).done();
    },
    getDataSource: function(subjects: Array<any>,sectionIdentities: Array<any>,rowIdentities: Array<any>): ListView.DataSource {
        return this.state.dataSource.cloneWithRowsAndSections(subjects,sectionIdentities,rowIdentities);
    },
    renderFooter: function(){
      if(this.state.totalProductNum <= 0){
          return(
              <View style={styles.viewEmptyData}>
                  <Text style={styles.txtEmptyData}>{strings.tipCartEmpty}</Text>
              </View>
          );
      }
    },
    renderSectionHeader: function(sectionData, sectionID) {
      var index = parseInt(sectionID.split('|')[1]);
      return(
          <View style={styles.viewCatalog}>
              <ItemCheckbox ref={(obj)=> this["s_" + index] = obj} size={25} checked={this.state.listValues["s_" + index ]} data={sectionID} color="#baa071" onCheck={this.onCheckCategoryCallback}/>
              <Icon name="ios-box" size={25} style={{marginLeft: 5,marginTop: 0,backgroundColor:'transparent'}}/>
              <Text style={styles.txtRowTitle}>{sectionID.split('|')[0] + "："}</Text>
          </View>
      );
    },
    renderRow: function(rowData, sectionID, rowID, highlightRowFunc){
        var sectionID = parseInt(sectionID.split('|')[1]);

        return (
          <TouchableHighlight
            key={rowData.id}
            underlayColor="#E6E6E6aa">
              <View style={styles.row}>
                  <ItemCheckbox keyId={rowData.id} ref={(obj)=> this["s_" + sectionID +"_r_" + rowData.id] = obj} price={rowData.price} quantity={rowData.quantity} checked={this.state.listValues["s_" + sectionID +"_r_" + rowData.id]} data={"s_" + sectionID +"_r_" + rowData.id} size={25} onCheck={this.onCheckItemCallback}/>
                  <Image
                      source={{uri: ServerURL + rowData.product_image}}
                      resizeMode="contain"
                      style={styles.cellImage} />
                  <View style={styles.textContainer}>
                      <Text style={styles.movieTitle} numberOfLines={1}>
                          {rowData.name}
                      </Text>
                      <View style={styles.viewNumRadius}>
                        <Text style={styles.movieRating} numberOfLines={1}>
                            {rowData.price}
                        </Text>
                      </View>
                  </View>
                  <View style={styles.viewToDetail}>
                    <TouchableHighlight onPress={()=>{this.subtractionBtn(rowData.id,rowData.quantity) }} style={styles.greenButton} underlayColor='#F3F3F3'>
                        <Icon name="minus" size={17} style={styles.greenButtonText}/>
                    </TouchableHighlight>
                    <View style={styles.viewNum}>
                      <Text style={styles.textNum}>{rowData.quantity}</Text>
                    </View>
                    <TouchableHighlight onPress={()=>{this.additionBtn(rowData.id,rowData.quantity)}} style={styles.greenButton} underlayColor='#F3F3F3'>
                        <Icon name="plus" size={17} style={styles.greenButtonText}/>
                    </TouchableHighlight>
                  </View>
              </View>
            </TouchableHighlight>
        );
    },
    //单项选择
    onCheckItemCallback: function(value,rowID){
        try{
            if(value == true){
                var sectionID = rowID.substr(0,rowID.indexOf("_r_"));

                var listValues = this.state.listValues; listValues[rowID] = true;
                var isCategoryAll = true; var isCheckedAll = true;

                for (var item in listValues) {
                    if(item.includes(sectionID + "_r_") == true && listValues[item] == false){
                        isCategoryAll = false; break;
                    }
                }
                if(isCategoryAll == true){
                    this[sectionID].setValue(true);
                    listValues[sectionID] = true;
                }
                this.setState({listValues: listValues});

                //所有的选项框
                for (var item in listValues) {
                    if(listValues[item] == false){
                        isCheckedAll = false; break;
                    }
                }
                if(isCheckedAll == true){
                    this['itemCheckAll'].setValue(true);
                }
            }else if(value == false){
                var listValues = this.state.listValues; listValues[rowID] = false;
                for (var item in listValues) {
                    if(item == (rowID.substr(0,rowID.indexOf("_r_")))){
                        this[item].setValue(false);
                        listValues[item] = false;
                    }
                }
                this.setState({listValues: listValues});
                this['itemCheckAll'].setValue(false);
            }
            this.finalCalc();
        }catch(e){}
    },
    //分类选择
    onCheckCategoryCallback: function(value,id){
        try{
            var sectionID = id.split('|')[1];
            if(value == true){
                var listValues = this.state.listValues;
                for (var item in listValues) {
                    if(item.includes("s_" + sectionID) == true){
                        this[item].setValue(true);
                        listValues[item] = true;
                    }
                }
                this.setState({listValues: listValues});
                //所有都选上后，全选勾上
                var checkedAll = true;
                for (var item in listValues) {
                    if(listValues[item] == false){
                        checkedAll = false; break;
                    }
                }
                if(checkedAll){
                    this['itemCheckAll'].setValue(true);
                }
            }else if(value == false){
                var listValues = this.state.listValues;
                for (var item in listValues) {
                    if(item.includes("s_" + sectionID) == true){
                        this[item].setValue(false);
                        listValues[item] = false;
                    }
                }
                this.setState({listValues: listValues});
                this['itemCheckAll'].setValue(false);
            }
            this.finalCalc();
        }catch(e){}
    },
    //全部选择
    onCheckAllCallback: function(value,id){
        try{
            if(value == true){
                var listValues = this.state.listValues;
                for (var item in listValues) {
                    if(item.includes("s_") == true){
                        this[item].setValue(true);
                        listValues[item] = true;
                    }
                }
                this.setState({listValues: listValues});
            }else if(value == false){
                var listValues = this.state.listValues;
                for (var item in listValues) {
                    if(item.includes("s_") == true){
                        this[item].setValue(false);
                        listValues[item] = false;
                    }
                }
                this.setState({listValues: listValues});
            }
            this.finalCalc();
        }catch(e){}
    },
    finalCalc: function(){
        var listValues = this.state.listValues; var totalNum = 0; var totalProductTypeNum = 0; var grandTotal = 0; var pIds = [];
        for (var item in listValues) {
            if(item.includes("s_") == true && item.includes("_r_") == true){
                if(listValues[item] == true){
                    totalNum += this[item].props.quantity; totalProductTypeNum++;
                    grandTotal += this[item].props.quantity * parseFloat(this[item].props.price);
                    pIds.push(this[item].props.keyId);
                }
            }
        }
        if(totalNum <= 0){
            this.setState({
                totalNum: '',
                isDisabled: (totalNum <= 0)? true: false,
                grandTotal: grandTotal.toFixed(2),
                pIds: pIds,
                totalProductTypeNum: totalProductTypeNum
            });
        }else{
            this.setState({
                totalNum: '（' + totalNum + '）',
                isDisabled: (totalNum <= 0)? true: false,
                grandTotal: grandTotal.toFixed(2),
                pIds: pIds,
                totalProductTypeNum: totalProductTypeNum
            });
        }
        //检查 并 修正 全选的状态
        if(this.state.totalProductNum > 0){
            var isCheckedAll = true;
            for (var item in listValues) {
                if(listValues[item] == false){
                    isCheckedAll = false; break;
                }
            }
            if(isCheckedAll == true){
                this['itemCheckAll'].setValue(true);
            }
        }
    },
    render: function () {
        var bottomView = null;
        if(this.props.isDeleteStatus == strings.btnEdit){
          bottomView =
            <View style={styles.viewBottomInfo}>
                <View style={styles.viewSettleAccounts}>
                    <Text>
                        {strings.txtTotal}<Text style={{color: 'red'}}>￥ {this.state.grandTotal}</Text>
                    </Text>
                    <Text style={{alignSelf: 'flex-end'}}>{strings.txtExcluding}</Text>
                </View>

                <Button onPress={()=>{this.props.commodityClearing(this.state.grandTotal,this.state.totalNum,this.state.pIds,this,false)}} style={styles.button} underlayColor='#169E82' isDisabled={this.state.isDisabled} textStyle={styles.buttonText} disabledStyle={{backgroundColor:'#CBCBCB',width:103}}>
                  {strings.formatString(strings.lblToTheSettlement,this.state.totalNum)  }
                </Button>
            </View>
        }else{
            bottomView =
            <View style={styles.viewBottomInfo}>
                <Button onPress={()=>{this.props.deleteCartItems(this.state.totalProductTypeNum,this.state.pIds)}} style={styles.deleteButton} underlayColor='#EFB64B' textStyle={styles.buttonText}>
                  {strings.btnDeleted}
                </Button>
            </View>
        }
        return (
          <View style={[styles.container,{borderTopWidth: 1,borderColor:'#E7EAEC'}]}>
              <ScrollView style={styles.scrollView}>
                  <View style={styles.listView}>
                      <ListView
                          ref="ListView"
                          dataSource={this.state.dataSource}
                          renderRow={this.renderRow}
                          renderSectionHeader={this.renderSectionHeader}
                          renderFooter={this.renderFooter}
                          automaticallyAdjustContentInsets={false}
                          keyboardDismissMode="on-drag"
                          keyboardShouldPersistTaps={true}
                          showsVerticalScrollIndicator={false}
                        />
                  </View>
              </ScrollView>
              <View style={styles.viewBottom}>
                  <View style={styles.viewCheckedAll}>
                      <ItemCheckbox size={25} ref={(obj)=>this.itemCheckAll = obj} checked={true} color="#baa071" onCheck={this.onCheckAllCallback}/>
                      <Text style={styles.txtCheckboxAll}>{strings.checkedTxt}</Text>
                  </View>
                  {
                      bottomView
                  }
              </View>
          </View>
        );
    }
});
module.exports = Cart;
