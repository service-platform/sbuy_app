//导入容器 combineReducers
import { combineReducers } from 'redux';
import { productsReducer } from './reducers/productsReducer';
import { loginReducer } from './reducers/loginReducer';

//创建一个跟还原剂
const rootReducer = combineReducers({
  loginReducer,
  productsReducer
});
//导出跟还原剂
export default rootReducer;
