//导入常量
import * as types from '../constants';

//处理数据块 此处按照页面来分action
function receivePosts(actionType,json) {
    var returnData = {
        type: actionType,
        ServerData: json
    };
    return returnData;
}
//fetch 请求方法 dispatch：回调参数，相当于：(store.dispatch)
export function postsLogin(data) {
    return dispatch => {
        //POST 提交方式2：有框架，服务器，拆分键值对(types.SERVER_IP)
        return fetch(ServerURL + `/account/api-login/`,{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            body: JSON.ObjectToString(data)
        })
        .then(response => response.json())
        .then(json => dispatch(receivePosts(types.POST_LOGIN,json)))
    }
}

export function userRegister(data) {
    return dispatch => {
        //POST 提交方式2：有框架，服务器，拆分键值对(types.SERVER_IP)
        return fetch(ServerURL + `/account/register-api/`,{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            body: JSON.ObjectToString(data)
        })
            .then(response => response.json())
            .then(json => dispatch(receivePosts(types.POST_REGIST,json)))
    }
}
