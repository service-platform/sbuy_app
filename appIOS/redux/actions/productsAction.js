//导入常量
import * as types from '../constants';

//处理数据块 此处按照页面来分action
function receivePosts(actionType,json) {
    var returnData = {
        type: actionType,
        ServerData: json
    };
    return returnData;
}
//通过外部传输 URL 获得值
export function postsGoodsUrl(url) {
    return dispatch => {
        //POST 提交方式2：有框架，服务器，拆分键值对(types.SERVER_IP)
        return fetch(`${ url }`,{
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        })
        .then(response => response.json())
        .then(json => dispatch(receivePosts(types.GET_INFO,json)))
    }
}
//添加，删除 收藏（云端请求）
export function postsFavorites(data) {
    return dispatch => {
        //POST 提交方式2：有框架，服务器，拆分键值对(types.SERVER_IP)
        return fetch(ServerURL + `/add-wish/`,{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            body: JSON.ObjectToString(data)
        })
        .then(response => response.json())
        .then(json => dispatch(receivePosts(types.GET_INFO,json)))
    }
}
//添加购物车（修改购物车：update = true,delete = true）
export function postsCartInfo(data) {
    return dispatch => {
        //POST 提交方式2：有框架，服务器，拆分键值对(types.SERVER_IP)
        return fetch(ServerURL + `/cart/add-api/`,{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            body: JSON.ObjectToString(data)
        })
        .then(response => response.json())
        .then(json => dispatch(receivePosts(types.GET_INFO,json)))
    }
}
//修改购物车数量
export function postsUpdateCartInfo(data) {
    return dispatch => {
        //POST 提交方式2：有框架，服务器，拆分键值对(types.SERVER_IP)
        return fetch(ServerURL + `/cart/update-api/`,{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            body: JSON.ObjectToString(data)
        })
        .then(response => response.json())
        .then(json => dispatch(receivePosts(types.GET_INFO,json)))
    }
}
//批量删除购物车
export function postsDeleteCartInfo(data) {
    return dispatch => {
        //POST 提交方式2：有框架，服务器，拆分键值对(types.SERVER_IP)
        return fetch(ServerURL + `/cart/remove-api/`,{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            body: JSON.ObjectToString(data)
        })
        .then(response => response.json())
        .then(json => dispatch(receivePosts(types.GET_INFO,json)))
    }
}
//生成订单信息
export function postsOrderInfo(data) {
    return dispatch => {
        //POST 提交方式2：有框架，服务器，拆分键值对(types.SERVER_IP)
        return fetch(ServerURL + `/orders/create-api/`,{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            body: JSON.ObjectToString(data)
        })
        .then(response => response.json())
        .then(json => dispatch(receivePosts(types.GET_INFO,json)))
    }
}
//立即购买商品
export function postsBuyNow(data) {
    return dispatch => {
        //POST 提交方式2：有框架，服务器，拆分键值对(types.SERVER_IP)
        return fetch(ServerURL + `/orders/immediately/`,{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            body: JSON.ObjectToString(data)
        })
        .then(response => response.json())
        .then(json => dispatch(receivePosts(types.GET_INFO,json)))
    }
}
//获取订单列表 API
export function getOrderList(user_id) {
    return dispatch => {
        //POST 提交方式2：有框架，服务器，拆分键值对(types.SERVER_IP)
        return fetch(ServerURL + `/orders/get-api/` + user_id +`/`,{
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        })
        .then(response => response.json())
        .then(json => dispatch(receivePosts(types.GET_INFO,json)))
    }
}
//地址管理：（添加地址）
export function postsAddAddress(data) {
    return dispatch => {
        //POST 提交方式2：有框架，服务器，拆分键值对(types.SERVER_IP)
        return fetch(ServerURL + `/account/address/add/api/`,{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            body: JSON.ObjectToString(data)
        })
        .then(response => response.json())
        .then(json => dispatch(receivePosts(types.GET_INFO,json)))
    }
}
//地址管理：（获取地址列表）
export function getGetAddressList(user_id) {
    return dispatch => {
        //POST 提交方式2：有框架，服务器，拆分键值对(types.SERVER_IP)
        return fetch(ServerURL + `/account/get/address/api/` + user_id + `/`,{
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        })
        .then(response => response.json())
        .then(json => dispatch(receivePosts(types.GET_INFO,json)))
    }
}
//地址管理：（更新地址信息）
export function getUpdateAddressInfo(data) {
    return dispatch => {
        //POST 提交方式2：有框架，服务器，拆分键值对(types.SERVER_IP)
        return fetch(ServerURL + `/account/address/update/api/`,{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            body: JSON.ObjectToString(data)
        })
        .then(response => response.json())
        .then(json => dispatch(receivePosts(types.GET_INFO,json)))
    }
}
//地址管理：（删除地址信息）
export function getDeleteAddressById(addressId) {
    return dispatch => {
        //POST 提交方式2：有框架，服务器，拆分键值对(types.SERVER_IP)
        return fetch(ServerURL + `/account/address/delete/api/`,{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            body: JSON.ObjectToString(addressId)
        })
        .then(response => response.json())
        .then(json => dispatch(receivePosts(types.GET_INFO,json)))
    }
}
//fetch 请求方法 dispatch：回调参数，相当于：(store.dispatch)
export function getPaymentInfo(url) {
    return dispatch => {
        //POST 提交方式2：有框架，服务器，拆分键值对(types.SERVER_IP)
        return fetch(`${ url }`,{
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        })
        .then(response => response.json())
        .then(json => dispatch(receivePosts(types.GET_PAYMENT_INFO,json)))
    }
}
//fetch 请求方法 dispatch：回调参数，相当于：(store.dispatch)
export function postPlaymentResult(url,data) {
    return dispatch => {
        //POST 提交方式2：有框架，服务器，拆分键值对(types.SERVER_IP)
        return fetch(`${ url }`,{
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            body: JSON.ObjectToString(data)
        })
        .then(response => response.json())
        .then(json => dispatch(receivePosts(types.POST_PLAYMENT_RESULT,json)))
    }
}
