//服务器 IP 地址
export const SERVER_IP = 'http://192.168.1.31:8678';

//指令（Action）  此处 前后台  方法名必须写一样，方便维护
export const POST_LOGIN = 'POST_LOGIN';
export const GET_INFO = 'GET_INFO';
export const GET_PAYMENT_INFO = 'GET_PAYMENT_INFO';
export const POST_PLAYMENT_RESULT = 'POST_PLAYMENT_RESULT';
export const POST_REGIST = 'POST_REGIST';
