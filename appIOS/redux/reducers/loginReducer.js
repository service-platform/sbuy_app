//导入常量  后期开发  一个页面一个总fucntion ，下面多个 业务逻辑 比如CRUD，这个就可以细分
import * as types from '../constants';

//数据返回，追加时间
export function loginReducer (state = {}, action = {}) {
    switch (action.type) {
        case types.POST_LOGIN:
            var date = new Date();
            return Object.assign({}, action, {
                type: types.POST_LOGIN,
                ResponseTime: date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds() + "." + date.getMilliseconds()
            })
        case types.POST_REGIST:
            var date = new Date();
            return Object.assign({}, action, {
                type: types.GET_INFO,
                ResponseTime: date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds() + "." + date.getMilliseconds()
            })
        default:
            return state
    }
}
