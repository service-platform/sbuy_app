var React = require('react-native');
var {
  StyleSheet,
  Dimensions
} = React;
let { width, height } = Dimensions.get('window');

var localStyles = StyleSheet.create({
  container: {
    paddingLeft: 10,
    paddingRight: 10,
    height: height,
    width: width,
    alignItems: 'center',
    backgroundColor: 'rgb(241,240,248)',
  },
  viewToolbar:{
    height: 55
  },
  btnToolbarRight: {
    height: 27,
    width: 50,
    flex: 1,
    marginTop:25,
    backgroundColor: '#1AB394',
    borderColor: '#1AB394',
    borderWidth: 1,
    borderRadius: 4,
    marginBottom: 10,
    marginLeft: 7,
    alignSelf: 'stretch',
    justifyContent: 'center',

    margin:0,
    position:'absolute'
  },
  txtToolbar:{
    marginTop:30,
    marginBottom:15,
    alignSelf:'center'
  },
  txtPaymentTypes:{
    marginRight:10
  },
  txtOrderDetail:{
    backgroundColor: 'transparent',
    textAlignVertical: 'top'  //文字上下居中
  },
  txtInfo:{
    alignSelf:'center',
    color: 'black',
    marginTop: 10,
    marginBottom:10
  },
  txtRedInfo:{
    color: 'red'
  },
  button: {
    width: 200,
    height: 35,
    backgroundColor:"#f23030",
    justifyContent:"center",
    alignItems:"center",
    flexDirection:"row"
  },
  buttonText: {
    fontSize: 18,
    color: '#F3F3F3',
    alignSelf: 'center'
  },
  viewDeliveryMethod: {
    flexDirection: 'row',
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    borderColor: '#BAA071',
    backgroundColor: 'white',
    borderLeftWidth: 5,
    paddingTop: 7,
    paddingLeft: 10,
    height: 30,
  },
  viewDeliveryMethod1: {
    flexDirection: 'column',
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    borderColor: '#BAA071',
    backgroundColor: 'rgb(255,255,255)',
    borderLeftWidth: 5,
    paddingTop: 7,
    paddingLeft: 10,
  },
  viewDeliveryMethod2: {
    flexDirection: 'row',
    borderColor: '#BAA071',
    backgroundColor: 'rgb(255,255,255)',
    borderLeftWidth: 5,
    paddingTop: 7,
    paddingLeft: 10,
    height: 30,
  },
  viewDeliveryMethod3: {
    flexDirection: 'row',
    backgroundColor: 'transparent',
    height: 28,
  },
  btnAddress: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
  },
  row: {
      alignItems: 'center',
      backgroundColor: 'white',
      flexDirection: 'row',
      paddingLeft: 5,
      paddingRight: 5,
      marginLeft: 2,
      marginRight: 5,
      marginBottom: 5,
      marginTop: 5,
      borderRadius: 0,
      borderColor: '#ECECEC',
      borderWidth: 1,

      shadowColor: '#e9e9e9',
      shadowOffset: {
          width: 0,
          height: 0
      },
      shadowOpacity: 0.5,
      shadowRadius: 6,
      elevation: 5
  },
  cellImage: {
      backgroundColor: 'white',
      height: 70,
      marginRight: 10,
      width: 50
  },
  movieTitle: {
      fontSize: 18,
      fontWeight: '500',
      marginBottom: 2,
      color: 'black',
      width: (width - 130)
  },
  movieRating: {
      marginTop: 0,
      color: 'white',
      fontSize: 14
  },
  viewNumRadius:{
    backgroundColor: '#78767B',
    height: 20,
    borderRadius: 3,
    paddingLeft: 5,
    paddingRight: 5,
    marginTop: 3
  },
  viewNumQuantity: {
    backgroundColor: '#baa071',
    height: 20,
    borderRadius: 3,
    paddingLeft: 5,
    paddingRight: 5,
    right: -10,
    marginTop: -26,
    position: 'absolute'
  },
  greenButtonText: {
    color: 'white',
    alignSelf: 'center'
  },
  greenButton1: {
    height: 27,
    flex: 1,
    paddingLeft: 10,
    paddingRight: 10,
    alignSelf: 'stretch',
    justifyContent: 'center',
    top:-7,
    right:8,
    position:'absolute',
    borderWidth:0
  },
  greenButton: {
    height: 20,
    width: 25,
    flex: 1,
    marginTop:6,
    marginBottom: 10,
    marginLeft: 7,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
  addorder:{
    width:width,
    height:45,
    backgroundColor:"#f8f8f8",
    position:'absolute',
    bottom:-45,
    borderTopWidth:1,
    borderTopColor:'#e0e0e0',
    justifyContent:"center",
    alignItems:"center"
  }
});
module.exports = localStyles;
