var React = require('react-native');
var {
  StyleSheet,
  Dimensions
} = React;
let { width, height } = Dimensions.get('window');

var localStyles = StyleSheet.create({
  container: {
    paddingLeft: 10,
    paddingRight: 10,
    height: height,
    width: width,
    alignItems: 'center',
    backgroundColor: 'rgb(241,240,248)',
  },
  greenButton: {
    height: 20,
    width: 25,
    flex: 1,
    marginTop:6,
    marginBottom: 10,
    marginLeft: 7,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
  greenButtonText: {
    color: 'white',
    alignSelf: 'center'
  },
  button: {
    height: 36,
    width: (width - 40),
    flex: 1,
    marginTop:10,
    flexDirection: 'row',
    backgroundColor: '#1AB394',
    borderColor: '#1AB394',
    borderWidth: 1,
    borderRadius: 2,
    marginBottom: 10,
    alignSelf: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
  scrollview:{
    marginLeft: 5,
    marginRight: 5,
    height: (height - 160)
  },
  row:{
    marginLeft: 3,
    borderColor: 'rgb(236, 236, 236)',
    borderBottomWidth: 1,
    paddingBottom: 7,
    paddingTop: 5
  },
  viewContent:{
    marginLeft: 0,
    paddingLeft: 10,
    borderColor: '#BAA071',
    borderLeftWidth: 3
  },
  processStatus:{
    color:'rgb(66,66,66)',
    marginLeft:10,
    marginRight:10
  },
  viewProcessStatus:{
    backgroundColor:'rgb(218,218,218)',
    borderRadius:4,
    alignItems:'center',
    padding: 3,
    position:'absolute',
    right:15,
    top:0
  },
  btnProcessStatus:{
    backgroundColor:'rgb(218,218,218)',
    borderRadius:4,
    alignItems:'center',
    padding: 3,
    position:'absolute',
    top:0,
    marginTop:29,
    right:16,
  },
  OrderConfirm:{
    height:(height),
    backgroundColor: '#FFFFFF',
    borderTopWidth: 1,
    borderColor:'#E7EAEC'
  }
});
module.exports = localStyles;
