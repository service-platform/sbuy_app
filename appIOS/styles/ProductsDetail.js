var React = require('react-native');
var {
  StyleSheet,
  Dimensions
} = React;
let { width, height } = Dimensions.get('window');

var localStyles = StyleSheet.create({
    container:{
      alignItems:"center"
    },
    image:{
      width:200,
      height:300,
      marginTop:10,
      marginBottom:10
    },
    txtView:{
      padding: 5,
    },
    txtName:{
      fontSize: 16,
      fontWeight: 'bold'
    },
    price:{
      fontSize: 16,
      color:'red'
    },
    desc:{
      color:'rgb(182,182,182)'
    },
    toolbar: {
      backgroundColor: '#4D77D8',
      height: 55,
    },
    viewBottom:{
      position: 'absolute',
      top: (height - 65),
      left: 0,
      height:40,
      width: width,
      borderColor: 'rgb(208,208,208)',
      borderTopWidth: 1,
      borderBottomWidth: 1,
      backgroundColor: 'rgb(247,247,247)',
      flexDirection: 'row',
      flexWrap: 'nowrap'
    },
    viewNum:{
      flex: 2
    },
    viewTxtNum:{
      alignSelf:'center',
      marginTop:10
    },
    button: {
      height: 27,
      width: 50,
      flex: 1,
      marginTop:6,
      backgroundColor: 'rgb(76,218,101)',
      borderColor: 'rgb(66, 185, 87)',
      borderWidth: 1,
      borderRadius: 4,
      marginBottom: 10,
      marginLeft: 7,
      alignSelf: 'stretch',
      justifyContent: 'center'
    },
    redButton: {
      height: 27,
      width: 50,
      flex: 1,
      marginTop:6,
      backgroundColor: '#1d1d1d',
      borderColor: '#1d1d1d',
      borderWidth: 1,
      borderRadius: 4,
      marginBottom: 10,
      marginLeft: 7,
      marginRight: 7,
      alignSelf: 'stretch',
      justifyContent: 'center'
    },
    buttonText: {
      fontSize: 24,
      color: 'white',
      alignSelf: 'center'
    },
});
module.exports = localStyles;
