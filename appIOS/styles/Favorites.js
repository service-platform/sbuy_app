var React = require('react-native');
var {
  StyleSheet,
  Dimensions
} = React;
let { width, height } = Dimensions.get('window');

var localStyles = StyleSheet.create({
  listView:{
    borderTopWidth: 1,
    borderColor:'#E7EAEC'
  },
  separator:{
    paddingTop: 10,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  touchableHighlight: {
      backgroundColor: 'white',
      marginBottom: 10,
      marginLeft: parseInt((width-((parseInt(width/2)-20)*2))/3)
  },
  row: {
      alignItems: 'center',
      backgroundColor: 'white',
      height: 205,
      shadowColor: '#666666',
      shadowOffset: {
          width: 0,
          height: 0
      },
      shadowOpacity: 0.5,
      shadowRadius: 4,
      elevation: 8
  },
  cellImage: {
      backgroundColor: 'white',
      height: 160,
      width: parseInt(width/2)-20,
  },
  movieTitle: {
      fontSize: 15,
      fontWeight: '500',
      width:280,
      color: '#666'
  },
  movieRating: {
      color: 'white',
      fontFamily: 'Arial',
      marginLeft:10,
      marginRight:10,
      paddingTop: 2
  },
  closeBtn:{
    position:'absolute',
    top: 3,
    right:5,
  },
  closeIcon:{
    color:'#baa071'
  },
  viewToDetail:{
    position: 'absolute',
    marginTop: 20,
    marginLeft: 7,
  },
  viewNum:{
    backgroundColor: '#78767B',
    height: 20,
    borderRadius: 3
  },
  textContainer:{
    position: 'absolute',
    marginTop: 0,
    paddingLeft: 7,
    backgroundColor: 'transparent'
  },
});
module.exports = localStyles;
