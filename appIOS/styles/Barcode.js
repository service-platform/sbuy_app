var React = require('react-native');
var {
  StyleSheet,
  Dimensions
} = React;
let { width, height } = Dimensions.get('window');

var localStyles = StyleSheet.create({
  container: {
    paddingLeft: 10,
    paddingRight: 10,
    height: (height - 128),
    width: width,
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    borderTopWidth: 1,
    borderColor: '#E7EAEC'
  },
  backButton: {
    height: 20,
    width: 25,
    flex: 1,
    marginTop:6,
    marginBottom: 10,
    marginLeft: 7,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
  toolbar: {
    backgroundColor: '#F3F3F4',
    height: 55,
  },
  viewToolbar:{
    height: 55,
    backgroundColor: 'white'
  },
  txtBGToolbar:{
    marginTop:30,
    marginBottom:15,
    alignSelf:'center',
    color:'black',
    fontWeight: 'bold',
    fontSize: 20
  },
  txtToolbar:{
    marginTop:30,
    marginBottom:15,
    alignSelf:'center',
    color:'white',
    fontWeight: 'bold',
    fontSize: 20
  },
  btnToolRight:{
    height: 27,
    width: 50,
    flex: 1,
    marginTop:25,
    backgroundColor: '#1d1d1d',
    borderRadius: 4,
    marginBottom: 10,
    marginLeft: 7,
    alignSelf: 'stretch',
    justifyContent: 'center',
    margin:0,
    right:8,
    top:0,
    position:'absolute'
  },
  scrollableTabView:{
    backgroundColor: '#2F4050'
  },
  fieldView: {
    position: 'absolute',
    height:40,
    width:260,
    marginBottom: 10,
    top: 0,
    left: 0,
    flexDirection: 'row',
    backgroundColor: 'rgb(255,255,255)',
    borderColor: '#E5E6E7',
    borderBottomWidth: 1,
    borderRightWidth: 1
  },
  imageHighlight:{
    alignSelf: 'center',
    backgroundColor: 'rgb(255,255,255)'
  },
  imageIcon:{
    width:32,
    height:40,
    backgroundColor: 'rgb(255,255,255)'
  },
  searchInput: {
    alignSelf: 'center',
    marginLeft: 3,
    borderWidth: 0,
    width:220,
    height:25
  },
  button: {
    height: 40,
    width: (width-260),

    backgroundColor: '#1d1d1d',
    borderColor: '#1d1d1d',
    marginBottom: 10,
    justifyContent: 'center'
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
  tabView: {
    flex: 1,
    padding: 0,
    backgroundColor:'#FFFFFF'
  },
  card: {
    borderWidth: 1,
    backgroundColor: '#fff',
    borderColor: 'rgba(0,0,0,0.1)',
    margin: 5,
    height: 150,
    padding: 15,
    shadowColor: '#ccc',
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.5,
    shadowRadius: 3,
  },
  //------------------------------------------------------------
  floatView:{
    backgroundColor:'white',
    width: width,
    height: (height - 73),
    position: 'absolute',
    top: 0,
    left: 0,
  },
  floatViewBack:{
    backgroundColor:'black',
    width: width,
    height: (height - 73),
    position: 'absolute',
    top: 0,
    left: 0,
  },
  floatViewHeaderIcon:{
    position: 'absolute',
    top: 0,
    left: 0,
    width:115,
    height:105
  },
  floatWebView:{
    backgroundColor:'white',
    width: width,
    height: (height-48),
    position: 'absolute',
    top: 0,
    left: 0,
  },
  subContainer:{
    alignItems:"center",
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderColor: '#E7EAEC',
  },
  imgDetail:{
    width: width,
    height:300,
    marginTop:10,
    marginBottom:10,
    borderColor: '#D2D2D2',
    borderBottomWidth: 1,
    backgroundColor: 'white',
  },
  txtView:{
    padding: 5,
    borderColor: '#D2D2D2',
    borderLeftWidth: 1
  },
  txtName:{
    paddingLeft: 0,
    fontSize: 18,
    fontWeight: 'bold'
  },
  price:{
    paddingLeft: 0,
    fontSize: 16,
    color:'#4EA5F1'
  },
  desc:{
    color:'black',
    paddingLeft: 15,
    paddingRight: 15,
    marginBottom: 10
  },
  lblShowMoreDetails:{
    fontSize: 16,
    color:'#4EA5F1',
    paddingLeft: 15,
    marginBottom: 20,
  },
  viewBottom:{
    position: 'absolute',
    top: (height - 88),
    left: 0,
    height:40,
    width: width,
    backgroundColor: 'rgb(247,247,247)',
    flexDirection: 'row',
    flexWrap: 'nowrap'
  },
  viewNum:{
    flex: 2
  },
  viewTxtNum:{
    alignSelf:'center',
    marginTop:10,
    color: 'black'
  },
  greenButton: {
    height: 35,
    width: 35,
    flex: 1,
    marginLeft: 5,

    alignSelf: 'stretch',
    justifyContent: 'center'
  },
  redButton: {
    height: 33,
    width: 50,
    flex: 1,
    marginTop:3,
    backgroundColor: '#1d1d1d',
    borderColor: '#1d1d1d',
    borderWidth: 1,
    marginBottom: 10,
    marginLeft: 7,
    marginRight: 7,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
  greenButtonText: {
    color: 'white',
    alignSelf: 'center'
  },
  animatedScrollView:{
    flex:1,
    height: (height-260)
  },
  animatedViewDetail:{
    marginTop: 15,
    marginLeft: 120,
    width:200
  },
  animatedDetailFontRed:{
    color: 'red'
  },
  animatedDetailFontBlack:{
    color: 'black'
  },
  txtFontWhite:{
    color: 'white',
    alignSelf: 'center'
  },
  viewSplitline:{
    borderTopWidth: 1,
    borderColor: 'rgb(240,240,240)',
    marginTop: 6,
    margin: 10
  },
  viewQuantity:{
    flexDirection: 'row',
    position: 'absolute',
    right: 15,
    marginTop: -27,
  },
  txtTitleQuantity:{
    flex:2,
    marginTop:10,
    marginLeft:10
  },
  viewSubQuantity:{
    flex:1,
    flexDirection: 'row',
    borderColor: '#BDBDBD',
    borderWidth: 1
  },
  subtractionBtn:{
    backgroundColor: '#F5F5F5',
    width:36,
    height:30,
    justifyContent:'center'
  },
  iconQuantity:{
    color: '#999999',
    alignSelf: 'center'
  },
  txtQuantity:{
    color: '#000000',
    fontSize:22,
    fontWeight: 'bold',
    alignSelf: 'center'
  },
  viewQuantityNum:{
    backgroundColor: 'white',
    width:36,
    height:30,
    justifyContent:'center',
    borderLeftWidth:1,
    borderRightWidth:1,
    borderColor: 'white'
  },
  orderBtn:{
    backgroundColor: '#1d1d1d',
    marginLeft: 10,
    marginRight: 10,
    height:35,
    justifyContent:'center'
  },
  closeOrderDetail:{
    position:'absolute',
    left: (width-40),
    top: 5
  },
  headerImgIcon:{
    width: 100,
    height: 100,
    borderRadius: 4,
    borderWidth: 2,
    borderColor: '#F3BB2E',
    backgroundColor:'white',
    marginLeft: 10
  },
  viewToolbarIcon:{
    height: 55,
    borderColor: '#E7EAEC',
    borderBottomWidth: 1
  },
  //--------------------------------------------------------------------------------
  viewBtnEditLayout:{
    position: 'absolute',
    top: 26,
    right: 8,
    flexDirection: 'row'
  },
  viewBtnEdit:{
    borderWidth: 1,
    borderColor: '#577FF5',
    padding: 5,
    paddingTop: 2,
    paddingBottom: 2,
    borderRadius: 3
  },
  txtBtnEdit:{
    color:'#577FF5',
    fontSize: 16,
    // fontWeight: 'bold'
  },
  viewShare: {
    position:'absolute',
    right: 10,
    marginTop:-40
  }
});
module.exports = localStyles;
