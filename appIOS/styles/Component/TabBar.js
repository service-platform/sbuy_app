var React = require('react-native');
var {
  StyleSheet,
  Dimensions
} = React;
let { width, height } = Dimensions.get('window');

var styles = StyleSheet.create({
  tab: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 5,
    flexDirection: 'row',
  },
  tabs: {
    height: 45,
    flexDirection: 'row',
    paddingTop: 0,
    borderWidth: 1,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderBottomColor: 'rgba(0,0,0,0.05)',
  },
  icon: {
    width: 30,
    height: 30,
    top:5,
    left: 4,
    alignSelf:'center'
  },
  title:{
    top:0,
    alignSelf:'center'
  },
  viewBadge:{
    backgroundColor:'#EC6161',
    opacity: 0.9,
    position: 'absolute',
    top: 0,right: 4,
    borderRadius: 12,
    padding:0,
    paddingLeft: 5,
    paddingRight: 5
  },
  txtBadge:{
    color:'white',
    fontSize: 12,
    fontFamily: 'Arial'
  }
});
module.exports = styles;
