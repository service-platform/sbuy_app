var React = require('react-native');
var {
  StyleSheet,
  Dimensions
} = React;
let { width, height } = Dimensions.get('window');

var localStyles = StyleSheet.create({
  container: {
      height: (height - 98),
      width: width,
      borderTopWidth: 1,
      borderColor:'#E7EAEC'
  },
  title:{
      backgroundColor: 'rgb(250,250,250)',
      height:25,
      width: width,
      borderBottomWidth:1,
      borderColor:'rgb(236,236,236)',
      paddingLeft:10
  },
  listView:{
      width: width,
      marginBottom: 10,
      marginTop: 5,
  },
  title2:{
      backgroundColor: 'rgb(250,250,250)',
      height:25,
      width: width,
      borderBottomWidth:1,
      borderTopWidth:1,
      borderColor:'rgb(236,236,236)',
      paddingLeft:10,
      paddingTop:1,
  },
  otherText:{
      flexDirection:'row',
      marginLeft: 10,
      marginRight: 10,
      width: width,
      borderBottomWidth: 1,
      borderColor: 'rgb(236,236,236)',
      backgroundColor: 'transparent',
      height: 30,
      paddingTop:3,
  },
  txtName:{
      flex:3,
      fontSize: 16,
      color: 'rgb(123,123,123)'
  },
  txtValue:{
      flex:1,
      fontFamily: 'Arial',
      fontWeight: 'bold',
      fontSize: 14,
      color: '#555555',
  },
  txtTitle:{
      color: 'rgb(183,183,183)'
  },
  txtValue1:{
    flex:1,
    fontFamily: 'Arial',
    fontWeight: 'bold',
    fontSize: 14,
    color: '#555555',
  },
  itemCenter:{
    width:80,
    height:20,
    marginRight:10,
    alignItems:'flex-end',
    paddingRight:18
  },
  itemBlueCenter:{
    borderWidth:1,
    borderColor:'rgb(9,123,252)',
    backgroundColor: 'rgb(9,123,252)',
    borderRadius: 10,
    width:80,
    height:20,
    marginRight:10,
    alignItems:'flex-end',
    paddingRight:18
  },
  txtRowTitle: {
      marginLeft: 5
  },
  viewCatalog:{
    flexDirection:'row',
    marginLeft: 10,
    alignItems: 'center',
    marginBottom: 5
  },
  scrollView:{
    width: width,
    height: (height - 100)
  },
  txtCheckboxAll:{
    marginLeft: 5
  },
  viewBottom:{
    alignSelf:'center',
    flexDirection: 'row',
    alignItems:'center',
    borderColor:'#CCCCCC',
    borderTopWidth:1,
    width: width,
    paddingTop: 5,
    paddingLeft: 5,
    paddingRight: 5
  },
  viewCheckedAll:{
    flexDirection: 'row',
    marginTop: -10,
    flex: 1,
    alignItems: 'center'
  },
  viewSettleAccounts:{
      flexDirection: 'column',
      justifyContent: 'flex-end',
      marginTop: -10,
      marginRight: 10
  },
  viewEmptyData:{
    borderColor: '#CCCCCC',
    borderWidth: 1,
    backgroundColor: '#EEEEEE',
    alignItems: 'center',
    height: 30,
    margin: 10,
  },
  txtEmptyData:{
    alignSelf:'center',
    marginTop: 6
  },
  row: {
    alignItems: 'center',
    backgroundColor: 'white',
    flexDirection: 'row',
    paddingLeft: 10,
    paddingRight: 10,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 8,
    borderRadius: 0,

    shadowColor: '#666666',
    shadowOffset: {
        width: 0,
        height: 0
    },
    shadowOpacity: 0.5,
    shadowRadius: 4,
    elevation: 8
  },
  cellImage: {
      backgroundColor: 'white',
      height: 80,
      marginRight: 7,
      marginLeft: 3,
      marginBottom: 0,
      width: 55
  },
  movieTitle: {
      fontSize: 18,
      fontWeight: 'bold',
      marginBottom: 6,
      color: '#696969',
      width: (width - 140)
  },
  movieRating: {
      marginTop: 5,
      color: '#4EA5F1',
      fontSize: 14
  },
  button: {
    height: 36,
    flexDirection: 'row',
    backgroundColor: '#baa071',
    borderWidth: 0,
    borderRadius: 0,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center',
    paddingLeft: 8,
    paddingRight: 8,
  },
  buttonText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'white',
    alignSelf: 'center'
  },
  viewToDetail:{
    position: 'absolute',
    top: 0,
    right:0,
    flexDirection: 'column',
    borderLeftWidth: 1,
    borderColor: '#D4D4D4',

    shadowColor: '#BFBFBF',
    shadowOffset: {
        width: -1,
        height: 0
    },
    shadowOpacity: 0.7,
    shadowRadius: 4,
    elevation: 8,
  },
  viewNum:{
    height: 28,
    paddingTop: 3
  },
  textNum:{
    fontFamily: 'Arial',
    fontWeight: 'bold',
    fontSize: 16,
    color: '#000000',
    marginLeft:12,
    marginRight:13
  },
  iconRightArrow:{
    marginTop:-4,
    marginLeft:8,
    color: 'rgb(184,184,183)',
  },
  greenButton: {
    height: 26,
    width: 35,
    flex: 1,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
  greenButtonText: {
    color: '#9a9a9a',
    alignSelf: 'center'
  },
  viewBottomInfo:{
    flexDirection: 'row',
    alignItems:'center'
  },
  deleteButton: {
    height: 36,
    width: 120,
    flexDirection: 'row',
    backgroundColor: '#F1B138',
    borderWidth: 0,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center',
    borderRadius: 0,
  }
});
module.exports = localStyles;
