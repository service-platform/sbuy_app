var React = require('react-native');
var {
  StyleSheet,
  Dimensions
} = React;
let { width, height } = Dimensions.get('window');

var localStyles = StyleSheet.create({
  container: {
    paddingLeft: 10,
    paddingRight: 10,
    height: height,
    width: width,
    alignItems: 'center',
    backgroundColor: 'rgb(240,239,245)',
    marginTop:10,
  },
  facebookButton:{
    height: 36,
    width: (width - 40),
    flex: 1,
    marginTop:6,
    flexDirection: 'row',
    backgroundColor: 'rgb(78,217,100)',
    borderColor: 'rgb(78,217,100)',
    borderWidth: 1,
    borderRadius: 2,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
  geographicalButton:{
    height: 36,
    width: (width - 40),
    flex: 1,
    marginTop:6,
    flexDirection: 'row',
    backgroundColor: 'rgb(0,122,255)',
    borderColor: 'rgb(0,122,255)',
    borderWidth: 1,
    borderRadius: 2,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
  facebookView:{
    flexDirection:'row'
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
  greenButton: {
    height: 27,
    width: 50,
    flex: 1,
    marginTop:6,
    backgroundColor: '#1AB394',
    borderColor: '#1AB394',
    borderWidth: 1,
    borderRadius: 4,
    marginBottom: 10,
    marginLeft: 7,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
  greenButtonText: {
    color: 'white',
    alignSelf: 'center'
  },
});
module.exports = localStyles;
