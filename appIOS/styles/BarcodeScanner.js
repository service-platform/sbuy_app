var React = require('react-native');
var {
  StyleSheet,
  Dimensions
} = React;
let { width, height } = Dimensions.get('window');

var localStyles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imgMaskLayer: {
    width: width,
    height: height,
    position: 'absolute',
    top: 0,left: 0
  },
  btnBack: {
    height: 30,
    width: 30,
    flex: 1,
    top:20,
    backgroundColor: 'transparent',
    borderRadius: 4,
    justifyContent: 'center',
  },
  viewIcon: {
    color: '#BAA071',
    alignSelf: 'center'
  },
  imgScanningStrip: {
    width: (width - 20),
    height: 13,
    position: 'absolute',
    top: 200,
    left: 10,
  },
  viewToolbar:{
    backgroundColor: '#241f19',
    opacity: 0.9,
    position:'absolute',
    top: 0,
    left:0,
    width: width,
    height: 52,
    flexDirection: 'row',
    paddingRight: 10,
    paddingLeft: 10,
  },
  txtToolbar:{
    color: '#BAA071',
    flex: 8,
    textAlign: 'center',
    fontSize: 16,
    paddingTop: 25,
  },
  btnFromPhoto:{
    height: 30,
    width: 30,
    flex: 1,
    top:20,
    backgroundColor: 'transparent',
    borderRadius: 4,
    justifyContent: 'center',
  },
});
module.exports = localStyles;
