var React = require('react-native');
var {
  StyleSheet,
  Dimensions
} = React;
let { width, height } = Dimensions.get('window');

var localStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(39,44,48)',
  },
  rowSplitLine: {
      height: 1,
      backgroundColor: 'gray',
      marginLeft: 20,
      marginRight: 20,
  },
  viewHeader:{
    height: 30,
    justifyContent: 'center',
    marginTop: 20
  },
  viewHeaderLayout:{
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  imgHeader:{
    width: 20,
    height: 20,
    marginLeft: 20
  },
  txtHeader:{
    borderWidth: 0,
    width:240,
    color:'rgb(211,211,212)',
    backgroundColor: 'transparent',
    fontSize:14
  },
  viewHeaderIcon:{
    height: 110,
    justifyContent: 'center'
  },
  viewHeaderIconLayout:{
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center'
  },
  txtHeaderName:{
    fontSize: 16,
    color: 'rgb(211,211,212)',
    marginLeft: 10
  },
  imgHeaderIcon:{
    width: 80,
    height: 80,
    borderRadius:40,
    borderWidth:2,
    borderColor:'#D2D2D2'
  },
  btnHeaderImg:{
    borderRadius: 40,
    marginLeft:10
  },
  viewMenuItem:{
    height: 55,
    justifyContent: 'center'
  },
  menuIcon:{
    width: 20,
    height: 20
  },
  viewMenuLayout:{
    flexDirection:'row'
  },
  viewMenuLayout1:{
    flexDirection:'row',
    alignItems: 'center'
  },
  txtMenuItem:{
    fontSize: 12,
    color: 'rgb(211,211,212)',
    marginLeft: 10
  },
  btnMenuItem:{
    marginLeft:20,
    height:55,
    flex: 1,
    marginRight: 20,
    justifyContent: 'center'
  },
});
module.exports = localStyles;
