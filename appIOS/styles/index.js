var React = require('react-native');
var {
  StyleSheet,
  Dimensions
} = React;
let { width, height } = Dimensions.get('window');

var localStyles = StyleSheet.create({
  container: {
    paddingLeft: 10,
    paddingRight: 10,
    height: height,
    width: width,
    alignItems: 'center',
    backgroundColor: 'rgb(255,255,255)',
  },
  bgImage:{
    height: height,
    width: width,
  },
  subContainer:{
    flexDirection:'column',
    position: 'absolute',
    width: width,
    bottom:60
  },
  button: {
     height: 36,
     width: 260,
     flex: 1,
     marginTop:10,
     flexDirection: 'row',
     backgroundColor: 'rgba(0,0,0,0)',
     borderColor: '#f3f3f3',
     borderWidth: 1,
     borderRadius: 20,
     marginBottom: 5,
     alignSelf: 'center',
     justifyContent: 'center'
  },
  signinButton: {
    height: 36,
    width: 260,
    flex: 1,
    marginTop:5,
    flexDirection: 'row',
    backgroundColor: '#f3f3f3',
    borderColor: '#f3f3f3',
    borderWidth: 1,
    borderRadius: 20,
    marginBottom: 10,
    alignSelf: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
  buttonLogin:{
    fontSize: 18,
    color: '#1a5088',
    alignSelf: 'center'
  },
  logoImage:{
    width: (width - 50),
    height: 150,

    position: 'absolute',
    marginTop: 150,
    marginLeft: 17
  }
});
module.exports = localStyles;
