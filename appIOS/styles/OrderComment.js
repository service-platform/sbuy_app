var React = require('react-native');
var {
  StyleSheet,
  Dimensions
} = React;
let { width, height } = Dimensions.get('window');

var localStyles = StyleSheet.create({
  container: {
    flex: 1,
    height: height,
    width: width,
    backgroundColor: 'rgb(240,239,245)',
  },
  toolbar: {
    backgroundColor: '#F3F3F4',
    height: 55,
  },
  viewDescription:{
    flexDirection:'row',
    marginTop: 10,
    marginLeft: 10
  },
  viewDescriptionBtn:{
    marginTop: 10,
    flexDirection: 'row',
    justifyContent:'center',
    marginLeft: 0,
  },
  imgProduct:{
    width:35,
    height:50,
    borderWidth: 2,
    borderColor: '#e9e9e9'
  },
  subContainer:{
    flex: 1,
    alignItems: 'center',
    marginLeft: 20,
    marginTop: 15,
    flexDirection: 'row',
  },
  viewStarRating: {
    width: 100
  },
  txtInfo: {
    marginRight:5,
    marginTop:-2
  },
  viewText: {
     margin: 10,
     borderColor: '#DADADA',
     borderWidth: 1,
     borderRadius: 4
  },
  txtInput: {
    backgroundColor: 'transparent',
    textAlignVertical: 'top',
    height: 120,
    paddingLeft: 5
  },
  imgPicture:{
    width: 65,
    height: 85,
    marginRight: 5,
    marginBottom: 5,
    borderWidth: 2,
    borderColor: 'rgb(224,224,224)'
  },
  viewAdd: {
    borderColor: 'rgb(224,224,224)',
    borderWidth: 1,
    width: 65,
    height: 85,
    overflow: 'hidden',
    backgroundColor: 'rgb(247,247,247)'
  },
  iconAdd:{
    marginTop: -12,
    marginLeft: 4,
    color: 'rgb(194,194,194)'
  },
  button: {
    height: 36,
    width: (width - 20),
    marginTop: 0,
    flexDirection: 'row',
    backgroundColor: '#baa071',
    borderColor: '#baa071',
    borderWidth: 1,
    borderRadius: 2,
    justifyContent: 'center',
    marginBottom: 10
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
  greenButton: {
    height: 27,
    width: 50,
    flex: 1,
    marginTop:6,
    borderRadius: 4,
    marginBottom: 10,
    marginLeft: 7,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
  greenButtonText: {
    color: 'white',
    alignSelf: 'center'
  },
  viewImageList:{
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: (width - 10),
    paddingRight: 0
  }
});
module.exports = localStyles;
