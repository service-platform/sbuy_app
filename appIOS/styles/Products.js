var React = require('react-native');
var {
  StyleSheet,
  Dimensions
} = React;
let { width, height } = Dimensions.get('window');

var localStyles = StyleSheet.create({
  container: {
    paddingLeft: 10,
    paddingRight: 10,
    height: (height - 128),
    width: width,
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    borderTopWidth: 1,
    borderColor: '#E7EAEC'
  },
  //以下是 列表  样式－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－
  scrollView:{
    width: width,
    height: (height - 136),
    position:'absolute',
    top: 32,
    left: 0
  },
  separator:{
    paddingTop: 10,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  touchablelight: {
      backgroundColor: 'white',
      marginBottom: 10,
      marginLeft: parseInt((width-((parseInt(width/2)-20)*2))/3)
  },
  row: {
      alignItems: 'center',
      backgroundColor: 'white',
      height: 205,
      shadowColor: '#666666',
      shadowOffset: {
          width: 0,
          height: 0
      },
      shadowOpacity: 0.5,
      shadowRadius: 4,
      elevation: 8
  },
  cellImage: {
      backgroundColor: 'white',
      height: 160,
      width: parseInt(width/2)-20,
  },
  movieTitle: {
      fontSize: 15,
      fontWeight: '500',
      width:280,
      color: '#666'
  },
  viewToDetail:{
    position: 'absolute',
    marginTop: 20,
    marginLeft: 10,
  },
  viewNum:{
    backgroundColor: '#78767B',
    height: 20,
    borderRadius: 3,
    paddingTop: 2
  },
  textNum:{
    color: 'white',
    fontFamily: 'Arial',
    marginLeft:10,
    marginRight:7
  },
  textContainer:{
    position: 'absolute',
    backgroundColor:'transparent',
    marginTop: 0,
    paddingLeft: 7,
  },
  viewSearch: {
    width: width,
    height: 30,
    paddingRight: 6,
    paddingLeft: 8,
    flexDirection: 'row',
    marginTop: 3,
    borderBottomWidth: 2,
    borderColor: '#DEDEDE',
  },
  viewInput: {
    marginRight: 10,
    flex: 3,
    height: 30,
    paddingBottom: 5,
  },
  txtInput: {
    flex: 1,
    backgroundColor: 'transparent',
    height: 30,
    padding: 0,
    paddingLeft: 2,
    fontSize: 14,
  },
  btnSearch: {
    height: 30,
    width: 30,
    flex: 1,
    justifyContent: 'center',
  },
  txtSearch: {
    height: 18,
    width: 18,
    marginLeft:50
  },
  viewTypes: {
    backgroundColor: 'white',
    position: 'absolute',
    left: width,
    top: 32,
    paddingTop: 5,
    paddingBottom: 5,
    borderColor: '#DEDEDE',
    borderBottomWidth: 2,
    width: width
  }
});
module.exports = localStyles;
