'use strict';
require ('react');
require ('./utils/General');
require ('./utils/Constant');
var React = require('react-native');
var TimerMixin = require('react-timer-mixin');
var strings = require('./utils/LocalizedStrings');
var {
    AppRegistry,
    Platform,
    Navigator,
    Alert,
    AlertIOS
} = React;
//此处和上面用 require 一样
import store from './redux/store';
import { Provider } from 'react-redux';

import index from './pages/index';

var _navigator; var isExit = true;
var App = React.createClass({
  mixins: [TimerMixin],

  initialRoute: {
    component: index,  //component 与renderScene 的route.component 一致
    name: 'index'
  },
  //场景配置，页面怎么呈现出来
  configureScene: function() {
      return Navigator.SceneConfigs.FloatFromBottomAndroid;
  },
  renderScene: function(route, navigator) {
    _navigator = navigator;
    const Component = route.component;
    return (
      <Component {...route.params}  navigator={_navigator}/>
    );
  },

  render: function() {
    return (
      <Provider store={store} key="provider">
        <Navigator
          ref="test"
          initialRoute={this.initialRoute}  //指定打开的是哪一个页面
          configureScene={() => this.configureScene()}  // 也可以{()=>this.configureScene}
          renderScene={(route, navigator) => this.renderScene(route, navigator)}
        />
      </Provider>
    );
  }
});

AppRegistry.registerComponent('SBuy', () => App);
