//Object To String
JSON.ObjectToString = function(obj){
    var retString = ""
    for(item in obj){
        if(obj[item] instanceof Array){
          retString += item + "=[" + obj[item] + "]&"
        }else{
          retString += item + "=" + obj[item] + "&"
        }
    }
    retString = retString.substr(0,retString.length-1);
    retString = encodeURI(retString);
    return retString;
}

//author: Jun Cocoa
Date.prototype.Format = function (fmt) {
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "h+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
window.GLOBAL.fnNavigatorPage = function(obj,pageName,pageObj,paramsObj){
  const { navigator } = obj.props;
  if(navigator) {
     navigator.push({
         name: pageName,
         component: pageObj,
         params: paramsObj
     })
  }
};
window.GLOBAL.isEmptyObject = function(e) {
    var t;
    for (t in e)
        return !1;
    return !0
}
