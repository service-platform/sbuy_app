

#ifndef IntegratedAlipay_AlipayHeader_h
#define IntegratedAlipay_AlipayHeader_h


#import <AlipaySDK/AlipaySDK.h>
#import "AlipayRequestConfig.h"
#import "DataSigner.h"

#import <Foundation/Foundation.h>


#define kPartnerID @""


#define kSellerAccount @""


#define kNotifyURL @""


#define kAppScheme @"integratedAlipay"



#define kPrivateKey @""


#endif
