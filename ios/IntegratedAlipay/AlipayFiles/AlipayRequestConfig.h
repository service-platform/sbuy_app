

#import <Foundation/Foundation.h>
#import "AlipayHeader.h"
#import "RCTBridgeModule.h"

@interface AlipayRequestConfig : NSObject<RCTBridgeModule>



+ (void)alipayWithPartner:(NSString *)partner
                   seller:(NSString *)seller
                  tradeNO:(NSString *)tradeNO
              productName:(NSString *)productName
       productDescription:(NSString *)productDescription
                   amount:(NSString *)amount
                notifyURL:(NSString *)notifyURL
                   itBPay:(NSString *)itBPay;
@end

