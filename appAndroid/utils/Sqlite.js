'use strict';
var React = require('react-native');
var {
    AsyncStorage,
    ToastAndroid,
} = React;

//SQLite 数据库
var Sqlite = require('@remobile/react-native-sqlite');
var db = Sqlite.openDatabase({name: "my.db"}, function(db) {
  //要创建的表，初始化时候，创建
  db.transaction(function(tx) {
      // tx.executeSql('DROP TABLE IF EXISTS orderList');
      tx.executeSql('CREATE TABLE IF NOT EXISTS orderList (id text primary key,name text,address text,describe text,deliveryType integer,paymentType integer,dateTime text, price DECIMAL(10,2),status text)',[],function(tx, res){
          console.log("SQL: CREATE TABLE IF NOT EXISTS orderList (id text primary key,name text,address text,describe text,deliveryType integer,paymentType integer,dateTime text, price DECIMAL(10,2),status text)");
          // tx.executeSql('DROP TABLE IF EXISTS orderDetailList');
          tx.executeSql('CREATE TABLE IF NOT EXISTS orderDetailList (id integer primary key autoincrement ,orderId text, productId text,imageUrl text,name text,desc text,realPrice DECIMAL(10,2),totalNum integer)',[],function(tx, res){
            console.log("SQL: CREATE TABLE IF NOT EXISTS orderDetailList (id integer primary key autoincrement,orderId text, productId text,imageUrl text,name text,desc text,realPrice DECIMAL(10,2),totalNum integer)" );

            // tx.executeSql('DROP TABLE IF EXISTS orderCommentList');
            tx.executeSql('CREATE TABLE IF NOT EXISTS orderCommentList (id integer primary key autoincrement ,orderId text, descTally DECIMAL(2,1),desc text,sellerService DECIMAL(2,1),logisticsService DECIMAL(2,1),isAnonymous BOOL)',[],function(tx, res){
              console.log("SQL: CREATE TABLE IF NOT EXISTS orderCommentList (id integer primary key autoincrement ,orderId text, descTally DECIMAL(2,1),desc text,sellerService DECIMAL(2,1),logisticsService DECIMAL(2,1),isAnonymous BOOL)" );
            }, function(e) {
               console.log("ERROR: " + e.message);
            });
          }, function(e) {
             console.log("ERROR: " + e.message);
          });
      }, function(e) {
          console.log("ERROR: " + e.message);
      });
  });
});

export function addOrderInfo(data){
  db.transaction(function(tx) {
      //添加订单详情表
      for (var i = 0; i < data.list.length; i++) {
        var obj = data.list[i];

        tx.executeSql("INSERT INTO orderDetailList (orderId,productId,imageUrl,name,desc,realPrice,totalNum) VALUES ('" + data.Id + "','" + obj.id + "','" + obj.imageUrl + "','" + obj.name.replace(/'/g, "''").replace(/\//g, "//") + "','" + obj.desc.replace(/'/g, "''").replace(/\//g, "//") + "'," + obj.realPrice + "," + obj.totalNum + ");",[],function(tx, res){
            console.log("SQL(" + res.rowsAffected + "): INSERT INTO orderDetailList (orderId,productId,imageUrl,name,desc,realPrice,totalNum) VALUES ('" + data.Id + "','" + obj.id + "','" + obj.imageUrl + "','" + obj.name.replace(/'/g, "''").replace(/\//g, "//") + "','" + obj.desc.replace(/'/g, "''").replace(/\//g, "//") + "'," + obj.realPrice + "," + obj.totalNum + ");");
        }, function(e) {
            console.log("ERROR: " + e.message);
        });
      }
      //添加订单表
      tx.executeSql("INSERT INTO orderList (id, name, address, describe,deliveryType,paymentType,dateTime,price,status) VALUES ('" + data.Id + "','" + data.name + "','" + data.address + "','" + data.describe + "'," + data.deliveryType + "," + data.paymentType + ",'" + data.dateTime + "'," + data.price + ",'" + data.status + "')",[],function(tx, res){
          console.log("SQL(" + res.rowsAffected + "): INSERT INTO orderList (id, name, address, describe,deliveryType,paymentType,dateTime,price,status) VALUES ('" + data.Id + "','" + data.name + "','" + data.address + "','" + data.describe + "'," + data.deliveryType + "," + data.paymentType + ",'" + data.dateTime + "'," + data.price + ",'" + data.status + "')");
          ToastAndroid.show("订单：添加成功。", ToastAndroid.SHORT);
          AsyncStorage.removeItem("Order");
      }, function(e) {
          console.log("ERROR: " + e.message);
      });
  });
}

export function selectALLOrder(refreshData){
  db.transaction(function(tx) {
    tx.executeSql("select * from orderList", [], function(tx, res) {
        if (res.rows.length == 0){
          refreshData([]);
        }else{
          var arrays = [];
          for (var i = 0; i < res.rows.length; i++) {
            var obj = {
              id: res.rows.item(i).id,
              name: res.rows.item(i).name,
              address: res.rows.item(i).address,
              describe: res.rows.item(i).describe,
              deliveryType: res.rows.item(i).deliveryType,
              paymentType: res.rows.item(i).paymentType,
              dateTime: res.rows.item(i).dateTime,
              price: res.rows.item(i).price,
              status: res.rows.item(i).status
            }
            arrays.push(obj);
          }
          refreshData(arrays);
        }
    });
  });
}

export function selectALLOrderDetail(orderId,refreshData){
  db.transaction(function(tx) {
    tx.executeSql("select * from orderList where id='" + orderId + "'", [], function(tx, res) {
        console.log("SQL(" + res.rows.length + "): select * from orderList where id='" + orderId + "'");
        if (res.rows.length <= 0){
          refreshData(null);
        }else{
          var obj = {
            id: res.rows.item(0).id,
            total: res.rows.item(0).price,
            name: res.rows.item(0).name,
            address: res.rows.item(0).address,
            paymentType: res.rows.item(0).paymentType,
            deliveryType: res.rows.item(0).deliveryType,
          }

          tx.executeSql("select * from orderDetailList where orderId='" + orderId + "'", [], function(tx, res) {
              console.log("SQL(" + res.rows.length + "): select * from orderDetailList where orderId='" + orderId + "'");
              if (res.rows.length <= 0){
                obj.list = [];
                refreshData(obj);
              }else{
                obj.list = [];
                for (var i = 0; i < res.rows.length; i++) {
                  var orderDetailList = {
                    id: res.rows.item(i).id,
                    orderId: res.rows.item(i).orderId,
                    productId: res.rows.item(i).productId,
                    imageUrl: res.rows.item(i).imageUrl,
                    name: res.rows.item(i).name,
                    desc: res.rows.item(i).desc,
                    realPrice: res.rows.item(i).realPrice,
                    totalNum: res.rows.item(i).totalNum
                  }
                  obj.list.push(orderDetailList);
                }

                refreshData(obj);
              }
          });
        }
    });
  });
}
export function deleteALLOrder(){
  db.transaction(function(tx) {
      tx.executeSql('delete from orderList',[],function(tx, res){
          console.log("SQL(" + res.rowsAffected + "): delete from orderList");

          tx.executeSql('delete from orderDetailList',[],function(tx, res){
            console.log("SQL(" + res.rowsAffected + "): delete from orderDetailList");
          }, function(e) {
             console.log("ERROR: " + e.message);
          });
          ToastAndroid.show("订单：删除成功。", ToastAndroid.SHORT);
      }, function(e) {
          console.log("ERROR: " + e.message);
      });
  });
}
//添加评论信息
export function addCommentInfo(data,obj){
  db.transaction(function(tx) {
      tx.executeSql("Select * from orderCommentList where orderId='" + data.orderId + "'",[],function(tx, res){
          console.log("SQL(" + res.rowsAffected + "): Select * from orderCommentList where orderId='" + data.orderId + "'");
          debugger;
          if(res.rows.length <= 0){
              tx.executeSql("INSERT INTO orderCommentList (orderId, descTally, desc, sellerService,logisticsService,isAnonymous) VALUES ('" + data.orderId + "'," + data.descTally + ",'" + data.desc + "'," + data.sellerService + "," + data.logisticsService + "," + data.isAnonymous + ")",[],function(tx, res){
                  console.log("SQL(" + res.rowsAffected + "): INSERT INTO orderCommentList (orderId, descTally, desc, sellerService,logisticsService,isAnonymous) VALUES ('" + data.orderId + "'," + data.descTally + ",'" + data.desc + "'," + data.sellerService + "," + data.logisticsService + "," + data.isAnonymous + ")");
                  debugger;
                  ToastAndroid.show("评论已经提交：成功。", ToastAndroid.SHORT);
                  obj.backPage();
              }, function(e) {
                  console.log("ERROR: " + e.message);
              });
          }else{
            ToastAndroid.show("评论已经提交：成功。", ToastAndroid.SHORT);
            obj.backPage();
          }
      }, function(e) {
          console.log("ERROR: " + e.message);
      });
  });
}

//查询评论信息
export function selectCommentInfo(orderId,obj){
  db.transaction(function(tx) {
      tx.executeSql("Select * from orderCommentList where orderId='" + orderId + "'",[],function(tx, res){
          console.log("SQL(" + res.rowsAffected + "): Select * from orderCommentList where orderId='" + orderId + "'");
          debugger;
          if(res.rows.length >= 1){
            var data = {
              orderId: res.rows.item(0).orderId,
              descTally: res.rows.item(0).descTally,
              desc: res.rows.item(0).desc,
              sellerService: res.rows.item(0).sellerService,
              logisticsService: res.rows.item(0).logisticsService,
              isAnonymous: (res.rows.item(0).isAnonymous == 1)?true:false
            }
            obj.setCommentDate(data)
          }
      }, function(e) {
          console.log("ERROR: " + e.message);
      });

  });
}
