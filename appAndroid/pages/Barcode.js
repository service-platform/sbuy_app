var React = require('react-native');
var styles = require('./../styles/Barcode');
var BarcodeScanner = require('./BarcodeScanner');
var ScrollableTabView = require('react-native-scrollable-tab-view');
var TabBar = require('./Component/TabBar');
var Favorites = require('./Favorites');
var Cart = require('./Cart');
var OrderConfirm = require('./OrderConfirm');
var OrderDetail = require('./OrderDetail');
var HistoryList = require('./HistoryList');
var UserCenter = require('./UserCenter');
var AddressList = require('./AddressList');

var Category = require('./Category');

var Products = require('./../pages/Products');
var MarkerSelection = require('./Component/MarkerSelection');
var ProductsDetail = require('./../pages/ProductsDetail');

import store from './../redux/store';
import { postsGoodsUrl, postsCartInfo, postsFavorites, getPaymentInfo, postPlaymentResult,postsDeleteCartInfo } from './../redux/actions/productsAction';

var {
    View,
    Text,
    Image,
    Alert,
    Animated,
    Easing,
    TextInput,
    Dimensions,
    ScrollView,
    AsyncStorage,
    ToolbarAndroid,
    ToastAndroid,
    TouchableHighlight,
    TouchableOpacity,
    NativeModules,
    Platform,
    NetInfo,
    BackAndroid,
    DeviceEventEmitter,
    WebView,
    RefreshControl,
} = React;
let { width, height } = Dimensions.get('window');
var Icon = require('react-native-vector-icons/Ionicons');
var strings = require('./../utils/LocalizedStrings');

var Barcode = React.createClass({
    getInitialState: function () {
        return {
            imageUrl: require('image!splash'),
            toolbarTitle: '精选',
            name: '',
            desc: "",
            htmlDesc: "",
            realPrice: 0,
            totalNum: 1,
            cartTotalNum: 0,
            fadeAnim: new Animated.Value(width),
            fadeWebViewAnim: new Animated.Value(width),

            ServerData: {},
            favoritesIcon: require('image!collect'),
            automaticFolding: true,
            txtBorderColor: '#E5E6E7',
            isScanBar: false,
            txtEdit: strings.btnEdit
        };
    },
    componentDidMount: function() {
      JSON.backIndex = 3;
    },
    componentWillMount:function() {
      var me = this;
      BackAndroid.addEventListener('hardwareBackPress', ()=>{
          if(JSON.backIndex >= 3){
              me.refs.scrollableTabView.goToPage(0);
          }
      });
    },
    componentWillUnmount:function() {
        BackAndroid.removeEventListener('hardwareBackPress', ()=>{});
    },
    backLogin: function(){
      const { navigator } = this.props;
      if(navigator) {
         navigator.pop();
      }
    },
    openDetailByFavorites: function(id, automaticFolding, totalNum){
        this.setState({
          automaticFolding: automaticFolding,
          totalNum: totalNum
        });
        var me = this;
        store.dispatch(postsGoodsUrl(ServerURL + "/api/" + id + "/" + window.GLOBAL.user_id + "/")).then((json) =>{
            var productData = store.getState().productsReducer.ServerData;
            var reg = /<[^<>]+>/g;
            if(productData.length == 0){
                ToastAndroid.show(strings.alertNoProd, ToastAndroid.SHORT);
            }else {
                me.setState({
                    imageUrl: {uri: ServerURL.replace("/en", "") + productData.image},
                    name: productData.name,
                    desc: productData.description.replace(reg,''),
                    htmlDesc: productData.description,
                    realPrice: productData.price,
                    ServerData: productData,
                    favoritesIcon: productData.wish_status ? require('image!collect_chenck') : require('image!collect'),
                    isScanBar: false
                });
                Animated.timing(
                    me.state.fadeAnim,
                    {toValue: 0, duration: 500, easing: Easing.linear},
                ).start();
                me.refs.productsDetail.setTotalNum(totalNum);
            }
        }).catch((error)=>{
          ToastAndroid.show("对不起，数据请求失败：" + error.message, ToastAndroid.SHORT);
        }).done();
    },
    openProductsDetail: function(dataURL){
        var me = this;
        switch (strings.getLanguage()) {
            case "en":
                if(dataURL.includes("/en/") == false){
                    dataURL = dataURL.replace(":8000/",":8000/en/");
                }
                break;
            case "zh":
                if(dataURL.includes("/en/") == true){
                    dataURL = dataURL.replace(":8000/en/",":8000/");
                }
                break;
            default:
        }
        store.dispatch(postsGoodsUrl(dataURL)).then((json) => {
            var productData = store.getState().productsReducer.ServerData;
            var reg = /<[^<>]+>/g;
            if (productData.length == 0) {
                ToastAndroid.show(strings.alertNoProd, ToastAndroid.SHORT);
            } else {
                me.setState({
                    imageUrl: {uri: ServerURL.replace("/en", "") + productData.image},
                    name: productData.name,
                    desc: productData.description.replace(reg,''),
                    htmlDesc: productData.description,
                    realPrice: productData.price,
                    ServerData: productData,
                    favoritesIcon: productData.wish_status ? require('image!collect_chenck') : require('image!collect'),
                    isScanBar: true,
                });
                Animated.timing(
                    me.state.fadeAnim,
                    {toValue: 0, duration: 500, easing: Easing.linear},
                ).start();
            }
        }).catch((error)=>{
          ToastAndroid.show("对不起，数据请求失败：" + error.message, ToastAndroid.SHORT);
        }).done();
    },
    setTabViewPage: function(){
        this.refs.scrollableTabView.goToPage(0);
    },
    changeTab: function (index) {
        window.GLOBAL.tabIndex = index.i;
        switch (index.i) {
          case 0:
            this.setState({toolbarTitle: strings.lblTitleTab1,isShowEdit: false});
            this.refs.products.btnReloadData();
            Animated.timing(
               this.state.fadeAnim,
               {toValue: width,duration: 500},
            ).start();
            Animated.timing(
               this.state.fadeWebViewAnim,
               {toValue: width,duration: 500},
            ).start();
            break;
          case 1:
            this.setState({toolbarTitle: strings.lblTitleTab2,isShowEdit: false});

            if(window.GLOBAL.isOpenCameraView == false){
                window.GLOBAL.isOpenCameraView = true;
                const { navigator } = this.props;
                if(navigator) {
                   navigator.push({
                       name: 'BarcodeScanner',
                       component: BarcodeScanner,
                       params: {
                         openProductsDetail: this.openProductsDetail,
                         setTabViewPage: this.setTabViewPage
                       }
                   })
                }
            }
            Animated.timing(
               this.state.fadeAnim,
               {toValue: width,duration: 500},
            ).start();
            Animated.timing(
               this.state.fadeWebViewAnim,
               {toValue: width,duration: 500},
            ).start();
            break;
          case 2:
            this.setState({toolbarTitle: strings.lblTitleTab3,isShowEdit: false});
            this.refs.favorites.refreshData();
            Animated.timing(
               this.state.fadeAnim,
               {toValue: width,duration: 500},
            ).start();
            Animated.timing(
               this.state.fadeWebViewAnim,
               {toValue: width,duration: 500},
            ).start();
            break;
          case 3:
            this.setState({toolbarTitle: strings.lblTitleTab4,isShowEdit: true});
            this.refs.cart.refreshData(false);
            Animated.timing(
               this.state.fadeAnim,
               {toValue: width,duration: 500},
            ).start();
            Animated.timing(
               this.state.fadeWebViewAnim,
               {toValue: width,duration: 500},
            ).start();
            break;
          case 4:
            this.setState({toolbarTitle: strings.lblTitleTab5,isShowEdit: false});
            Animated.timing(
               this.state.fadeAnim,
               {toValue: width,duration: 500},
            ).start();
            Animated.timing(
               this.state.fadeWebViewAnim,
               {toValue: width,duration: 500},
            ).start();
            break;
          default:
        }
    },
    orderBuyBtn:function(){
      var datas = this.refs.productsDetail.getTypes();
      var me = this;
      if(datas.isType == true){
          var objData = {};
          if(datas.items.length <= 0){
              objData = {
                user: window.GLOBAL.user_id,
                product: this.state.ServerData.id,
                quantity: this.refs.productsDetail.getTotalNum()
              };
          }else{
              objData = {
                user: window.GLOBAL.user_id,
                product: this.state.ServerData.id,
                quantity: this.refs.productsDetail.getTotalNum(),
                attribute: datas.items
              };
          }
          store.dispatch(postsCartInfo(objData)).then((json) =>{
              if(json.ServerData.add_cart_item == true){
                  ToastAndroid.show(strings.toastAddedToCart, ToastAndroid.SHORT);
                  me.refs.cart.refreshData(false);
                  // me.refs.scrollableTabView.goToPage(3);

                  if(this.state.automaticFolding == true){
                    Animated.timing(
                       this.state.fadeAnim,
                       {toValue: width,duration: 500},
                    ).start();
                  }
              }else{
                  ToastAndroid.show(strings.toastCartFailed, ToastAndroid.SHORT);
              }
          }).catch((error)=>{
            ToastAndroid.show("对不起，数据请求失败：" + error.message, ToastAndroid.SHORT);
          }).done();
      }else{
          ToastAndroid.show(strings.toastRelevantType, ToastAndroid.SHORT);
      }
    },
    btnBuyNow: function(){
      var realPrice = (this.state.realPrice * this.refs.productsDetail.getTotalNum()).toFixed(2);
      var obj = {
          product_image: this.refs.productsDetail.getProductImage(),
          name: this.refs.productsDetail.getName(),
          price: this.state.realPrice,
          quantity: this.refs.productsDetail.getTotalNum()
      };
      this.commodityClearing(realPrice,this.refs.productsDetail.getTotalNum().toString(),obj,this,true)
    },
    backBarcode: function(){
      Animated.timing(
         this.state.fadeAnim,
         {toValue: width,duration: 500},
      ).start();
      if(this.state.isScanBar == true){
         this.setTabViewPage();
      }
    },
    backProductDesc: function(){
      Animated.timing(
         this.state.fadeWebViewAnim,
         {toValue: width,duration: 500},
      ).start();
    },
    favoritesInfo:function(index){
      var me = this;
      if(index == 0){
        var obj = {
          user_id: window.GLOBAL.user_id,
          product_id: this.state.ServerData.id,
          follow: !this.state.ServerData.wish_status
        }
        this.state.ServerData.wish_status = !this.state.ServerData.wish_status;

        store.dispatch(postsFavorites(obj)).then((json) =>{
            if(json.ServerData.add_status == "successful"){
                this.setState({
                  favoritesIcon: require('image!collect_chenck')
                });
                me.refs.favorites.refreshData();
                ToastAndroid.show(strings.toastFavoriteSuccess, ToastAndroid.SHORT);
            }else if(json.ServerData.add_status == "delete"){
                this.setState({
                  favoritesIcon: require('image!collect')
                });
                me.refs.favorites.refreshData();
                ToastAndroid.show(strings.toastCancelFavorite, ToastAndroid.SHORT);
            }
        }).catch((error)=>{
            ToastAndroid.show("对不起，数据请求失败：" + error.message, ToastAndroid.SHORT);
        }).done();
      }
    },
    commodityClearing: function(grandTotal,totalNum,pIds,obj,isBuyNow){
        var paramsObj = {
            totalNum: totalNum,
            grandTotal: grandTotal,
            obj: obj,
            isBuyNow: isBuyNow,
            pIds: pIds
        };
        fnNavigatorPage(this,'OrderConfirm',OrderConfirm,paramsObj);
    },
    orderDetail: function(orderObj){
        var paramsObj = {
            orderObj: orderObj
        };
        fnNavigatorPage(this,'OrderDetail',OrderDetail,paramsObj);
    },
    openCategoryDetail: function(id){
      this.openDetailByFavorites(id,true,1);
    },
    wechartPaymentTest: function(){
      //注册 APP
      // NativeModules.WeChatAndroid.registerApp("sdf",(err,registerOK) => {
      //   debugger;
      // });
      //打开微信
      // NativeModules.WeChatAndroid.openWXApp((err,res) => {
      //   debugger;
      // });
      //是否安装微信
      // NativeModules.WeChatAndroid.isWXAppInstalled((err,isInstalled) => {
      //   debugger;
      // });
      //是否支持微信 API
      // NativeModules.WeChatAndroid.isWXAppSupportAPI((err,isSupport) => {
      //   debugger;
      // });
      //获取支持的 API
      // NativeModules.WeChatAndroid.getWXAppSupportAPI((err,supportAPI) => {
      //   debugger;
      // });
      //认证登录，返回
      // NativeModules.WeChatAndroid.sendAuthReq(null,null,(err,authReqOK) => {
      //   debugger;
      //   // 处理登录回调结果
      //   DeviceEventEmitter.addListener('finishedAuth',function(event){
      //       debugger;
      //       var success = event.success;
      //       if(success){
      //        ToastAndroid.show(
      //         ' code = ' + JSON.stringify(event.code) +
      //         ' state = ' + JSON.stringify(event.state),
      //         ToastAndroid.LONG
      //        );
      //       }else{
      //        ToastAndroid.show('授权失败',ToastAndroid.SHORT);
      //       }
      //   });
      // });

      // var payOptions = {
      //   appId: 'wxd930ea5d5a258f4f',
      //   nonceStr: '5K8264ILTKCH16CQ2502SI8ZNMTM67VS',
      //   packageValue: 'Sign=WXPay',
      //   partnerId: '1900000109',
      //   prepayId: 'WX1217752501201407033233368018',
      //   timeStamp: '1412000000',
      //   sign: 'C380BEC2BFD727A4B6845133519F3AD6',
      // };
      // NativeModules.WeChatAndroid.registerApp("wxd930ea5d5a258f4f",(err,registerOK) => {
      //     NativeModules.WeChatAndroid.weChatPay(payOptions,(err,sendReqOK) => {
      //       debugger;
      //       DeviceEventEmitter.emit('finishedPay',sendReqOK);
      //     });
      // });


      //  处理支付回调结果
      // DeviceEventEmitter.addListener('finishedPay',function(event){
      //    var success = event.success;
      //    if(success){
      //     // 在此发起网络请求由服务器验证是否真正支付成功，然后做出相应的处理
      //    }else{
      //     ToastAndroid.show('支付失败',ToastAndroid.SHORT);
      //    }
      // });
    },
    setLanguage: function(index){
      if(index == 0){
        strings.setLanguage('en');

        if(this.state.txtEdit == '编 辑'){
            this.setState({
              txtEdit: strings.btnEdit
            },...this.state);
        }else{
            this.setState({
              txtEdit: strings.btnFinish
            },...this.state);
        }
        ServerURL = i18nIndex[1];
      }else{
        strings.setLanguage('zh');
        if(this.state.txtEdit == 'Edit'){
            this.setState({
              txtEdit: strings.btnEdit
            },...this.state);
        }else{
            this.setState({
              txtEdit: strings.btnFinish
            },...this.state);
        }
        ServerURL = i18nIndex[0];
      }
      if(window.GLOBAL.tabIndex == undefined){window.GLOBAL.tabIndex = 0};
      switch (window.GLOBAL.tabIndex) {
        case 0:
          this.setState({toolbarTitle: strings.lblTitleTab1});
          break;
        case 1:
          this.setState({toolbarTitle: strings.lblTitleTab2});
          break;
        case 2:
          this.setState({toolbarTitle: strings.lblTitleTab3});
          this.refs.favorites.refreshData();
          break;
        case 3:
          this.setState({toolbarTitle: strings.lblTitleTab4});
          this.refs.cart.refreshData(false);
          break;
        case 4:
          this.setState({toolbarTitle: strings.lblTitleTab5});
          break;
        default:
      }
    },
    showWebViewDesc:function(){
      Animated.timing(
         this.state.fadeWebViewAnim,
         {toValue: 0,duration: 500,easing: Easing.linear},
      ).start();
    },
    setCartTotalNum: function(totalNum){
      this.setState({
        cartTotalNum: totalNum
      })
    },
    btnEditClick: function(){
        if(this.state.txtEdit == strings.btnEdit){
            this.setState({txtEdit: strings.btnFinish});
        }else{
            this.setState({txtEdit: strings.btnEdit});
        }
    },
    deleteCartItems: function(totalNum,pIds){
      if(totalNum <= 0){ ToastAndroid.show(strings.toastCartEmpty, ToastAndroid.SHORT); return;}

      Alert.alert(
          strings.alertPrompt,
          strings.formatString(strings.alertDeleteCartItem,totalNum),
          [
            {text: strings.alertCancel},
            {text: strings.alertOk, onPress: () => {
              var objs = {pIds: pIds};

              store.dispatch(postsDeleteCartInfo(objs)).then((json) =>{
                  if(json.ServerData.cart_item_delete == true){
                      ToastAndroid.show(strings.toastDeletedSuccess, ToastAndroid.SHORT);
                      this.refs.cart.refreshData(true);
                  }else{
                      ToastAndroid.show(strings.toastDeletedFailed, ToastAndroid.SHORT);
                  }
              }).catch((error)=>{
                  ToastAndroid.show("对不起，数据请求失败：" + error.message, ToastAndroid.SHORT);
              }).done();
            }},
          ]
      );
    },
    onAddressManager: function(){
        const { navigator } = this.props;
        if(navigator) {
           navigator.push({
               name: 'AddressList',
               component: AddressList,
           });
        }
    },
    onShowHistoryList: function(){
        const { navigator } = this.props;
        if(navigator) {
           navigator.push({
               name: 'HistoryList',
               component: HistoryList,
           });
        }
    },
    openScanCode: function(){
        this.refs.scrollableTabView.goToPage(1);
    },
    render: function () {
        var btnEdit = null;
        if(this.state.isShowEdit == true){
            btnEdit =
              <View style={styles.viewBtnEditLayout}>
                <TouchableOpacity onPress={this.btnEditClick}>
                    <View style={styles.viewBtnEdit}>
                        <Text style={styles.txtBtnEdit}>{this.state.txtEdit}</Text>
                    </View>
                </TouchableOpacity>
              </View>
        }else{
            btnEdit = <View></View>;
        }
        return (
          <View style={{height:height-25}}>
                <ToolbarAndroid
                    actions={toolbarActions}
                    onActionSelected={this.openScanCode}
                    style={styles.toolbar}
                    titleColor="#A1A3A5"
                    title={this.state.toolbarTitle} />

                <ScrollableTabView ref="scrollableTabView" initialPage={0} style={{backgroundColor: '#2F4050'}} onChangeTab={this.changeTab} tabBarPosition="bottom" renderTabBar={() => <TabBar cartTotalNum={this.state.cartTotalNum}/>}>
                    <View tabLabel={strings.tab1 + "|ios-home"}>
                        <Products ref="products" openCategoryDetail={this.openCategoryDetail}/>
                    </View>
                    <ScrollView tabLabel={strings.tab2 + "|navicon-round"} style={[styles.tabView,{padding: 0,backgroundColor:'#FFFFFF'}]}>
                        <Category ref="category" openProductsDetail={this.openProductsDetail}/>
                    </ScrollView>
                    <ScrollView tabLabel={strings.tab3 + "|ios-heart-outline"} style={[styles.tabView,{paddingLeft:0,paddingRight:0,paddingTop:0,backgroundColor:'#FFFFFF'}]}>
                        <Favorites ref="favorites" openDetailByFavorites={this.openDetailByFavorites}/>
                    </ScrollView>
                    <View tabLabel={strings.tab4 + "|bag"} style={[styles.tabView,{paddingLeft:0,paddingRight:0,paddingTop:0,backgroundColor:'#FFFFFF'}]}>
                        <Cart ref="cart" deleteCartItems={this.deleteCartItems} isDeleteStatus={this.state.txtEdit} commodityClearing={this.commodityClearing} openDetailByFavorites={this.openDetailByFavorites} setCartTotalNum={this.setCartTotalNum}/>
                    </View>
                    <UserCenter ref="userCenter" setLanguage={this.setLanguage} backLogin={this.backLogin} onShowHistoryList={this.onShowHistoryList} onAddressManager={this.onAddressManager} tabLabel={strings.tab5 + "|person"}/>
                </ScrollableTabView>
                {btnEdit}
                <Animated.View style={[styles.floatView,{left: this.state.fadeAnim}]}>
                    <ProductsDetail ref="productsDetail" ServerData={this.state.ServerData} orderBuyBtn={this.orderBuyBtn} btnBuyNow={this.btnBuyNow} showWebViewDesc={this.showWebViewDesc} backBarcode={this.backBarcode} favoritesInfo={this.favoritesInfo} imageUrl={this.state.imageUrl} desc={this.state.desc} favoritesIcon={this.state.favoritesIcon}/>
                </Animated.View>
                <Animated.View style={[styles.floatView,{left: this.state.fadeWebViewAnim}]}>
                    <ToolbarAndroid
                        navIcon={require('image!leftarrow1')}
                        onIconClicked={this.backProductDesc}
                        style={styles.viewToolbarIcon}
                        titleColor='#A1A3A5'
                        title={this.state.name.substr(0,28)} />

                    <WebView
                      automaticallyAdjustContentInsets={false}
            					contentInset={{top: 0, right: 0, bottom: 0, left: 0}}
            					source={{html: this.state.htmlDesc}}
                      opaque={false}
                      javaScriptEnabled={true} />
                </Animated.View>
            </View>
        );
    }
});

module.exports = Barcode;
