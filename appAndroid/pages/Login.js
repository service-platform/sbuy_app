var React = require('react-native');
var styles = require('./../styles/Login');
var Barcode = require('./Barcode');
var TimerMixin = require('react-timer-mixin');
var Icon = require('react-native-vector-icons/Ionicons');
var t = require('tcomb-form-native');
var Form = t.form.Form;
//更加精细的判断
var Person = t.struct({
  // a required string(t.maybe(t.String) : 可选字段)
  email: t.refinement(t.String, function (n) {
    var regx = /^(\w)+(\.\w+)*@(\w)+((\.\w+)+)$/;
    return regx.test(n);
  }),
  // an required string
  password: t.refinement(t.String, function (n) {
    var regx = /^[A-Za-z0-9]{6,}$/;
    return regx.test(n);
  }),
});

var {
    View,//div
    Text,//label span
    Image,
    ScrollView,
    TextInput,// input
    ToastAndroid, //推送模式
    TouchableHighlight,//按钮
    AsyncStorage,
    TouchableOpacity
} = React;
import store from './../redux/store';
import { postsLogin } from './../redux/actions/loginAction';
var strings = require('./../utils/LocalizedStrings');

var Login = React.createClass({
    mixins: [TimerMixin],
    getInitialState: function () {
        return {
            isShowPassword: true,
            iconName: 'ios-eye-outline',
            editable: true,
            formError: {
              emailErrorState: false,
              passwordErrorState: false,
            },
            formBlur: {
              emailBlurState: false,
              passwordBlurState: false,
            },
            formValues: null
        };
    },
    componentDidMount: function() {
        //访问表单内部字段
        this.refs.form.getComponent('email').refs.input.focus();

        //如果有账户密码，自动登录
        var me = this;
        AsyncStorage.getItem("user")
        .then((value) => {
          if(value != null){
              var obj = JSON.parse(value);
              me.setState({
                editable: false,
                formValues:{
                    email: obj.email,
                    password: obj.password
                }
              },...this.state)
              me.loginButton();
          }
        })
        .catch((error) => {debugger;}).done();
    },
    loginButton: function(){
        //this.refs.form.validate().isValid() [true:没有错误 | false:有至少一个错误]
        var me = this;
        if(this.refs.form.validate().isValid() == false){
            me.setState({
              editable: true,
            },...this.state)
            ToastAndroid.show(strings.toastTipCorrectAccount, ToastAndroid.SHORT);
        }else{
          me.setState({
            editable: false,
          },...this.state)
          store.dispatch(postsLogin(this.state.formValues)).then((json)=>{
              var serverData = store.getState().loginReducer.ServerData;
              if(serverData.login_status == true){
                  window.GLOBAL.user_id = serverData.id;
                  AsyncStorage.setItem("user",JSON.stringify(this.state.formValues))
                  .then(() => {
                      ToastAndroid.show(strings.toastTipLoginSuccess, ToastAndroid.SHORT);
                  })
                  .catch((error) => {debugger;}).done();

                  this.setState({editable: false},...this.state);
                  this.setTimeout(
                    () => {
                      const { navigator } = me.props;
                      if(navigator) {
                         navigator.push({
                             name: 'Barcode',
                             component: Barcode
                         })
                      }
                      me.setState({editable: true});
                    },
                    1000
                  );
              }else{
                  me.setState({
                    editable: true,
                  },...this.state)
                  ToastAndroid.show("对不起，用户名 或 密码错误。", ToastAndroid.SHORT);
              }
          }).catch((error)=>{
              me.setState({
                editable: true,
              },...this.state)
              ToastAndroid.show("对不起，数据请求失败：" + error.message, ToastAndroid.SHORT);
          }).done();
        }
    },
    facebookButton: function(){
        //获取所有值
        var value = this.refs.form.getValue();
        if (value) {
          console.log(value);
          // 清除所有值
          this.setState({ formValues: null });
        }
        this.setState({isShowPassword: true})
    },
    onChange: function(value,fieldName) {
      //表单全部一起验证
      // this.refs.form.validate();
      //把值，写入到 state 对象中
      // this.setState({formValues: value});
      //针对每一个，逐个进行验证
      switch (fieldName[0]) {
        case "email":
            this.setState({
              formValues: value,
              formError: {
                ...this.state.formError,
                emailErrorState: !/^(\w)+(\.\w+)*@(\w)+((\.\w+)+)$/.test(value.email)
              }
            });
            this.refs.form.getComponent('email').removeErrors();
            return;
        case "password":
            this.setState({
              formValues: value,
              formError: {
                ...this.state.formError,
                passwordErrorState: !/^[A-Za-z0-9]{6,}$/.test(value.password)
              }
            });
            this.refs.form.getComponent('password').removeErrors();
            return;
        default:
          break;
      }
      this.refs.form.render();
      // debugger;
    },
    render: function () {
        // optional rendering options (see documentation)
        var options = {
          // auto: 'placeholders', //自动填充提示 ['placeholders' | 'none' <默认>]
          // order: ['Email','age', 'Password'],  //字段排序
          fields: {
            email:{
              label: strings.lblEmail,
              editable: this.state.editable,
              keyboardType: 'email-address',
              placeholder: strings.lblPlaceholderEmail,
              hasError: this.state.formError.emailErrorState,
              hasBlur: this.state.formBlur.emailBlurState,
              error: strings.lblEmailError,  //可以自定义：<i>A custom error message</i>
              onBlur: ()=>{
                this.setState({
                  formBlur: {
                    ...this.state.formBlur,
                    emailBlurState: false
                  }
                });
              },
              onFocus: () => {
                this.setState({
                  formBlur: {
                    ...this.state.formBlur,
                    emailBlurState: true
                  }
                });
              }
            },
            password:{
              label: strings.lblPassword,
              editable: this.state.editable,
              maxLength: 12,
              secureTextEntry: this.state.isShowPassword,
              isShowPassword: true,
              placeholder: strings.lblPlaceholderPassword,
              hasError: this.state.formError.passwordErrorState,
              hasBlur: this.state.formBlur.passwordBlurState,
              error: strings.lblPasswordError,
              onBlur: ()=>{
                this.setState({
                  formBlur: {
                    ...this.state.formBlur,
                    passwordBlurState: false
                  }
                });
              },
              onFocus: () => {
                this.setState({
                  formBlur: {
                    ...this.state.formBlur,
                    passwordBlurState: true
                  }
                });
              }
            }
          }
        };
        return (
            <View style={styles.container}>
                <Image source={require('image!background')} style={styles.imageBackground}/>
                <View style={styles.container}>
                  <Form
                        ref="form"
                        type={Person}
                        options={options}
                        value={this.state.formValues}
                        onChange={this.onChange}
                  />
                  <View style={{flexDirection:'column'}}>
                      <TouchableHighlight onPress={this.loginButton} style={styles.button} underlayColor='#99d9f4'>
                          <Text style={styles.buttonText}>{strings.btnSignIn}</Text>
                      </TouchableHighlight>
                  </View>
                </View>
            </View>
            );
    }
});
module.exports = Login;
