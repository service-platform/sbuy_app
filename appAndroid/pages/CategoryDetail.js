'use strict';
var React = require('react-native');
var styles = require('./../styles/CategoryDetail');

var {
    View,//div
    Text,//label span
    Image,
    ListView,
    Dimensions,
    ScrollView,
    ToastAndroid,
    ToolbarAndroid,
    TouchableHighlight,
} = React;

var CategoryDetail = React.createClass({
    getInitialState: function () {
        return {
          dataSource: new ListView.DataSource({
              rowHasChanged: (r1, r2) => r1 !== r2
          }),
        }
    },
    componentWillMount: function() {
      var obj1 = {
        imageUrl: require('image!pizza1'),
        name: 'Pizza1',
        oldPrice: 7.50,
        realPrice: 10.50
      };
      var obj2 = {
        imageUrl: require('image!pizza1'),
        name: 'Pizza2',
        oldPrice: 8.00,
        realPrice: 12.50
      };
      this.setState({
        dataSource: this.getDataSource([obj1,obj2])
      })
    },
    backPage: function(){
      const { navigator } = this.props;
      if(navigator) {
         navigator.pop();
      }
    },
    getDataSource: function(subjects: Array<any>): ListView.DataSource {
        return this.state.dataSource.cloneWithRows(subjects);
    },
    renderSeparator: function(sectionID, rowID, adjacentRowHighlighted){
        var style = styles.rowSeparator;
        if (adjacentRowHighlighted) {
            style = [style, styles.rowSeparatorHide];
        }
        return (
            <View key={'SEP_' + sectionID + '_' + rowID}  style={style}/>
        );
    },
    renderRow: function(data, sectionID, rowID, highlightRowFunc){
        var image = data.imageUrl;
        var title = data.name;
        var oldPrice = data.oldPrice;
        var realPrice = data.realPrice;

        return (
          <TouchableHighlight
              key={data.id}
              onPress={()=>{}}
              underlayColor="#E6E6E6aa"
              onShowUnderlay={() => highlightRowFunc(sectionID, rowID)}
              onHideUnderlay={() => highlightRowFunc(null, null)}>

                <View style={styles.row}>
                    <Image
                        source={image}
                        style={styles.image} />
                    <View style={styles.textContainer}>
                        <View style={{}}>
                          <Text style={styles.oldPrice} numberOfLines={1}>
                            原价：<Text style={{textDecorationLine :'line-through'}}>{'$ ' + oldPrice}</Text>，
                            现价：<Text style={styles.txtRealPrice}>{'$ ' + realPrice}</Text>
                          </Text>
                          <View style={{borderTopWidth:1,borderColor:'gray',width:80}}></View>
                        </View>

                        <View style={styles.textRealPrice}>
                          <Text style={styles.textNum}>$ 10.50</Text>
                        </View>
                    </View>

                </View>
              </TouchableHighlight>
        );
    },
    render: function () {
      return(
        <View style={{backgroundColor: '#FFFFFF'}}>
            <ToolbarAndroid
                navIcon={require('image!leftarrow1')}
                onIconClicked={() => {this.backPage()}}
                style={styles.toolbar}
                titleColor="#A1A3A5"
                title={this.props.name} />
            <View style={{borderTopWidth: 1,borderColor: '#E7EAEC'}}>
                <ScrollView>
                  <ListView
                      dataSource={this.state.dataSource}
                      renderRow={this.renderRow}
                      renderSeparator={this.renderSeparator}
                      automaticallyAdjustContentInsets={false}
                      keyboardDismissMode="on-drag"
                      keyboardShouldPersistTaps={true}
                      showsVerticalScrollIndicator={false}
                    />
                </ScrollView>
            </View>
        </View>
      )
    }
});

module.exports = CategoryDetail;
