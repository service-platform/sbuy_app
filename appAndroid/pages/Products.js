var React = require('react');
var ReactNative = require('react-native');
var styles = require('./../styles/Products');
var MarkerSelection = require('./Component/MarkerSelection');

var {
    View,
    Text,
    Image,
    TextInput,
    Dimensions,
    ToastAndroid,
    ListView,
    ScrollView,
    TouchableHighlight,
    RefreshControl,
    TouchableOpacity
} = ReactNative;
import store from './../redux/store';
import { postsGoodsUrl } from './../redux/actions/productsAction';

var ProgressBar = require('ProgressBarAndroid');
var strings = require('./../utils/LocalizedStrings');
let { width, height } = Dimensions.get('window');
var pageSize = 8;

var Item = React.createClass({
  render: function() {
    var image = "";
    if(this.props.data.image != undefined){
      if(this.props.data.image.includes("http://") == false){
        image = ServerURL + this.props.data.image;
      }else{
        image = this.props.data.image;
      }
    }
    var title = this.props.data.name;

    return (
      <TouchableHighlight
          key={this.props.data.id}
          onPress={()=>{this.props.openCategoryDetail(this.props.data.id)}}
          underlayColor="#E6E6E6aa"
          style={styles.touchablelight}>
              <View style={styles.row}>
                  <Image
                      source={{uri:image}}
                      resizeMode="contain"
                      style={styles.cellImage} />
                  <View style={styles.textContainer}>
                      <Text style={styles.movieTitle} numberOfLines={1}>
                          {(title.length >= strings.splitStr) ? title.substr(0,strings.splitStr) + '...': title }
                      </Text>
                  </View>
                  <View style={styles.viewToDetail}>
                    <View style={styles.viewNum}>
                      <Text style={styles.textNum}>{this.props.data.price}</Text>
                    </View>
                  </View>
              </View>
          </TouchableHighlight>
    );
  }
});

var Products = React.createClass({
  getInitialState: function () {
    return {
      txtProduct: "",
      txtBorderColor: '#E5E6E7',
      dataSource: new ListView.DataSource({
         rowHasChanged: (r1, r2) => r1 !== r2
      }),
      ServerData: [],
      dataTypes:[
        {name:'服装', value: 1},
        {name:'服饰', value: 2},
        {name:'化妆品', value: 3},
        {name:'食品', value: 4},
        {name:'保健品', value: 5},
        {name:'生活日用品', value: 6},
        {name:'设计师品牌', value: 7},
        {name:'酷玩', value: 8}
      ],
      viewTypeWidth: width
    }
  },
  btnReloadData: function(){
    this.setState({...this.state})

    store.dispatch(postsGoodsUrl(ServerURL + "/api/")).then((json) =>{
        var dataState = store.getState();

        this.setState({
          dataSource: this.getDataSource(dataState.productsReducer.ServerData),
          ServerData: dataState.productsReducer.ServerData
        });
    }).catch((error)=>{
      ToastAndroid.show("对不起，数据请求失败：" + error.message, ToastAndroid.SHORT);
    }).done();
  },
  componentDidMount: function() {
      store.dispatch(postsGoodsUrl(ServerURL + "/api/")).then((json) =>{
          var dataState = store.getState();

          this.setState({
            dataSource: this.getDataSource(dataState.productsReducer.ServerData),
            ServerData: dataState.productsReducer.ServerData
          });
      }).catch((error)=>{
        ToastAndroid.show("对不起，数据请求失败：" + error.message, ToastAndroid.SHORT);
      }).done();
  },
  openCategoryDetail: function(id){
      this.props.openCategoryDetail(id);
  },
  getDataSource: function(subjects: Array<any>): ListView.DataSource {
      return this.state.dataSource.cloneWithRows(subjects);
  },
  renderRow:  function(rowData: string, sectionID: string, rowID: string): ReactElement<any> {
    return (<Item data={rowData} openCategoryDetail={this.openCategoryDetail}/>);
  },
  btnSearch: function(){
      var urlSearch = ""; this.refs.txtInputProduct.blur();
      if(this.state.txtProduct == ""){
        urlSearch = ServerURL + "/api/";
      }else{
        urlSearch = ServerURL + "/product/search/" + this.state.txtProduct + "/";
      }
      store.dispatch(postsGoodsUrl(urlSearch)).then((json) =>{
          var dataState = store.getState();

          this.setState({
            dataSource: this.getDataSource(dataState.productsReducer.ServerData),
            ServerData: dataState.productsReducer.ServerData
          });
      }).catch((error)=>{
        ToastAndroid.show("对不起，数据请求失败：" + error.message, ToastAndroid.SHORT);
      }).done();
  },
  onSelectType: function(data){
      this.refs.txtInputProduct.blur();
      this.setState({txtProduct: data.name});
      this.btnSearch();
  },
  onBlurText: function(){
      this.setState({viewTypeWidth: width});
  },
  onFocusText: function(){
      this.setState({viewTypeWidth: 0});
  },
  onEndReached: function(){
      debugger;
  },
  render: function () {
      return (
          <View style={styles.container}>
              <View style={styles.viewSearch}>
                  <View style={styles.viewInput}>
                      <TextInput  style={styles.txtInput}
                        ref="txtInputProduct"
                        onChangeText={(text) => this.setState({txtProduct: text})}
                        value={this.state.txtProduct}
                        style={styles.txtInput}
                        onBlur={this.onBlurText}
                        onFocus={this.onFocusText}
                        placeholder={strings.txtInputProduct}
                        placeholderTextColor="#C3C3C3" />
                    </View>
                    <TouchableOpacity onPress={this.btnSearch} style={styles.btnSearch} activeOpacity={0.3}>
                        <Image source={require('image!searchindex')} style={styles.searchIndex}></Image>
                    </TouchableOpacity>
              </View>

              <ListView
                  dataSource={this.state.dataSource}
                  renderRow={this.renderRow}
                  style={styles.scrollView}
                  contentContainerStyle={styles.separator}

                  initialListSize={3}
                  scrollRenderAheadDistance={height}
                  pageSize={2}
                  onEndReachedThreshold={800}
                  onEndReached={this.onEndReached}

                  // renderFooter={()=>{
                  //   return(
                  //     <View style={{alignItems: 'center',width: width,marginBottom: 10}}>
                  //       <View style={{flexDirection: 'row',alignSelf:'center'}}>
                  //           <ProgressBar styleAttr="Inverse" style={{width:30,height:30,marginRight: 5,alignSelf:'center'}} />
                  //           <Text style={{alignSelf:'center'}}>加载更多信息</Text>
                  //       </View>
                  //     </View>
                  //   );
                  // }}
                />


              <View style={[styles.viewTypes,{left: this.state.viewTypeWidth}]}>
                  <MarkerSelection data={this.state.dataTypes} title={strings.lblCategory} onSelect={this.onSelectType} />
              </View>
          </View>
        )
   }
});

module.exports = Products;
