'use strict';
var React = require('react-native');
var styles = require('./../styles/Category');
var {
    View,
    Text,
    Dimensions,
} = React;
var strings = require('./../utils/LocalizedStrings');
let { width, height } = Dimensions.get('window');

var Category = React.createClass({
    render: function () {
      return(
        <View style={styles.container}>
          <Text style={styles.txtTip}>启动摄像头，请稍后...</Text>
        </View>
      )
    }
});

module.exports = Category;
