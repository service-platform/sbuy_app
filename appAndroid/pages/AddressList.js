import React from 'react';
import {
    Image,
    StyleSheet,
    Text,
    View,
    Alert,
    ListView,
    Dimensions,
    Component,
    ToastAndroid,
    ToolbarAndroid,
    TouchableOpacity,
    TouchableHighlight,
    TouchableWithoutFeedback
} from 'react-native';
import store from './../redux/store';
import { getGetAddressList,getDeleteAddressById,getUpdateAddressInfo } from './../redux/actions/productsAction';
var {height, width} = Dimensions.get('window');
var Icon = require('react-native-vector-icons/Ionicons');
var AddAddress = require('./AddAddress');
var strings = require('./../utils/LocalizedStrings');

var AddressList = React.createClass({
    getInitialState: function () {
      return {
        dataSource: new ListView.DataSource({
          rowHasChanged: (r1, r2) => r1 !== r2,
        }),
        red: '#baa071',

        mask: 0,
        addressId: 0,
        receiver: '',
        delivery_address: '',
      }
    },
    getDataSource: function(subjects: Array<any>): ListView.DataSource {
        return this.state.dataSource.cloneWithRows(subjects);
    },
    onItemClick: function(ItemData){
        var me = this;
        if(ItemData.default == 1){
          Alert.alert(strings.alertPrompt,strings.alertTipAddress,
            [
              {text: strings.alertDeleteAddress, onPress: () => {
                  var objId = {
                      id: ItemData.id
                  };
                  store.dispatch(getDeleteAddressById(objId)).then((json) =>{
                      if(json.ServerData.delete_address == true){
                          ToastAndroid.show('删除成功', ToastAndroid.SHORT);
                          me.refreshData();
                      }else{
                          ToastAndroid.show('删除失败', ToastAndroid.SHORT);
                      }
                  }).catch((error)=>{
                      ToastAndroid.show("对不起，数据请求失败：" + error.message, ToastAndroid.SHORT);
                  }).done();
              }}
          ]);
        }else{
          Alert.alert(strings.alertPrompt,strings.alertTipAddress,
            [
              {text: strings.alertSetDefault,onPress: ()=>{
                  var objItem = {
                      id: ItemData.id,
                      user: window.GLOBAL.user_id,
                      receiver: ItemData.receiver,
                      phone: ItemData.phone,
                      delivery_address: ItemData.delivery_address,
                      default: 1,
                  };
                  store.dispatch(getUpdateAddressInfo(objItem)).then((json) =>{
                      if(json.ServerData.update_address == undefined){
                          ToastAndroid.show(strings.toastSetDefaultOk, ToastAndroid.SHORT);
                          me.refreshData(); me.setState({mask: 1});
                      }else{
                          ToastAndroid.show(strings.toastSetDefaultFailure, ToastAndroid.SHORT);
                      }
                  }).catch((error)=>{
                      ToastAndroid.show("对不起，数据请求失败：" + error.message, ToastAndroid.SHORT);
                  }).done();
              }},
              {text: strings.alertDeleteAddress, onPress: () => {
                  var objId = {
                      id: ItemData.id
                  }
                  store.dispatch(getDeleteAddressById(objId)).then((json) =>{
                      if(json.ServerData.delete_address == true){
                          ToastAndroid.show(strings.toastDeleteOk, ToastAndroid.SHORT);
                          me.refreshData();
                      }else{
                          ToastAndroid.show(strings.toastDeleteFailure, ToastAndroid.SHORT);
                      }
                  }).catch((error)=>{
                      ToastAndroid.show("对不起，数据请求失败：" + error.message, ToastAndroid.SHORT);
                  }).done();
              }}
          ]);
        }

    },
    onItemEdit: function(itemData){
        const { navigator } = this.props;
        if(navigator) {
           navigator.push({
             name: 'AddAddress',
             component: AddAddress,
             params: {
                mark: 'Update',
                refreshData: this.refreshData,
                setAddressData: this.setAddressData,
                id: itemData.id,
                receiver: itemData.receiver,
                phone: itemData.phone,
                delivery_address: itemData.delivery_address,
                default: itemData.default
             }
           });
        }
    },
    setAddressData: function(data) {
        this.setState({
            mask: 2,
            addressId: data.addressId,
            receiver: data.receiver,
            delivery_address: data.delivery_address,
        })
    },
    refreshData: function(){
        store.dispatch(getGetAddressList(window.GLOBAL.user_id)).then((json) =>{
            this.setState({
                dataSource: this.getDataSource(json.ServerData)
            });
        }).catch((error)=>{
            ToastAndroid.show("对不起，数据请求失败：" + error.message, ToastAndroid.SHORT);
        }).done();
    },
    componentWillMount: function(){
        this.refreshData();
    },
    backPage: function(){
        const { navigator } = this.props;
        if(navigator) {
           navigator.pop();
        }
        if(this.props.refreshAddressData != undefined){
            if(this.state.mask == 1){
                this.props.refreshAddressData(true);
            }else if(this.state.mask == 2){
                this.props.refreshAddressData(false,{
                  addressId: this.state.addressId,
                  receiver: this.state.receiver,
                  delivery_address: this.state.delivery_address,
                });
            }
        }
    },
    addAddress: function(){
        const { navigator } = this.props;
        if(navigator) {
           navigator.push({
             name: 'AddAddress',
             component: AddAddress,
             params: {
                mark: 'Add',
                refreshData: this.refreshData,
                setAddressData: this.setAddressData,
             }
           });
        }
    },
    renderRow: function(rowData, sectionID, rowID, highlightRowFunc){
        if(rowData.default == 1){
            return (
                <TouchableOpacity activeOpacity={0.5} onLongPress={()=>{this.onItemClick(rowData)}}>
                    <View style={styles.addressBox}>
                         <View style={styles.namePhone} >
                             <Text style={[styles.name,{color:this.state.red}]}>{rowData.receiver}</Text>
                             <Text style={[styles.phone,{color:this.state.red}]}>{rowData.phone}</Text>
                         </View>
                         <View style={styles.address}>
                             <Text style={styles.pretermit}>{strings.lblAddressDefault}</Text>
                             <Text style={[styles.txtAddress,{width: (width - 100)}]}>{rowData.delivery_address}</Text>
                         </View>
                         <TouchableOpacity onPress={()=>{this.onItemEdit(rowData)}} activeOpacity={0.5} style={styles.btnEdit}>
                            <Image source={require('image!edit')} style={styles.edit} />
                         </TouchableOpacity>
                    </View>
                </TouchableOpacity>
            )
        }else{
            return (
                <TouchableOpacity activeOpacity={0.5} onLongPress={()=>{this.onItemClick(rowData)}}>
                    <View style={styles.addressBox}>
                        <View style={styles.namePhone} >
                            <Text style={styles.name}>{rowData.receiver}</Text>
                            <Text style={styles.phone}>{rowData.phone}</Text>
                        </View>
                        <View style={styles.address}>
                            <Text style={styles.txtAddress}>{rowData.delivery_address}</Text>
                        </View>
                        <TouchableOpacity onPress={()=>{this.onItemEdit(rowData)}} activeOpacity={0.5} style={styles.btnEdit}>
                            <Image source={require('image!edit')} style={styles.edit} />
                        </TouchableOpacity>
                    </View>
                </TouchableOpacity>
            )
        }
    },
    render: function(){
        return (
            <View style={styles.container}>
                <ToolbarAndroid
                    navIcon={require('image!leftarrow1')}
                    onIconClicked={this.backPage}
                    style={styles.toolbar}
                    titleColor="#A1A3A5"
                    title={strings.lblAddressToolbarTitle} />
                <ListView
                    ref="ListView"
                    dataSource={this.state.dataSource}
                    renderRow={this.renderRow}
                  />

                <View style={styles.addAddress}>
                    <TouchableHighlight onPress={this.addAddress}>
                        <View style={styles.button}>
                           <Icon name="plus" size={22} style={styles.iconPlus} />
                           <Text style={styles.color}>{strings.btnAddAddress}</Text>
                        </View>
                    </TouchableHighlight>
                </View>
            </View>
        );
    }
});
module.exports = AddressList;

var styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#F9F9F9'
    },
    toolbar:{
        backgroundColor: '#F3F3F4',
        height: 55,
    },
    name:{
        fontSize:16,
        color:'#232326'
    },
    phone:{
        marginLeft:25,
        fontSize:16,
        color:'#232326'
    },
    addressBox:{
        width:width,
        backgroundColor:'#FFF',
        paddingTop:15,
        paddingBottom:15,
        paddingLeft:20,
        borderBottomWidth:1,
        borderTopWidth:1,
        borderColor:'#dddddd',
        marginTop:10
    },
    namePhone:{
        flexDirection:'row'
    },
    txtAddress:{
        width: (width - 40),
        fontSize:13,
        flexWrap:"wrap"
    },
    pretermit:{
        marginRight:20,
        height:20,
        paddingLeft:6,
        paddingRight:6,
        backgroundColor:"#baa071",
        color:"#fff",
        textAlign:'center'
    },
    address:{
        flexDirection:'row',
        paddingTop:8
    },
    btnEdit: {
        position:'absolute',
        top: 10,
        right: 20
    },
    edit:{
        width:27,
        height:27,
    },
    tick:{
        width:30,
        height:30,
        marginRight:10
    },
    addAddress:{
        width:width,
        height:50,
        backgroundColor:"#f8f8f8",
        position:'absolute',
        bottom:0,
        borderTopWidth:1,
        borderTopColor:'#e0e0e0',
        justifyContent:"center",
        alignItems:"center"
    },
    button:{
        width: 200,
        height: 35,
        backgroundColor:"#1D1D1D",
        justifyContent:"center",
        alignItems:"center",
        flexDirection:"row"
    },
    color:{
        color:"#fff",
        fontSize:16
    },
    iconPlus:{
        color: '#fff',
        marginRight: 10
    }
});
