'use strict';
var React = require('react-native');
var styles = require('./../styles/OrderDetail');
var {
    View,//div
    Text,//label span
    Image,
    ListView,
    ScrollView,
    ToolbarAndroid,
    ToastAndroid,
    TouchableHighlight,
} = React;
var QRCode = require('react-native-qrcode');
import * as Sqlite from './../utils/Sqlite';
var strings = require('./../utils/LocalizedStrings');

var OrderDetail = React.createClass({
    getDefaultProps: function() {
        return {
            orderId: '',
        };
    },
    propTypes: {
        orderId: React.PropTypes.string.isRequired,
    },
    getInitialState: function () {
        return {
          dataSource: new ListView.DataSource({
              rowHasChanged: (r1, r2) => r1 !== r2
          }),
          total: '',
          orderId: '',
          name: '',
          address: '',
          paymentType: '',
          deliveryType: '',
          QRCodeText: ''
        }
    },
    componentWillMount: function() {
        debugger;
        var obj = {
          id: this.props.orderObj.ID,
          total: this.props.orderObj.money,
          name: this.props.orderObj.data.receiver,
          address: this.props.orderObj.data.address,
          paymentType: '支付宝',
          deliveryType: '快递',
          list: []
        };
        debugger;
        for (var i = 0; i < this.props.orderObj.data.item.length; i++) {
          var tempObj = this.props.orderObj.data.item;
          var orderDetailList = {
            id: tempObj[i].id,
            orderId: this.props.orderObj.ID,
            productId: tempObj[i].product,

            imageUrl: "",
            name: "",
            desc: "",
            price: tempObj[i].price,
            totalNum: tempObj[i].quantity
          }
          obj.list.push(orderDetailList);
        }
        this.refreshData(obj);
    },
    backPage: function(){
      const { navigator } = this.props;
      if(navigator) {
         navigator.pop();
      }
    },
    refreshData: function(baseInfo){
      //翻译 支付类型
      switch (baseInfo.paymentType) {
        case 1:
          baseInfo.paymentType = "微信";
          break;
        case 2:
          baseInfo.paymentType = "现金";
          break;
        case 3:
          baseInfo.paymentType = "支付宝";
          break;
        case 4:
          baseInfo.paymentType = "银联网银";
          break;
      }
      //翻译 配送方式
      switch (baseInfo.deliveryType) {
        case 1:
          baseInfo.deliveryType = "带走";
          break;
        case 2:
          baseInfo.deliveryType = "自取";
          break;
        case 3:
          baseInfo.deliveryType = "快递";
          break;
      }
      if(baseInfo != null){
        this.setState({
          dataSource: this.getDataSource(baseInfo.list),
          total: baseInfo.total,
          orderId: baseInfo.id,
          name: baseInfo.name,
          address: baseInfo.address,
          paymentType: baseInfo.paymentType,
          deliveryType: baseInfo.deliveryType,
          QRCodeText: "weixin://wxpay/bizpayurl?pr=T2eiHsn",
        });
      }
    },
    getDataSource: function(subjects: Array<any>): ListView.DataSource {
        return this.state.dataSource.cloneWithRows(subjects);
    },
    renderSeparator: function(sectionID, rowID, adjacentRowHighlighted){
        var style = styles.rowSeparator;
        if (adjacentRowHighlighted) {
            style = [style, styles.rowSeparatorHide];
        }
        return (
            <View key={'SEP_' + sectionID + '_' + rowID}  style={style}/>
        );
    },
    renderRow: function(data, sectionID, rowID, highlightRowFunc){
        var image = data.image;
        var title = data.name;
        var rating = data.price;
        var desc = data.desc;
        var totalNum = data.totalNum;

        return (
          <TouchableHighlight
              key={data.id}
              onPress={()=>{}}
              underlayColor="#E6E6E6aa"
              onShowUnderlay={() => highlightRowFunc(sectionID, rowID)}
              onHideUnderlay={() => highlightRowFunc(null, null)}>

                <View style={styles.row}>
                    <Image
                        source={{uri: image}}
                        style={styles.cellImage} />
                    <View style={styles.textContainer}>
                        <Text style={styles.movieTitle} numberOfLines={1}>
                            {title}
                        </Text>
                        <Text style={styles.movieRating} numberOfLines={1}>
                            {strings.lblTotal + "$ " + rating}
                        </Text>
                        <Text style={styles.movieRating} numberOfLines={1}>
                            {strings.lblDescription + "：" + desc}
                        </Text>
                    </View>
                    <View style={styles.viewToDetail}>
                      <View style={styles.viewNum}>
                        <Text style={styles.textNum}>{totalNum}</Text>
                      </View>
                    </View>
                </View>
              </TouchableHighlight>
        );
    },
    render: function () {
      return(
        <View style={styles.container}>
            <ToolbarAndroid
                navIcon={require('image!leftarrow1')}
                onIconClicked={() => {this.backPage()}}
                style={styles.toolbar}
                titleColor="#A1A3A5"
                title={strings.lblPurchaseOrder} />
            <ScrollView style={{borderTopWidth: 1,borderColor: '#E7EAEC'}}>
              <ListView
                  dataSource={this.state.dataSource}
                  renderRow={this.renderRow}
                  renderSeparator={this.renderSeparator}
                  automaticallyAdjustContentInsets={false}
                  keyboardDismissMode="on-drag"
                  keyboardShouldPersistTaps={true}
                  showsVerticalScrollIndicator={false}
                />
                <View style={{marginTop: 10}}>
                    <Text style={{alignSelf:'flex-end',marginRight:10}}>
                        {strings.formatString(strings.lblTotal)}<Text style={{color: 'red'}}>{this.state.total}</Text>
                    </Text>
                </View>
                <View>
                    <Text style={{alignSelf:'flex-start',marginLeft:10}}>
                        {strings.lblOrderId} ID：<Text style={{color: 'red'}}>{this.state.orderId}</Text>
                    </Text>
                </View>
                <View>
                    <Text style={{alignSelf:'flex-start',marginLeft:10}}>
                        {strings.lblName}<Text style={{color: 'red'}}>{this.state.name}</Text>
                    </Text>
                </View>
                <View>
                    <Text style={{alignSelf:'flex-start',marginLeft:10}}>
                        {strings.lblDeliveryaddress}<Text style={{color: 'red'}}>{this.state.address}</Text>
                    </Text>
                </View>
                <View>
                    <Text style={{alignSelf:'flex-start',marginLeft:10}}>
                        {strings.lblPaymentMethod}<Text style={{color: 'red'}}>{this.state.paymentType}</Text>，
                        {strings.lblDeliveryMethod}<Text style={{color: 'red'}}>{this.state.deliveryType}</Text>
                    </Text>
                </View>
            </ScrollView>
        </View>
      )
    }
});

module.exports = OrderDetail;
