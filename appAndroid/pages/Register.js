var React = require('react-native');
var styles = require('./../styles/Register');
var verifyStyle = require('./../styles/verifyStyle');
var Icon = require('react-native-vector-icons/Ionicons');
var TimerMixin = require('react-timer-mixin');
var t = require('tcomb-form-native');
import store from './../redux/store';
import {userRegister} from './../redux/actions/loginAction';
var Barcode = require('./Barcode');


t.form.Form.stylesheet = verifyStyle;
var Form = t.form.Form;

var {
    View,
    Text,
    Image,
    ToastAndroid,
    TouchableHighlight,
    ToolbarAndroid
} = React;
var strings = require('./../utils/LocalizedStrings');

var Register = React.createClass({
    mixins: [TimerMixin],
    getInitialState: function () {
        return {
          formValues: null,
          formError: {
            emailErrorState: false,
            passwordErrorState: false,
            confirmPwdErrorState: false,
            fullNameErrorState: false,
              usernameErrorState : false,
              phoneNumberErrorState : false
          },
          formBlur: {
            emailBlurState: false,
            passwordBlurState: false,
            confirmPwdBlurState: false,
            fullNameBlurState: false,
            phoneNumberBlurState: false,
            addressBlurState: false,
            streetBlurState: false,
              usernameBlurState :false
          }
        };
    },
    onChange:function (value,fieldName) {
      switch (fieldName[0]) {
        case "email":
            this.setState({
              formValues: value,
              formError: {
                ...this.state.formError,
                emailErrorState: !/^(\w)+(\.\w+)*@(\w)+((\.\w+)+)$/.test(value.email)
              }
            });
            return;
        case "password":
            this.setState({
              formValues: value,
              formError: {
                ...this.state.formError,
                passwordErrorState: !/^[A-Za-z0-9]{6,}$/.test(value.password)
              }
            });
            return;
        case "confirmPassword":
            if(this.state.formValues == null){
              this.setState({
                formValues: value,
                formError: {
                  ...this.state.formError,
                  confirmPwdErrorState: true
                }
              });
            }else{
              if(this.state.formValues.password == undefined){
                this.setState({
                  formValues: value,
                  formError: {
                    ...this.state.formError,
                    confirmPwdErrorState: true
                  }
                });
              }else{
                this.setState({
                  formValues: value,
                  formError: {
                    ...this.state.formError,
                    confirmPwdErrorState: !(value.confirmPassword === this.state.formValues.password)
                  }
                });
              }
            }
            return;
        case "FullName":
            this.setState({
              formValues: value,
              formError: {
                ...this.state.formError,
                fullNameErrorState: (value.FullName === "")
              }
            });
            return;
        case "username":
            this.setState({
                formValues: value,
                formError: {
                  ...this.state.formError,
                  usernameErrorState: (value.username === "")
                }
            });
            return;
        case "PhoneNumber":
            this.setState({
                formValues: value,
                formError: {
                  ...this.state.formError,
                  phoneNumberErrorState: !/^[0-9]*[-]*$/.test(value.PhoneNumber)
                }
            });
            return;
        default:
          break;
      }
    },
    registerBtn: function(){
        var me = this;
      if(this.refs.form.validate().isValid() == false){
          ToastAndroid.show(strings.toastInputOk,ToastAndroid.SHORT);
      }else{
          var obj = {
               username : this.state.formValues.username,
               email :  this.state.formValues.email,
               password :  this.state.formValues.password,
               password2 :  this.state.formValues.confirmPassword,
               phone :  this.state.formValues.PhoneNumber
          };
        me.setState({
            editable: false,
        },...this.state);
        store.dispatch(userRegister(obj)).then((json)=>{
            debugger;
            var serverData = store.getState().loginReducer.ServerData;

            if(serverData.regist_status == true){
                window.GLOBAL.user_id = serverData.user_id;

                ToastAndroid.show(strings.toastRegisterSuccess, ToastAndroid.SHORT);
                this.setTimeout(
                    () => {
                        const { navigator } = me.props;
                        if(navigator) {
                            navigator.push({
                                name: 'Barcode',
                                component: Barcode
                            })
                        }
                        me.setState({editable: true});
                    },
                    1000
                );
            }else{
                ToastAndroid.show(strings.toastRegisterFailure, ToastAndroid.SHORT);
            }
        }).catch((error)=>{
            me.setState({
                editable: true,
            },...this.state)
            ToastAndroid.show("对不起，数据请求失败：" + error.message, ToastAndroid.SHORT);
        }).done();
      }
    },
    geographicalBtn: function () {
      var me = this;
      navigator.geolocation.getCurrentPosition(
          (initialPosition) => {
              var latitude = initialPosition.coords.latitude
              var longitude = initialPosition.coords.longitude;

              //北京市海淀区中关村大街27号1101-08室
              // longitude = 116.322987;
              // latitude = 39.983424;

              //广东省东莞市东莞市市辖区莞太路24号
              // longitude = 113.75;
              // latitude = 23.03;

              fetch('http://api.map.baidu.com/geocoder/v2/?ak=61f8bd72d68aef3a7b66537761d29d82&callback=renderReverse&location=' + latitude + ',' + longitude + '&output=json&pois=0')
              .then(response => response.text()) //返回 非 JSON 字符串
              .then(text => {
                  var processText = text.replace("renderReverse&&renderReverse(","").substr(0,text.replace("renderReverse&&renderReverse(","").length - 1);
                  var obj = JSON.parse(processText);
                  if(obj.result.addressComponent.country != "中国"){
                    ToastAndroid.show("位置信息不在中国，或不可用。", ToastAndroid.SHORT);
                  }else{
                    me.setState({
                      formValues: {
                        Address: obj.result.addressComponent.country + "、" + obj.result.addressComponent.province + "、" + obj.result.addressComponent.city + "、" + obj.result.addressComponent.district,
                        Street: obj.result.addressComponent.street + " " + obj.result.addressComponent.street_number
                      }
                    });
                  }
              }).catch((error)=>{
                //Google API，获取街道信息
                // fetch('http://maps.google.com/maps/api/geocode/json?latlng=' + latitude + ',' + longitude + '&sensor=false')
                //   .then(response => response.json())
                //   .then(text => {
                //       debugger;
                // });
              });
          },
          (error) => {
            ToastAndroid.show("GPS 不可用，请打开。", ToastAndroid.SHORT);
          }
        );
    },
    actionSelect: function(index){
      if(index == 0){
        strings.setLanguage('en');
        ServerURL = i18nIndex[1];
        this.setState({...this.state});
      }else{
        strings.setLanguage('zh');
        ServerURL = i18nIndex[0];
        this.setState({...this.state});
      }
    },
    render: function () {
        var me = this;
        //更加精细的判断
        var Person = t.struct({
          // a required string(t.maybe(t.String) : 可选字段)
          email: t.refinement(t.String, function (n) {
            var regx = /^(\w)+(\.\w+)*@(\w)+((\.\w+)+)$/;
            return regx.test(n);
          }),
          // an required string
          password: t.refinement(t.String, function (n) {
            var regx = /^[A-Za-z0-9]{6,}$/;
            return regx.test(n);
          }),
          confirmPassword: t.refinement(t.String, function (n) {
            return (n === me.state.formValues.password);
          }),
          PhoneNumber: t.refinement(t.maybe(t.String), function (n) {
            var regx = /^[0-9]*[-]*$/;
            return regx.test(n);
          }),
          username : t.String
          //Address: t.maybe(t.String),
          //Street: t.maybe(t.String)
        });

        // optional rendering options (see documentation)
        var options = {
          auto: 'placeholders', //自动填充提示 ['placeholders' | 'none' <默认>]
          fields: {
            email:{
              keyboardType: 'email-address',
              placeholder: strings.lblPlaceholderEmail,
              hasError: this.state.formError.emailErrorState,
              hasBlur: this.state.formBlur.emailBlurState,
              error: strings.lblEmailError,  //可以自定义：<i>A custom error message</i>
              onBlur: ()=>{
                this.setState({
                  formBlur: {
                    ...this.state.formBlur,
                    emailBlurState: false
                  }
                });
              },
              onFocus: () => {
                this.setState({
                  formBlur: {
                    ...this.state.formBlur,
                    emailBlurState: true
                  }
                });
              }
            },
            password:{
              maxLength: 12,
              secureTextEntry: true,
              placeholder: strings.lblPlaceholderPassword,
              hasError: this.state.formError.passwordErrorState,
              hasBlur: this.state.formBlur.passwordBlurState,
              error: strings.lblPasswordError,
              onBlur: ()=>{
                this.setState({
                  formBlur: {
                    ...this.state.formBlur,
                    passwordBlurState: false
                  }
                });
              },
              onFocus: () => {
                this.setState({
                  formBlur: {
                    ...this.state.formBlur,
                    passwordBlurState: true
                  }
                });
              }
            },
            confirmPassword:{
              maxLength: 12,
              secureTextEntry: true,
              placeholder: strings.lblPlaceholderConfirmPassword,
              hasError: this.state.formError.confirmPwdErrorState,
              hasBlur: this.state.formBlur.confirmPwdBlurState,
              error: strings.lblPasswordConfirmError,
              onBlur: ()=>{
                this.setState({
                  formBlur: {
                    ...this.state.formBlur,
                    confirmPwdBlurState: false
                  }
                });
              },
              onFocus: () => {
                this.setState({
                  formBlur: {
                    ...this.state.formBlur,
                    confirmPwdBlurState: true
                  }
                });
              }
            },
            PhoneNumber:{
              placeholder: strings.lblPhoneNumber,
              keyboardType: 'phone-pad',
              hasBlur: this.state.formBlur.phoneNumberBlurState,
              onBlur: ()=>{
                this.setState({
                  formBlur: {
                    ...this.state.formBlur,
                    phoneNumberBlurState: false
                  }
                });
              },
              onFocus: () => {
                this.setState({
                  formBlur: {
                    ...this.state.formBlur,
                    phoneNumberBlurState: true
                  }
                });
              }
            },
            // Address:{
            //   placeholder: 'Address',
            //   hasBlur: this.state.formBlur.addressBlurState,
            //   onBlur: ()=>{
            //     this.setState({
            //       formBlur: {
            //         ...this.state.formBlur,
            //         addressBlurState: false
            //       }
            //     });
            //   },
            //   onFocus: () => {
            //     this.setState({
            //       formBlur: {
            //         ...this.state.formBlur,
            //         addressBlurState: true
            //       }
            //     });
            //   }
            // },
            // Street:{
            //   placeholder: 'Street and house number',
            //   hasBlur: this.state.formBlur.streetBlurState,
            //   onBlur: ()=>{
            //     this.setState({
            //       formBlur: {
            //         ...this.state.formBlur,
            //         streetBlurState: false
            //       }
            //     });
            //   },
            //   onFocus: () => {
            //     this.setState({
            //       formBlur: {
            //         ...this.state.formBlur,
            //         streetBlurState: true
            //       }
            //     });
            //   }
            // },
            username:{
                placeholder: strings.lblUserName,
                hasError: this.state.formError.usernameErrorState,
                hasBlur: this.state.formBlur.usernameBlurState,
                onBlur: ()=>{
                  this.setState({
                        formBlur: {
                          ...this.state.formBlur,
                          usernameBlurState: false
                        }
                      });
                },
                onFocus: () => {
                    this.setState({
                          formBlur: {
                            ...this.state.formBlur,
                            usernameBlurState: true
                          }
                        });
                }
            },
          }
        };
        return(
          <View style={styles.container}>
                <ToolbarAndroid
                    actions={toolbarActions}
                    onActionSelected={this.actionSelect}
                    style={styles.viewToolbarIcon}
                    titleColor='#A1A3A5'
                    title={strings.btnRegister} />
                <View style={styles.viewContainer}>
                    <Form
                          ref="form"
                          type={Person}
                          options={options}
                          value={this.state.formValues}
                          onChange={this.onChange}
                    />
                    <View style={{flexDirection: 'column'}}>
                        <TouchableHighlight onPress={this.registerBtn} style={styles.facebookButton} underlayColor='rgb(150, 220, 161)'>
                            <View style={styles.facebookView}>
                              <Icon name="ios-compose-outline" size={24} style={[this.imageIcon,{width:24,height:24,marginTop:6,marginRight:5,color: 'white'}]} />
                              <Text style={[styles.buttonText,{fontSize:14}]}>{strings.btnRegister}</Text>
                            </View>
                        </TouchableHighlight>
                    </View>
                </View>
          </View>
        );
    }
});
module.exports = Register;
