var React = require('react-native');
var styles = require('./../styles/Barcode');
var MarkerSelection = require('./Component/MarkerSelection');
var Icon = require('react-native-vector-icons/Ionicons');
var {
    Text,
    View,
    Image,
    Alert,
    ScrollView,
    ToastAndroid,
    ToolbarAndroid,
    TouchableOpacity,
    TouchableHighlight,

    NativeModules,
    DeviceEventEmitter,
} = React;
var SetNumber = require('./Component/SetNumber');
var strings = require('./../utils/LocalizedStrings');
var returnTypes = {
    isType: false,
    items: []
};

var ProductsDetail = React.createClass({
    getInitialState: function () {
        return {}
    },
    getTypes: function(){
        if(returnTypes.isType == true){
            return returnTypes;
        }else{
            var isType = true; var index = 0;
            for (var item in this.props.ServerData.attributes) {
                if(this.refs['type_'+ index].getValue() == ""){
                    isType = false; break;
                }
                index++;
            }

            if(isType == true){
                var index = 0; var datas = [];
                for (var item in this.props.ServerData.attributes) {
                    datas.push(this.refs['type_'+ index].getValue().value);
                    index++;
                }
                returnTypes = {
                    isType: isType,
                    items: datas
                }
                return returnTypes;
            }else{
                returnTypes = {
                    isType: false,
                    items: []
                }
                return returnTypes;
            }
        }
    },
    shouldComponentUpdate: function() {
        var index = 0;
        for (var item in this.props.ServerData.attributes) {
            this.refs['type_'+ index].initiValue();
            index++;
        }
        return true;
    },
    getTotalNum: function(){
        return this.refs.setNumber.getValue();
    },
    getName: function(){
        return this.props.ServerData.name;
    },
    getProductImage: function(){
        return this.props.imageUrl.uri;
    },
    setTotalNum: function(num){
        this.refs["setNumber"].setValue(num);
    },
    weChatShare: function(){
        //分享网页
        var webpageOptions = {
              title: this.props.desc.toString().substr(0,250),
              desc: '特意分享给你',
              thumbSize: 150,
              scene: 1,
              type: 3,

              webpageUrl: ServerURL + '/shop/' + this.props.ServerData.id + '/' + this.props.ServerData.slug + '/',
              thumbImage: this.props.imageUrl.uri.toString(),
        }

        NativeModules.WeChatAndroid.registerApp("wxbc8cf90b64f6de9b",(err,registerOK) => {
            if(registerOK == true){
                NativeModules.WeChatAndroid.sendShare(webpageOptions,(status,sendOK) => {
                    if(status != 0){
                        ToastAndroid.show(sendOK.toString(),ToastAndroid.SHORT);
                    }else{
                        ToastAndroid.show(strings.openWeChatShare,ToastAndroid.SHORT);
                    }
                });
            }
        });
    },
    render: function () {
        var toolbarAction = [
          {title: 'Favorites',icon: this.props.favoritesIcon,show: 'always'}
        ];
        if(isEmptyObject(this.props.ServerData) == true){
            returnTypes = {
                isType: false,
                items: []
            }
            return(<View></View>);
        }else{
              if(this.props.ServerData.attributes != null){
                var arrays = [];
                for (var item in this.props.ServerData.attributes) {
                    var obj = {
                        name: item,
                        items: []
                    }
                    for (var i = 0; i < this.props.ServerData.attributes[item].length; i++) {
                        var objSubItem = {
                          name: this.props.ServerData.attributes[item][i][1],
                          value: this.props.ServerData.attributes[item][i][0]
                        }
                        obj.items.push(objSubItem);
                    }
                    arrays.push(obj);
                }
                returnTypes = {
                    isType: false,
                    items: []
                }
                return (
                    <View style={[styles.floatView]}>
                      <ToolbarAndroid
                          actions={toolbarAction}
                          navIcon={require('image!leftarrow1')}
                          onActionSelected={this.props.favoritesInfo}
                          onIconClicked={this.props.backBarcode}
                          style={styles.viewToolbarIcon}
                          titleColor='#A1A3A5'
                          title={this.props.ServerData.name.substr(0,28)} />
                        <ScrollView style={styles.animatedScrollView}>
                            <View style={styles.subContainer}>
                                <Image source={this.props.imageUrl} resizeMode="contain" style={styles.imgDetail} />
                            </View>
                            <View style={styles.viewShare}>
                                <TouchableOpacity onPress={this.weChatShare} style={[styles.redButton,{width: 70}]}activeOpacity={0.8}>
                                    <View style={{flexDirection: 'row',justifyContent:'center'}}>
                                        <Icon name="aperture" size={25} style={[styles.greenButtonText,{marginRight: 5}]}/>
                                        <Text style={[styles.greenButtonText,{fontSize: 14}]}>{strings.btnWeChatShare}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            {
                                  arrays.map((item,index)=>{
                                      return(<MarkerSelection ref={'type_' + index} key={index} data={item.items} title={item.name + "："} />)
                                  })
                            }
                            <View style={styles.txtView}>
                                <Text style={styles.price}>￥:{this.props.ServerData.price}</Text>
                                <SetNumber ref="setNumber" value={1}/>
                            </View>
                            <View style={styles.txtView}>
                                <Text style={styles.desc}>{this.props.desc}</Text>
                                <TouchableOpacity onPress={this.props.showWebViewDesc} activeOpacity={0.8}>
                                  <Text style={styles.lblShowMoreDetails}>{strings.lblShowMoreDetails}</Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                        <View style={styles.viewBottom}>
                            <TouchableOpacity onPress={this.props.orderBuyBtn} style={styles.redButton}  activeOpacity={0.8}>
                                <Text style={[styles.greenButtonText,{fontSize: 18}]}>{strings.btnOrder}</Text>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={this.props.btnBuyNow} style={[styles.redButton,{backgroundColor:'#1D1D1D',borderColor: '#1D1D1D'}]} activeOpacity={0.8}>
                                <Text style={[styles.greenButtonText,{fontSize: 18}]}>{strings.btnBuyNow}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                );
            }else{
              //没有类型的时候
              returnTypes = {
                  isType: true,
                  items: []
              }
              return (
                <View style={[styles.floatView]}>
                  <ToolbarAndroid
                      actions={toolbarAction}
                      navIcon={require('image!leftarrow1')}
                      onActionSelected={this.props.favoritesInfo}
                      onIconClicked={this.props.backBarcode}
                      style={styles.viewToolbarIcon}
                      titleColor='#A1A3A5'
                      title={this.props.ServerData.name.substr(0,28)} />

                    <ScrollView style={styles.animatedScrollView}>
                        <View style={styles.subContainer}>
                            <Image source={this.props.imageUrl} resizeMode="contain" style={styles.imgDetail} />
                        </View>
                        <View style={styles.viewShare}>
                            <TouchableOpacity onPress={this.weChatShare} style={[styles.redButton,{width: 70}]} activeOpacity={0.8}>
                                <View style={{flexDirection: 'row',justifyContent:'center'}}>
                                    <Icon name="aperture" size={25} style={[styles.greenButtonText,{marginRight: 5}]}/>
                                    <Text style={[styles.greenButtonText,{fontSize: 14}]}>{strings.btnWeChatShare}</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.txtView}>
                            <Text style={styles.price}>￥:{this.props.ServerData.price}</Text>
                            <SetNumber ref="setNumber" value={1}/>
                        </View>
                        <View style={styles.txtView}>
                            <Text style={styles.desc}>{this.props.desc}</Text>
                            <TouchableOpacity onPress={this.props.showWebViewDesc} activeOpacity={0.8}>
                              <Text style={styles.lblShowMoreDetails}>{strings.lblShowMoreDetails}</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                    <View style={styles.viewBottom}>
                        <TouchableOpacity onPress={this.props.orderBuyBtn} style={styles.redButton} activeOpacity={0.8}>
                            <Text style={[styles.greenButtonText,{fontSize: 16}]}>{strings.btnOrder}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.props.btnBuyNow} style={[styles.redButton,{backgroundColor:'#1D1D1D',borderColor: '#1D1D1D'}]} activeOpacity={0.8}>
                            <Text style={[styles.greenButtonText,{fontSize: 16}]}>{strings.btnBuyNow}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
              );
            }
        }

    }
});
module.exports = ProductsDetail;
