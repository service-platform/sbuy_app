'use strict';
var React = require('react-native');
var styles = require('./../styles/DrawerLayout');
var Share = require('react-native-share');
var ImagePicker = require('@remobile/react-native-image-picker');
var Camera = require('@remobile/react-native-camera');
import Menu from 'react-native-simplemenu-android';
var PushNotification = require('react-native-push-notification');
var httpCache = require('react-native-http-cache');

var {
    View,
    Text,
    Image,
    Alert,
    TextInput,
    StyleSheet,
    BackAndroid,
    Animated,
    PixelRatio,
    ToastAndroid,
    TouchableHighlight,//按钮
} = React;
var strings = require('./../utils/LocalizedStrings');
var splitLineHeight = 1 / PixelRatio.get();

var DrawerLayout = React.createClass({
    getInitialState: function() {
        return {
            bounceValue: new Animated.Value(0),
            image: require('image!user_logo')
        };
    },
    setHeader:function () {
      var me = this;
      Menu.show({
        options: [strings.lblMenuLocationImg, strings.lblOpenCamera]
      }, function(i) {
          if (i == 0) {
              var options = {maximumImagesCount: 1, quality: 90};
              ImagePicker.getPictures(options, function(results) {
                  me.setState({
                      image: {uri: results[0], isStatic: true}
                  });
              }, function (error) {
                  Alert.alert('Error', error,[{text: 'Cancel'}]);
              });
          } else if (i == 1) {
              var options = {
                  quality: 80,
                  allowEdit: true,
                  destinationType: Camera.DestinationType.DATA_URL,
              };
              Camera.getPicture(options, (imageData) => {
                  me.setState({
                    image: {uri:'data:image/jpeg;base64,' + imageData}
                  });
              });
          }
      });
    },
    mySBuy: function(){

    },
    clearCache: function(){
      httpCache.getSize()
      .then((value) =>{
          Alert.alert(
            strings.lblClearCache,
            strings.formatString(strings.alertTipClear,value),
            [
              {text: strings.alertCancel},
              {text: strings.alertClear, onPress: () => {
                httpCache.clear()
                .then((value) =>{
                    ToastAndroid.show(strings.toastClearSuccess, ToastAndroid.SHORT);
                }).catch((error) => {
                    ToastAndroid.show(strings.toastClearFailure, ToastAndroid.SHORT);
                }).done();
              }},
            ]
          );
      }).catch((error) => {debugger;}).done();
    },
    shareAPP: function(){
      Share.open({
         share_text: "这是一个分享链接",
         share_URL: "http://www.s-buy.com",
         title: strings.alertShareTitle
       },function(e) {
         console.log(e);
       });
    },
    exitSBuy:function(){
        var me = this;
        Alert.alert(
          strings.lblExit,
          strings.alertTipExit,
          [
            {text: strings.alertCancel},
            {text: strings.lblExit, onPress: () => {
                JSON.backIndex = 1;
                me.props.backLogin();
            }},
          ]
        );
    },
    PushNotif: function(){
      PushNotification.localNotification({
          /* Android Only Properties */
          id: 1, // (optional) default: Autogenerated Unique ID
          title: "Congratulations", // (optional)
          ticker: "My Notification Ticker", // (optional)
          largeIcon: "ic_launcher", // (optional) default: "ic_launcher"
          smallIcon: "ic_notification", // (optional) default: "ic_notification" with fallback for "ic_launcher"

          /* iOS and Android properties */
          message: "Congratulations, winning the lottery!" // (required)
      });
    },
    render: function () {
        return (
            <View style={styles.container}>
                <View style={{height: 30, justifyContent: 'center', marginTop: 10}}>
                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                        <Image
                            source={require('image!search')}
                            style={{width: 20, height: 20, marginLeft: 20}} />

                        <TextInput
                            style={{borderWidth: 0,width:240,color:'rgb(211,211,212)',backgroundColor: 'transparent'}}
                            placeholderTextColor="rgb(211,211,212)"
                            onChangeText={(text) => this.setState({barCode: text})}
                            placeholder={strings.lblSearch}/>
                    </View>
                </View>
                <View style={[styles.rowSplitLine,{marginBottom:10}]}></View>
                <View style={{height: 110, justifyContent: 'center'}}>
                    <View style={{flex: 1, flexDirection: 'column', alignItems: 'center'}}>
                            <TouchableHighlight onPress={this.setHeader} style={{borderRadius: 40,marginLeft:10}} underlayColor='#E2E2E244'>
                                <Image
                                    source={this.state.image}
                                    style={{width: 80, height: 80,borderRadius:40,borderWidth:2,borderColor:'#D2D2D2'}} />
                            </TouchableHighlight>
                            <Text style={{fontSize: 16, color: 'rgb(211,211,212)', marginLeft: 10}}>
                                Richard The King
                            </Text>
                    </View>
                </View>
                <View style={styles.rowSplitLine}></View>

                <View style={{height: 55, justifyContent: 'center'}}>
                    <TouchableHighlight onPress={this.mySBuy} style={{marginLeft:20,height:55,flex: 1,marginRight: 20,justifyContent: 'center'}} underlayColor='#E2E2E244'>
                        <View style={{flexDirection:'row'}}>
                              <Image
                                  source={require('image!setting')}
                                  style={{width: 20, height: 20}} />

                              <Text style={{fontSize: 12, color: 'rgb(211,211,212)', marginLeft: 10}}>
                                  {strings.lblMySBuy}
                              </Text>
                        </View>
                    </TouchableHighlight>
                </View>
                <View style={styles.rowSplitLine}></View>

                <View style={{height: 55, justifyContent: 'center'}}>
                    <TouchableHighlight onPress={this.clearCache} style={{marginLeft:20,height:55,flex: 1,marginRight: 20,justifyContent: 'center'}} underlayColor='#E2E2E244'>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Image
                                source={require('image!setting')}
                                style={{width: 20, height: 20}} />

                            <Text style={{fontSize: 12, color: 'rgb(211,211,212)', marginLeft: 10}}>
                                {strings.lblClearCache}
                            </Text>
                        </View>
                    </TouchableHighlight>
                </View>
                <View style={styles.rowSplitLine}></View>

                <View style={{height: 55, justifyContent: 'center'}}>
                    <TouchableHighlight onPress={this.shareAPP} style={{marginLeft:20,height:55,flex: 1,marginRight: 20,justifyContent: 'center'}} underlayColor='#E2E2E244'>
                        <View style={{flexDirection:'row',alignItems: 'center'}}>
                            <Image
                                source={require('image!setting')}
                                style={{width: 20, height: 20}} />

                            <Text style={{fontSize: 12, color: 'rgb(211,211,212)', marginLeft: 10}}>
                                {strings.lblShareAPP}
                            </Text>
                        </View>
                    </TouchableHighlight>
                </View>
                <View style={styles.rowSplitLine}></View>

                <View style={{height: 50, justifyContent: 'center', marginTop: 5}}>
                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                        <Image
                            source={require('image!setting')}
                            style={{width: 20, height: 20, marginLeft: 20}} />

                        <Text style={{fontSize: 12, color: 'rgb(211,211,212)', marginLeft: 10}}>
                            {strings.lblAboutAPP}
                        </Text>
                    </View>
                </View>
                <View style={styles.rowSplitLine}></View>
                <View style={{height: 55, justifyContent: 'center'}}>
                    <TouchableHighlight onPress={this.PushNotif} style={{marginLeft:20,height:55,flex: 1,marginRight: 20,justifyContent: 'center'}} underlayColor='#E2E2E244'>
                        <View style={{flexDirection:'row',alignItems: 'center'}}>
                            <Image
                                source={require('image!setting')}
                                style={{width: 20, height: 20}} />

                            <Text style={{fontSize: 12, color: 'rgb(211,211,212)', marginLeft: 10}}>
                                {strings.lblPushNotifications}
                            </Text>
                        </View>
                    </TouchableHighlight>
                </View>
                <View style={styles.rowSplitLine}></View>
                <View style={{height: 55, justifyContent: 'center'}}>
                    <TouchableHighlight onPress={this.exitSBuy} style={{marginLeft:20,height:55,flex: 1,marginRight: 20,justifyContent: 'center'}} underlayColor='#E2E2E244'>
                        <View style={{flexDirection:'row',alignItems: 'center'}}>
                            <Image
                                source={require('image!setting')}
                                style={{width: 20, height: 20}} />

                            <Text style={{fontSize: 12, color: 'rgb(211,211,212)', marginLeft: 10}}>
                                {strings.lblExit}
                            </Text>
                        </View>
                    </TouchableHighlight>
                </View>


            </View>
        );
    },

    componentDidMount() {
        this.state.bounceValue.setValue(0.5);     // Start large
        Animated.decay(                          // Base: spring, decay, timing
            this.state.bounceValue,                 // Animate `bounceValue`
            {
                toValue: 1.2,                         // Animate to smaller size
                duration: 3000,                          // Bouncier spring
            }
        ).start();                                // Start the animation
    }

});

var styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgb(39,44,48)',
    },

    rowSplitLine: {
        height: 1,
        backgroundColor: 'gray',
        marginLeft: 20,
        marginRight: 20,
    },
});

module.exports = DrawerLayout;
