var React = require('react-native');
var styles = require('./../styles/BarcodeScanner');
var BarcodeScanner = require('react-native-barcodescanner');
var Icon = require('react-native-vector-icons/Ionicons');
var {
    Text,
    View,
    Image,
    Alert,
    TouchableOpacity,
    ToastAndroid,
    Dimensions,
    TextInput,
    NativeModules
} = React;
var ImagePicker = require('@remobile/react-native-image-picker');
var strings = require('./../utils/LocalizedStrings');
let { width, height } = Dimensions.get('window');
var intervalTimer = null;
var Barcode = React.createClass({
    getInitialState: function () {
        return {
            torchMode: 'off',
            cameraType: 'back',
            scanningStripTop: 190,  //190 - 410
        };
    },
    barcodeReceived(e) {
        //e.type、e.data
        ToastAndroid.show(strings.toastOpenURL, ToastAndroid.SHORT);
        window.GLOBAL.isOpenCameraView = false;

        this.props.openProductsDetail(e.data + "api/");
        const { navigator } = this.props;
        if(navigator) {
            navigator.pop();
        }
    },
    exitCamera: function(){
      clearInterval(intervalTimer);
      const { navigator } = this.props;
      if(navigator) {
          navigator.pop();
      }
      window.GLOBAL.isOpenCameraView = false;
      this.props.setTabViewPage();
    },
    componentWillMount: function(){
      var me = this;
      intervalTimer = setInterval(function(){
        if(me.state.scanningStripTop <= 410){
          me.setState({
            scanningStripTop: (me.state.scanningStripTop + 3)
          });
        }else{
          me.setState({
            scanningStripTop: 190
          });
        }
      },5);
    },
    QRFromPhoto: function(){
        var options = {maximumImagesCount: 1, quality: 100}; var me = this;
        ImagePicker.getPictures(options, function(results) {
            //decodeURI 解决中文编码的问题，路径需去掉 file://
            NativeModules.QRFormPhotoReact.QRScanningImage(decodeURI(results[0].replace("file://","")),(code,strInfo)=>{
                if(code == "0"){
                    if(strInfo.includes(i18nIndex[0]) == false){
                        ToastAndroid.show(strings.toastQRNothing, ToastAndroid.SHORT); return;
                    }

                    ToastAndroid.show(strings.toastOpenURL, ToastAndroid.SHORT);
                    window.GLOBAL.isOpenCameraView = false;

                    me.props.openProductsDetail(strInfo);
                    const { navigator } = me.props;
                    if(navigator) {
                        navigator.pop();
                    }
                    me.props.setTabViewPage();
                }else{
                    ToastAndroid.show(strInfo, ToastAndroid.SHORT);
                }
            });
        }, function (error) {
            // Alert.alert('Error', error,[{text: 'Cancel'}]);
        });
    },
    render: function () {
        return (
          <View style={styles.container}>
                <BarcodeScanner
                    onBarCodeRead={this.barcodeReceived}
                    style={{ flex: 1}}
                    torchMode={this.state.torchMode}
                    cameraType={this.state.cameraType}
                    viewFinderDrawLaser={true}
                />
                <View style={styles.viewToolbar}>
                    <TouchableOpacity onPress={this.exitCamera} style={styles.btnBack} activeOpacity={0.5}>
                        <Icon name="chevron-left" size={30} style={styles.viewIcon}/>
                    </TouchableOpacity>
                    <Text style={styles.txtToolbar}>{strings.toolbarTxtQR}</Text>
                    <TouchableOpacity onPress={this.QRFromPhoto} style={styles.btnFromPhoto} activeOpacity={0.5}>
                        <Icon name="image" size={30} style={styles.viewIcon}/>
                    </TouchableOpacity>
                </View>
                <Image source={require('image!scanningstrip')} resizeMode="stretch" style={[styles.imgScanningStrip,{top: this.state.scanningStripTop}]} />
          </View>
        )
    }
});
module.exports = Barcode;
