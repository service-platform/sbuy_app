﻿import React from 'react';
import Menu from 'react-native-simplemenu-android';
var ImagePicker = require('@remobile/react-native-image-picker');
var Camera = require('@remobile/react-native-camera');
import {
    Alert,
    Image,
    StyleSheet,
    Text,
    View,
    ScrollView,
    Dimensions,
    Component,
    TouchableOpacity,
    TouchableHighlight,
} from 'react-native';

var {height, width} = Dimensions.get('window');
var Icon = require('react-native-vector-icons/Ionicons');
var strings = require('./../utils/LocalizedStrings');

var HistoryList =  React.createClass({
    getInitialState: function() {
        return {
            image: require('image!b')
        };
    },
    onClick: function(){
        // alert(1);
    },
    setLanguage: function(){
      var me = this;
      Menu.show({
        options: ['English', '简体中文']
      }, function(i) {
          me.props.setLanguage(i);
      });
    },
    setHeader:function () {
      var me = this;
      Menu.show({
        options: [strings.lblMenuLocationImg, strings.lblOpenCamera]
      }, function(i) {
          if (i == 0) {
              var options = {maximumImagesCount: 1, quality: 90};
              ImagePicker.getPictures(options, function(results) {
                  me.setState({
                      image: {uri: results[0], isStatic: true}
                  });
              }, function (error) {
                  Alert.alert('Error', error,[{text: 'Cancel'}]);
              });
          } else if (i == 1) {
              var options = {
                  quality: 80,
                  allowEdit: true,
                  destinationType: Camera.DestinationType.DATA_URL,
              };
              Camera.getPicture(options, (imageData) => {
                  me.setState({
                    image: {uri:'data:image/jpeg;base64,' + imageData}
                  });
              });
          }
      });
    },
    onShowHistoryList: function(){
        this.props.onShowHistoryList();
    },
    onAddressManager: function(){
        this.props.onAddressManager();
    },
    onExit: function(){
      var me = this;
      Alert.alert(
        strings.lblExit,
        strings.alertTipExit,
        [
          {text: strings.alertCancel},
          {text: strings.lblExit, onPress: () => {
              JSON.backIndex = 1;
              me.props.backLogin();
          }},
        ]
      );
    },
    render: function(){
        return (
            <ScrollView style={styles.container}>
                <View>
                  <Image style={styles.headPicture} source={require('image!bg')} />
                  <TouchableOpacity onPress={this.setHeader} activeOpacity={0.7} style={styles.btnUserLogo}>
                      <Image source={this.state.image} style={styles.userLogo}/>
                  </TouchableOpacity>
                </View>

                <TouchableOpacity onPress={this.onShowHistoryList} activeOpacity={0.5}>
                    <View style={styles.listBox}>
                        <Image style={styles.listPicture} source={require('image!dingdan')} />
                        <Text>{strings.lblAllOrders}</Text>
                        <Icon name="chevron-right" size={25} style={styles.rightPicture}/>
                    </View>
                </TouchableOpacity>



                <TouchableOpacity style={styles.itemLayer} onPress={this.onClick} activeOpacity={0.5}>
                  <View style={styles.collectBox}>
                      <Image style={[styles.pictureCommon,styles.collectPicture]} source={require('image!love')} />
                      <Text>{strings.lblMyFavorites}</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity style={styles.itemLayer} onPress={this.onClick} activeOpacity={0.5}>
                  <View style={styles.collectBox}>
                      <Image style={[styles.pictureCommon,styles.collectPicture]} source={require('image!ticket')} />
                      <Text>{strings.lblMyCoupons}</Text>
                      <Text style={styles.ticketCount}>4张</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={this.onClick} activeOpacity={0.5}>
                  <View style={styles.listBox}>
                          <Image style={styles.listPicture} source={require('image!integration')} />
                          <Text>{strings.lblMyPoints}</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity style={styles.itemLayer} onPress={this.setLanguage} activeOpacity={0.5}>
                  <View style={styles.collectBox}>
                      <Icon name="ios-world" size={32} style={styles.listPicture}/>
                      <Text>{strings.lblLanguageSettings}</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity style={styles.itemLayer} onPress={this.onAddressManager} activeOpacity={0.5}>
                  <View style={styles.collectBox}>
                      <Image style={[styles.pictureCommon,styles.collectPicture]} source={require('image!address')} />
                      <Text>{strings.lblAddressManager}</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={this.onClick} activeOpacity={0.5}>
                  <View style={styles.listBox}>
                      <Image style={styles.listPicture} source={require('image!servive')} />
                      <Text>{strings.lblFeedback}</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity style={styles.itemLayer} onPress={this.onExit} activeOpacity={0.5}>
                  <View style={styles.collectBox}>
                      <Image style={styles.listPicture} source={require('image!exit')} />
                      <Text>{strings.lblExitSystem}</Text>
                  </View>
                </TouchableOpacity>
            </ScrollView>
        );
    }
});
module.exports = HistoryList;

var styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        backgroundColor: '#F9F9F9',
    },
    btnUserLogo:{
        width:80,
        height:80,
        borderRadius: 40,
        position:'absolute',
        top:45,
        left:35,
    },
    userLogo:{
        width:80,
        height:80,
        borderRadius: 40,
        borderWidth:2,
        borderColor:'#D2D2D2',
    },
    headPicture:{
        width:width,
        height:160
    },
    rightPicture:{
        color: '#808080',
        position: 'absolute',
        right: 15,
        marginTop: 13
    },
    listBox:{
        width:width,
        height:50,
        alignItems:'center',
        flexDirection: 'row',
        paddingLeft:20,
        backgroundColor:'#fff',
        borderColor:'#e8e8e8',
        borderBottomWidth:1
    },
    listPicture:
    {
        width:30,
        height:30,
        marginRight:10
    },
    functionBox:{
        width:width,
        height:100,
        alignItems:'center',
        flexDirection: 'row',
        backgroundColor:'#fff',
        borderColor:'#e8e8e8',
        borderBottomWidth:1,
        justifyContent:'space-around'
    },
    payPicture:{
        marginLeft:5,
        marginBottom:12
    },
    pictureCommon:{
        width:30,
        height:30
    },
    TextCommon:{

    },
    payBox:{
        flexDirection: 'column',
        justifyContent:'center'
    },
    itemLayer:{
        marginTop:10
    },
    collectBox:{
        width:width,
        height:50,
        alignItems:'center',
        flexDirection: 'row',
        paddingLeft:20,
        backgroundColor:'#fff',
        borderColor:'#e8e8e8',
        borderBottomWidth:1,
        borderTopWidth:1,
    },
    collectPicture:{
        marginRight:10
    },
    ticketCount:{
        position:'absolute',
        right:15,
        top:15
    },
    button:{
        marginLeft:-10
    }

});
