var React = require('react-native');
var {
    View,
    Text,
    PropTypes,
    TouchableHighlight,
} = React;
var styles = require('./../../styles/Barcode');
var Icon = require('react-native-vector-icons/Ionicons');

var SetNumber = React.createClass({
    propTypes: {
        value : PropTypes.number
    },
    getInitialState: function () {
        return {
            selectNumber: 1
        }
    },
    getDefaultProps: function() {
        return {
            value: 1,
        };
    },
    componentDidMount: function() {
        if(this.props.value != undefined){
            this.setState({
                selectNumber: this.props.value
            });
        }
    },
    getValue: function(){
        return this.state.selectNumber;
    },
    setValue: function(totalNum){
        this.setState({
          selectNumber: totalNum
        });
    },
    additionBtn:function(){
        this.setState({
          selectNumber: (this.state.selectNumber + 1)
        });
    },
    subtractionBtn:function(){
        if(this.state.selectNumber > 1){
            this.setState({
                selectNumber: (this.state.selectNumber - 1)
            });
        }
    },
    render: function() {
        return (
          <View style={styles.viewQuantity}>
            <View style={styles.viewSubQuantity}>
                <TouchableHighlight onPress={this.subtractionBtn} underlayColor="#E0E0E0" style={styles.subtractionBtn}>
                    <Icon name="minus" size={30} style={styles.iconQuantity}/>
                </TouchableHighlight>
                <View style={styles.viewQuantityNum}>
                  <Text style={styles.txtQuantity}>{this.state.selectNumber}</Text>
                </View>
                <TouchableHighlight onPress={this.additionBtn} underlayColor="#E0E0E0" style={styles.subtractionBtn}>
                    <Icon name="plus" size={30} style={styles.iconQuantity}/>
                </TouchableHighlight>
            </View>
          </View>
        )
    }
});

module.exports = SetNumber;
