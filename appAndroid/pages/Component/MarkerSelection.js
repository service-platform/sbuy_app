'use strict';
var React = require('react-native');
var {
    View,
    Text,
    Dimensions,
    TouchableHighlight
} = React;
let { width, height } = Dimensions.get('window');

var MarkerSelection = React.createClass({
    getInitialState: function () {
        return {
          selectValue: ''
        }
    },
    getDefaultProps: function() {
       return {
           data: [],
           title: '',
       };
    },
    propTypes: {
       data: React.PropTypes.array.isRequired,
       title: React.PropTypes.string
    },
    initiValue: function(){
        this.setState({selectValue: ''});
    },
    componentDidMount: function() {
      this.setState({selectValue: ''});
      if(this.props.defaultValues != undefined && this.props.defaultValues != ""){
          var tempValue = "";
          for(var i=0;i < this.props.data.length ;i++){
              if(this.props.defaultValues == this.props.data[i].value){
                  tempValue = this.props.data[i];
                  break;
              }
          }
          if(tempValue != ""){
            this.setValue(tempValue,false);
          }
      }
    },
    getValue: function(){
        return this.state.selectValue;
    },
    setValue: function(data,isMark){
      this.setState({selectValue: data});
      if(this.props.onSelect){
          if(isMark == true){
              this.props.onSelect(data);
          }
      }
    },
    render: function () {
        var length = this.props.data.length;
        var me = this;
        if(this.props.title != ''){
            return (
              <View>
                  <Text style={{marginLeft:10,marginBottom:10}}>{this.props.title}</Text>
                  <View style={{flexDirection: 'row',marginLeft: 10,width: width, flexWrap: 'wrap'}}>
                      {
                          this.props.data.map((val,index) => {
                            if((index + 1) < length){
                              if(me.state.selectValue == val){
                                return(
                                  <TouchableHighlight key={index} onPress={()=>{this.setValue(val,true)}} underlayColor="#E0E0E0" style={{backgroundColor: '#baa071',borderRadius: 7,paddingTop:3,paddingBottom:3,marginRight: 10,marginBottom: 10}}>
                                    <Text style={{marginLeft:10,marginRight:10,color: 'white'}}>{val.name}</Text>
                                  </TouchableHighlight>);
                              }else{
                                return(
                                  <TouchableHighlight key={index} onPress={()=>{this.setValue(val,true)}} underlayColor="#E0E0E0" style={{backgroundColor: '#F5F5F5',borderRadius: 7,paddingTop:3,paddingBottom:3,marginRight: 10,marginBottom: 10}}>
                                    <Text style={{marginLeft:10,marginRight:10}}>{val.name}</Text>
                                  </TouchableHighlight>);
                              }
                            }else{
                              if(me.state.selectValue == val){
                                return(
                                  <TouchableHighlight key={index} onPress={()=>{this.setValue(val,true)}} underlayColor="#E0E0E0" style={{backgroundColor: '#baa071',borderRadius: 7,paddingTop:3,paddingBottom:3,marginBottom: 10}}>
                                    <Text style={{marginLeft:10,marginRight:10,color: 'white'}}>{val.name}</Text>
                                  </TouchableHighlight>);
                              }else{
                                return(
                                  <TouchableHighlight key={index} onPress={()=>{this.setValue(val,true)}} underlayColor="#E0E0E0" style={{backgroundColor: '#F5F5F5',borderRadius: 7,paddingTop:3,paddingBottom:3,marginBottom: 10}}>
                                    <Text style={{marginLeft:10,marginRight:10}}>{val.name}</Text>
                                  </TouchableHighlight>);
                              }
                            }
                          })
                      }
                  </View>
              </View>
            );
        }else{
            return (
              <View>
                  <View style={{flexDirection: 'row',marginLeft: 10,width: width, flexWrap: 'wrap'}}>
                      {
                          this.props.data.map((val,index) => {
                            if((index + 1) < length){
                              if(me.state.selectValue == val){
                                  return(
                                    <TouchableHighlight key={index} onPress={()=>{this.setValue(val,true)}} underlayColor="#E0E0E0" style={{backgroundColor: '#baa071',borderRadius: 7,paddingTop:3,paddingBottom:3,marginRight: 10,marginBottom: 10}}>
                                      <Text style={{marginLeft:10,marginRight:10,color: 'white'}}>{val.name}</Text>
                                    </TouchableHighlight>);
                              }else{
                                  return(
                                    <TouchableHighlight key={index} onPress={()=>{this.setValue(val,true)}} underlayColor="#E0E0E0" style={{backgroundColor: '#F5F5F5',borderRadius: 7,paddingTop:3,paddingBottom:3,marginRight: 10,marginBottom: 10}}>
                                      <Text style={{marginLeft:10,marginRight:10}}>{val.name}</Text>
                                    </TouchableHighlight>);
                              }
                            }else{
                              if(me.state.selectValue == val){
                                  return(
                                    <TouchableHighlight key={index} onPress={()=>{this.setValue(val,true)}} underlayColor="#E0E0E0" style={{backgroundColor: '#baa071',borderRadius: 7,paddingTop:3,paddingBottom:3,marginBottom: 10}}>
                                      <Text style={{marginLeft:10,marginRight:10,color: 'white'}}>{val.name}</Text>
                                    </TouchableHighlight>);
                              }else{
                                  return(
                                    <TouchableHighlight key={index} onPress={()=>{this.setValue(val,true)}} underlayColor="#E0E0E0" style={{backgroundColor: '#F5F5F5',borderRadius: 7,paddingTop:3,paddingBottom:3,marginBottom: 10}}>
                                      <Text style={{marginLeft:10,marginRight:10}}>{val.name}</Text>
                                    </TouchableHighlight>);
                              }
                            }
                          })
                      }
                  </View>
              </View>
            );
        }
    }
});
module.exports = MarkerSelection;
