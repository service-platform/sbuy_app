'use strict';

var React = require('react-native');
var Icon = require('react-native-vector-icons/Ionicons');
var styles = require('./../../styles/Component/FacebookTabBar');

var {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Animated,
} = React;

var FacebookTabBar = React.createClass({
  selectedTabIcons: [],
  unselectedTabIcons: [],

  propTypes: {
    goToPage: React.PropTypes.func,
    activeTab: React.PropTypes.number,
    tabs: React.PropTypes.array
  },

  renderTabOption(name, page) {
    var isTabActive = this.props.activeTab === page;
    var activeTextColor = this.props.activeTextColor || "#FFFFFF";
    var inactiveTextColor = this.props.inactiveTextColor || "#A7B1C2";

    //截断字符串（title：标题、iconName：图标名）
    var title = name.split('|')[0]; var iconName = name.split('|')[1];

    return (
      <TouchableOpacity key={iconName} onPress={() => this.props.goToPage(page)} style={styles.tab}>
        <View style={{flexDirection:'column'}}>
            <Icon name={iconName} size={30} style={[styles.icon,{color: isTabActive ? activeTextColor : inactiveTextColor}]}/>
            <Text style={[styles.title,{color: isTabActive ? activeTextColor : inactiveTextColor}]}>{title}</Text>
        </View>
      </TouchableOpacity>
    );
  },

  componentDidMount() {
    this.setAnimationValue({value: this.props.activeTab});
    this._listener = this.props.scrollValue.addListener(this.setAnimationValue);
  },

  setAnimationValue({value}) {
    var currentPage = this.props.activeTab;

    this.unselectedTabIcons.forEach((icon, i) => {
      var iconRef = icon;

      if (!icon.setNativeProps && icon !== null) {
        iconRef = icon.refs.icon_image
      }

      if (value - i >= 0 && value - i <= 1) {
        iconRef.setNativeProps({opacity: value - i});
      }
      if (i - value >= 0 &&  i - value <= 1) {
        iconRef.setNativeProps({opacity: i - value});
      }
    });
  },

  render() {
    var containerWidth = this.props.containerWidth;
    var numberOfTabs = this.props.tabs.length;
    var tabUnderlineStyle = {
      width: containerWidth / numberOfTabs,
      height: 3,
      backgroundColor: '#19AA8D',
    };

    var left = this.props.scrollValue.interpolate({
      inputRange: [0, 1], outputRange: [0, containerWidth / numberOfTabs]
    });

    return (
      <View>
        <Animated.View style={[tabUnderlineStyle, {left}]} />
        <View style={styles.tabs}>
          {this.props.tabs.map((tab, i) => this.renderTabOption(tab, i))}
        </View>
      </View>
    );
  },
});

module.exports = FacebookTabBar;
