var React = require('react-native');
var styles = require('./../styles/OrderConfirm');
var AddressList = require('./AddressList');
var SelectInvoice = require('./SelectInvoice');

var {
    View,
    Text,
    Image,
    Alert,
    Picker,
    NetInfo,
    TextInput,
    Dimensions,
    ScrollView,
    AsyncStorage,
    ToastAndroid,
    ToolbarAndroid,
    NativeModules,
    TouchableHighlight,
} = React;
let { width, height } = Dimensions.get('window');
var Icon = require('react-native-vector-icons/Ionicons');

import store from './../redux/store';
import { postsOrderInfo,postsBuyNow,getPaymentInfo,postPlaymentResult,postsGoodsUrl,getGetAddressList } from './../redux/actions/productsAction';
var strings = require('./../utils/LocalizedStrings');

var OrderConfirm = React.createClass({
    getInitialState: function () {
      return {
        total: 0,
        grandTotal: '0',
        txtAddress: "",
        deliveryType: 1,
        paymentType: 1,
        paymentString: 'WeChat',
        deliveryString: 'express',
        orderId: "",
        arrayList: [],
        addressId: 0,
        receiver: '',
        delivery_address: '',
        invoiceStatus: strings.lblNoInvoice,
        invoiceName: ''
      }
    },
    getDefaultProps: function() {
        return {
            grandTotal: '0',
        };
    },
    propTypes: {
        grandTotal: React.PropTypes.string.isRequired,
    },
    refreshAddress: function(mark,objData){
      if(mark == true){
          this.setState({
            addressId: 0,
            receiver: '',
            delivery_address: '',
          });
          store.dispatch(getGetAddressList(window.GLOBAL.user_id)).then((json) =>{
              for (var i = 0; i < json.ServerData.length; i++) {
                  if (json.ServerData[i].default == 1){
                    this.setState({
                        addressId: json.ServerData[i].id,
                        receiver: json.ServerData[i].receiver,
                        delivery_address: json.ServerData[i].delivery_address
                    });
                    break;
                  }
              }
          }).catch((error)=>{
              ToastAndroid.show("对不起，数据请求失败：" + error.message, ToastAndroid.SHORT);
          }).done();
      }else{
          this.setState({
            addressId: objData.addressId,
            receiver: objData.receiver,
            delivery_address: objData.delivery_address,
          });
      }
    },
    componentWillMount:function() {
      this.setState({grandTotal: this.props.grandTotal});
      //获取总数量
      this.setState({
          total: this.props.totalNum
      });
      this.refreshAddress(true);
      if(this.props.isBuyNow == true){
        var arrayList = [];
        arrayList.push(this.props.pIds);
        this.setState({
            arrayList: arrayList
        });
      }else if(this.props.isBuyNow == false){
        var me = this;
        store.dispatch(postsGoodsUrl(ServerURL + "/cart/get-cart-api/" + window.GLOBAL.user_id)).then((json) =>{
            if(isEmptyObject(json.ServerData) == false){
                var arrayList = []; var arrays = json.ServerData;

                for(var item in arrays){
                    for (var i = 0; i < arrays[item].length; i++) {
                        for (var j = 0; j < me.props.pIds.length; j++) {
                            if(me.props.pIds[j] === arrays[item][i].id){
                                arrayList.push(arrays[item][i]); break;
                            }
                        }
                    }
                }

                me.setState({
                    arrayList: arrayList
                });
            }
        }).catch((error)=>{
            ToastAndroid.show("对不起，数据请求失败：" + error.message, ToastAndroid.SHORT);
        }).done();
      }

      //获取街道信息
      return;
      navigator.geolocation.getCurrentPosition(
          (initialPosition) => {
              var latitude = initialPosition.coords.latitude
              var longitude = initialPosition.coords.longitude;

              //北京市海淀区中关村大街27号1101-08室
              // longitude = 116.322987;
              // latitude = 39.983424;

              fetch('http://api.map.baidu.com/geocoder/v2/?ak=61f8bd72d68aef3a7b66537761d29d82&callback=renderReverse&location=' + latitude + ',' + longitude + '&output=json&pois=0')
              .then(response => response.text()) //返回 非 JSON 字符串
              .then(text => {
                  var processText = text.replace("renderReverse&&renderReverse(","").substr(0,text.replace("renderReverse&&renderReverse(","").length - 1);
                  var obj = JSON.parse(processText);
                  if(obj.result.addressComponent.country != "中国"){
                    ToastAndroid.show("位置信息不在中国，或不可用。", ToastAndroid.SHORT);
                  }else{
                    me.setState({
                        txtAddress: obj.result.addressComponent.country + "、" + obj.result.addressComponent.province + "、" + obj.result.addressComponent.city + "、" + obj.result.addressComponent.district + "、" + obj.result.addressComponent.street + " " + obj.result.addressComponent.street_number
                    });
                  }
              })
              .catch((e)=>{});
          },
          (error) => {
            ToastAndroid.show("GPS 不可用，请打开。", ToastAndroid.SHORT);
          }
        );
    },
    paymentCallback: function(result){
        console.log(result);

        var str = result.substr(0,result.indexOf(";memo="));
        var strCode = parseInt(str.replace("resultStatus={","").replace("}",""));
        if(strCode == 9000){
            var strResult = result.split(';')[2];  //result={....
            strResult = result.split(';')[2].replace("result={","").replace("}","");  //partner="....

            if(this.state.orderId == ""){
                var objData = {
                    uid: window.GLOBAL.user_id,
                    pid: this.props.obj.state.ServerData.id,
                    quantity: this.props.totalNum,
                    paid: 1
                };
                store.dispatch(postsBuyNow(objData)).then((json) =>{
                    ToastAndroid.show(strings.lblSuccessfulpayment, ToastAndroid.SHORT);
                    this.backPage();
                }).catch((error)=>{
                    ToastAndroid.show("对不起，数据请求失败：" + error.message, ToastAndroid.SHORT);
                }).done();
            }else{
                var strResult = {
                  id: this.state.orderId,
                  paid: 1
                };
                store.dispatch(postPlaymentResult(ServerURL + '/orders/order-paid-api/',strResult)).then((json) =>{
                    if(json.ServerData.change_status == true){
                        ToastAndroid.show(strings.lblSuccessfulpayment, ToastAndroid.SHORT);
                        this.backPage();
                    }else{
                        ToastAndroid.show(strings.lblSystemBusy, ToastAndroid.SHORT);
                    }
                }).catch((error)=>{
                    ToastAndroid.show("对不起，数据请求失败：" + error.message, ToastAndroid.SHORT);
                }).done();
            }
        }else if(strCode == 6002){
            if(this.state.orderId == ""){
              var objData = {
                  uid: window.GLOBAL.user_id,
                  pid: this.props.obj.state.ServerData.id,
                  quantity: this.props.totalNum,
                  paid: 0
              };
              store.dispatch(postsBuyNow(objData)).then((json) =>{
                  if(json.ServerData.length >= 1){
                      this.setState({
                        orderId: json.ServerData[0].id
                      })
                  }
                  ToastAndroid.show(strings.lblNetworkError, ToastAndroid.SHORT);
              }).catch((error)=>{
                  ToastAndroid.show("对不起，数据请求失败：" + error.message, ToastAndroid.SHORT);
              }).done();
            }
        }else if(strCode == 6001){
            ToastAndroid.show(strings.lblUserCanceled, ToastAndroid.SHORT);
        }else{
            var strInfo = result.split(";")[1].replace("memo={","").replace("}","");

            if(this.state.orderId == ""){
              var objData = {
                  uid: window.GLOBAL.user_id,
                  pid: this.props.obj.state.ServerData.id,
                  quantity: this.props.totalNum,
                  paid: 0
              };
              store.dispatch(postsBuyNow(objData)).then((json) =>{
                  if(json.ServerData.length >= 1){
                      this.setState({
                        orderId: json.ServerData[0].id
                      });
                  }
                  ToastAndroid.show(strings.lblSystemSorry + strInfo + "。", ToastAndroid.SHORT);
              }).catch((error)=>{
                  ToastAndroid.show("对不起，数据请求失败：" + error.message, ToastAndroid.SHORT);
              }).done();
            }
        }
    },
    btnPayment: function(){
      if(this.props.isBuyNow == true){
        if(this.state.addressId == 0){
            ToastAndroid.show('请选择配送地址',ToastAndroid.SHORT); return;
        }
        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected == true){
                var me = this;
                store.dispatch(getPaymentInfo('http://120.25.72.158:8369/payHunter-1.0-SNAPSHOT/aliPay')).then((json) =>{
                    NativeModules.AlipayManager.pay(json.ServerData.data,this.paymentCallback);
                    // var testStr = 'resultStatus={9000};memo={处理成功};result={partner="2088221420923333"&seller_id="creativestar@cssinco.cn"&out_trade_no="20160329150156"&subject="支付宝支付"&body="用于测试支付宝快捷支付测试！"&total_fee="0.01"&notify_url="http://218.77.183.189:8090/merry/notify_url.jsp"&service="mobile.securitypay.pay"&payment_type="1"&_input_charset="utf-8"&it_b_pay="1d"&return_url="http://www.tianjiandao.com"&success="true"&sign_type="RSA"&sign="ecPYpWTTcqy7Ydsv+rRq2KRAoPMKPZyV4zljvw0rxebV5RMRybAnNaaPRqCq5lltvtoJ8sDDePU2uuUMxoKXkPaP32mikLojxPBeVP6PVSEt4WNHipRz2f5u0Izim6Caxk5J4Nh51/jK6bKGZNPBWLNQL/VJHwSkyPlBIbHStAY="}';
                    // this.paymentCallback(testStr);
                }).catch((error)=>{
                    ToastAndroid.show("对不起，数据请求失败：" + error.message, ToastAndroid.SHORT);
                }).done();
            }else{
                ToastAndroid.show("对不起，当前无法连接网络（offline）", ToastAndroid.SHORT);
            }
        });
      }else{
          if(this.state.addressId == 0){
              ToastAndroid.show('请选择配送地址',ToastAndroid.SHORT); return;
          }
          var me = this;
          var objData = {
              uid: window.GLOBAL.user_id,
              address_id: this.state.addressId,
              pIds: this.props.pIds
          };
          store.dispatch(postsOrderInfo(objData)).then((json) =>{
            if(json.ServerData[0].paid == false){
              this.props.obj.refreshData(true);
              this.backPage();
            }
          }).catch((error)=>{
              ToastAndroid.show("对不起，数据请求失败：" + error.message, ToastAndroid.SHORT);
          }).done();
      }
    },
    backPage: function(){
      const { navigator } = this.props;
      if(navigator) {
         navigator.pop();
      }
    },
    setPaymentType: function(type){
      switch (type) {
        case "WeChat":
          this.setState({paymentType: 1,paymentString: type});
          break;
        case "Cash":
          this.setState({paymentType: 2,paymentString: type});
          break;
        case "Alipay":
          this.setState({paymentType: 3,paymentString: type});
          break;
        case "OnlineBanking":
          this.setState({paymentType: 4,paymentString: type});
          break;
      }
    },
    setDeliveryType: function(type){
      switch (type) {
        case "takeAway":
          this.setState({deliveryType: 1,deliveryString: type});
          break;
        case "pickup":
          this.setState({deliveryType: 2,deliveryString: type});
          break;
        case "express":
          this.setState({deliveryType: 3,deliveryString: type});
          break;
      }
    },
    AddressManager: function(){
      const { navigator } = this.props;
      if(navigator) {
         navigator.push({
             name: 'AddressList',
             component: AddressList,
             params:{
               refreshAddressData: this.refreshAddress
             }
         });
      }
    },
    SelectInvoice: function(){
      const { navigator } = this.props;
      if(navigator) {
         navigator.push({
             name: 'SelectInvoice',
             component: SelectInvoice,
             params:{
               invoiceStatus: this.state.invoiceStatus,
               invoiceName: this.state.invoiceName,
               selectedInvoices: this.selectedInvoices
             }
         });
      }
    },
    selectedInvoices: function(index,Name){
      if(index == 1){
        this.setState({
            invoiceStatus: strings.lblNoInvoice
        })
      }else if(index == 2){
        this.setState({
            invoiceStatus: strings.lblIndividualInvoice
        })
      }else if(index == 3){
        //Name： 公司名称
        this.setState({
            invoiceName: Name,
            invoiceStatus: strings.lblCompanyInvoices
        })
      }
    },
    render: function () {
      var backgroundColor = this.props.isBuyNow ? "#1d1d1d": "#1d1d1d";
      var borderColor = this.props.isBuyNow ? "#1d1d1d": "#1d1d1d";
      var underlayColor = this.props.isBuyNow ? "#1d1d1d": "#1d1d1d";

      var addressView = null;
      if(this.state.addressId == 0){
          addressView = null
      }else{
          addressView = <View style={{marginBottom: 5}}>
              <Text style={{color: 'black'}}>
                  {strings.lblReceiver}<Text style={{color: '#767676'}}>{this.state.receiver}</Text>{'\r\n'}
                  {strings.lblShippingAddress}<Text style={{color: '#767676'}}>{this.state.delivery_address}</Text>
              </Text>
          </View>
      }
      return(
        <View style={styles.OrderConfirm}>
            <ToolbarAndroid
                navIcon={require('image!leftarrow1')}
                onIconClicked={this.backPage}
                style={styles.toolbar}
                titleColor="#A1A3A5"
                title={strings.lblOrderConfirm} />

            <ScrollView style={{borderTopWidth: 1,borderColor: '#E7EAEC',height: (height -125)}}>
                <View style={styles.viewDeliveryMethod}>
                    <Text style={{flex:2,color:'#767676'}}>{strings.lblDeliveryMethod}</Text>
                    <Picker
                      style={[styles.deliveryPicker,{flex:1,width:100,marginRight:-25}]}
                      selectedValue={this.state.deliveryString}
                      prompt={strings.lblDeliveryMethod}
                      onValueChange={this.setDeliveryType}>
                      <Picker.Item label="带走" value="takeAway" />
                      <Picker.Item label="自取" value="pickup" />
                      <Picker.Item label="快递" value="express" />
                    </Picker>
                    <Icon name="ios-arrow-forward" size={25} style={{marginRight: 10}}/>
                </View>

                <TouchableHighlight onPress={this.AddressManager} underlayColor="#B3B3B3" style={styles.btnAddress}>
                    <View style={[styles.viewDeliveryMethod1,{marginLeft: 0,marginTop:0,marginRight:0,marginBottom:0}]}>
                        <View style={styles.viewDeliveryMethod3}>
                            <Text style={{flex:2,color:'#383838'}}>{strings.lblDeliveryAddress}</Text>
                            <Text style={{position: 'absolute',right: 30}}>{strings.lblSelectDeliveryAddress}</Text>
                            <Icon name="ios-arrow-forward" size={25} style={{marginRight: 10}}/>
                        </View>
                        {addressView}
                    </View>
                </TouchableHighlight>

                <View style={styles.viewDeliveryMethod1}>
                    <Text style={{color:'#383838'}}>{strings.lblGoodsList}</Text>
                    <View style={{}}>
                    {
                        this.state.arrayList.map((item,index) => {
                          return (
                            <View key={index} style={styles.row}>
                                <Image
                                    source={{uri: item.product_image}}
                                    resizeMode="contain"
                                    style={styles.cellImage} />
                                <View style={styles.textContainer}>
                                    <Text style={styles.movieTitle} numberOfLines={1}>
                                        {item.name}
                                    </Text>
                                    <View style={styles.viewNumRadius}>
                                      <Text style={styles.movieRating} numberOfLines={1}>
                                          {item.price}
                                      </Text>
                                    </View>
                                    <View style={styles.viewNumQuantity}>
                                      <Text style={styles.movieRating} numberOfLines={1}>
                                          {'x' + item.quantity}
                                      </Text>
                                    </View>
                                </View>
                            </View>
                          );
                        })
                    }
                    </View>
                </View>

                <View style={styles.viewDeliveryMethod}>
                    <Text style={{flex:2,color:'#383838'}}>{strings.lblPaymentType}</Text>
                    <Picker
                      style={[styles.deliveryPicker,{color: 'rgb(165,165,165)',flex:1,width:100,marginRight:-25}]}
                      selectedValue={this.state.paymentString}
                      prompt={strings.lblPaymentType}
                      onValueChange={this.setPaymentType}>
                      <Picker.Item label="微信" value="WeChat" />
                      <Picker.Item label="现金" value="Cash" />
                      <Picker.Item label="支付宝" value="Alipay" />
                      <Picker.Item label="银联网银" value="OnlineBanking" />
                    </Picker>
                    <Icon name="ios-arrow-forward" size={25} style={{marginRight: 10}}/>
                </View>

                <View style={styles.viewDeliveryMethod}>
                    <Text style={{flex:2,color:'#383838'}}>优惠卷</Text>
                    <Text style={{position: 'absolute',right: 30}}>暂不使用优惠卷</Text>
                    <Icon name="ios-arrow-forward" size={25} style={{marginRight: 10}}/>
                </View>
                <TouchableHighlight onPress={this.SelectInvoice} underlayColor="#B3B3B3" style={styles.btnAddress}>
                    <View style={[styles.viewDeliveryMethod2,{marginLeft: 0,marginTop:0,marginRight:0,marginBottom:0}]}>
                        <Text style={{flex:2,color:'#383838'}}>{strings.lblInvoice}</Text>
                        <Text style={{position: 'absolute',right: 30}}>{this.state.invoiceStatus}</Text>
                        <Icon name="ios-arrow-forward" size={25} style={{marginRight: 10}}/>
                    </View>
                </TouchableHighlight>
                <View>
                    <Text style={styles.txtInfo}>
                        {strings.lblTotal}
                        <Text style={styles.txtRedInfo}>{this.state.total.replace('（','').replace('）','')} </Text>
                        {strings.lblTotalAmount}
                        <Text style={styles.txtRedInfo}>{this.props.grandTotal} </Text>
                        {strings.lblAmountUnit}
                    </Text>
                </View>
            </ScrollView>
            <View style={styles.address}>
            <TouchableHighlight onPress={this.btnPayment} style={[styles.button,{backgroundColor: backgroundColor,borderColor: borderColor}]} underlayColor={underlayColor}>
                <Text style={styles.buttonText}>{this.props.isBuyNow ? strings.btnBuyNow: strings.lblSubmitOrder}</Text>
            </TouchableHighlight>
            </View>
        </View>
      );
    }
});
module.exports = OrderConfirm;
