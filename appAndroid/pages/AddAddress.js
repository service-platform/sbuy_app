import React from 'react';
import {
    Image,
    StyleSheet,
    Text,
    View,
    Switch,
    TextInput,
    Dimensions,
    ToolbarAndroid,
    ToastAndroid,
} from 'react-native';
import Button from 'apsl-react-native-button';
var Icon = require('react-native-vector-icons/Ionicons');
var {height, width} = Dimensions.get('window');
import store from './../redux/store';
import { postsAddAddress,getUpdateAddressInfo,getGetAddressList } from './../redux/actions/productsAction';
var strings = require('./../utils/LocalizedStrings');

var AddAddress = React.createClass({
    getInitialState: function () {
      return {
        receiver: '', // 收货人
        phone: '', // 手机号码
        delivery_address: '', // 收货地址
        defaultAddress: false, // 默认为收货地址(1为是，0为不是)
        isDisabled: false,
        red: '#f23020'
      }
    },
    backPage: function(){
      const { navigator } = this.props;
      if(navigator) {
         navigator.pop();
      }
    },
    componentWillMount: function(){
        store.dispatch(getGetAddressList(window.GLOBAL.user_id)).then((json) =>{
            if (json.ServerData.length <= 0){
                this.setState({defaultAddress: true});
            }
        }).catch((error)=>{}).done();

        if(this.props.mark == 'Update'){
            this.setState({
              receiver: this.props.receiver,
              phone: this.props.phone,
              delivery_address: this.props.delivery_address,
              defaultAddress: (this.props.default == 1)?true:false,
              isDisabled: false,
            });
        }else{
            this.setState({
              isDisabled: true,
            });
        }
    },
    btnSaveOrUpdate: function(){
        if(this.props.mark == 'Add'){
            if(this.state.receiver == ''){
                ToastAndroid.show(strings.toastReceiverNotEmpty, ToastAndroid.SHORT); return;
            }
            if(/^[1][358][0-9]{9}$/.test(this.state.phone) == false){
                ToastAndroid.show(strings.toastPhoneIncorrect, ToastAndroid.SHORT); return;
            }
            if(this.state.delivery_address == ''){
                ToastAndroid.show(strings.toastShippingAddressNotEmpty, ToastAndroid.SHORT); return;
            }

            var objData = {
                user: window.GLOBAL.user_id,
                receiver: this.state.receiver,
                phone: this.state.phone,
                delivery_address: this.state.delivery_address,
                default: (this.state.defaultAddress == true)?1:0,
            }
            var me = this;
            store.dispatch(postsAddAddress(objData)).then((json) =>{
                if(json.ServerData.add_address != undefined){
                    ToastAndroid.show("对不起，添加收货地址失败！", ToastAndroid.SHORT); return;
                }else{
                    ToastAndroid.show(strings.alertAddAddressOK, ToastAndroid.SHORT);
                    me.backPage(); me.props.refreshData();

                    me.props.setAddressData({
                      addressId: json.ServerData[0].id,
                      receiver: json.ServerData[0].receiver,
                      delivery_address: json.ServerData[0].delivery_address,
                    });
                }
            }).catch((error)=>{
                ToastAndroid.show("对不起，数据请求失败：" + error.message, ToastAndroid.SHORT);
            }).done();
        }else{
            var objData = {
                user: window.GLOBAL.user_id,
                id: this.props.id,
                receiver: this.state.receiver,
                phone: this.state.phone,
                delivery_address: this.state.delivery_address,
                default: (this.state.defaultAddress == true)?1:0,
            }
            var me = this;
            store.dispatch(getUpdateAddressInfo(objData)).then((json) =>{
                if(json.ServerData.update_address != undefined){
                    ToastAndroid.show("对不起，修改收货地址失败！", ToastAndroid.SHORT); return;
                }else{
                    ToastAndroid.show(strings.alertUpdateAddressOk, ToastAndroid.SHORT);
                    me.backPage(); me.props.refreshData();
                    me.props.setAddressData({
                      addressId: json.ServerData[0].id,
                      receiver: json.ServerData[0].receiver,
                      delivery_address: json.ServerData[0].delivery_address,
                    });
                }
            }).catch((error)=>{
                ToastAndroid.show("对不起，数据请求失败：" + error.message, ToastAndroid.SHORT);
            }).done();
        }
    },
    isNotEmpty: function(){
        if(this.state.receiver == '' || this.state.phone == '' || this.state.delivery_address == ''){
            return false;
        }else{
            if(/^[1][358][0-9]{9}$/.test(this.state.phone) == false){
                return false;
            }
            return true;
        }
    },
    render: function(){
        var toolbarTitle = strings.lblAddAddressTitle;
        if(this.props.mark == 'Update'){
            toolbarTitle = strings.lblUpdateAddressTitle;
        }else{
            toolbarTitle = strings.lblAddAddressTitle;
        }
        return (
            <View style={styles.container}>
                <ToolbarAndroid
                    navIcon={require('image!leftarrow1')}
                    onIconClicked={this.backPage}
                    style={styles.toolbar}
                    titleColor="#A1A3A5"
                    title={toolbarTitle} />
                <View style={styles.viewField}>
                    <View style={styles.viewFieldContent}>
                        <Text style={styles.txtFieldName}>{strings.lblAddReceiver}</Text>
                        <TextInput style={styles.inputField} placeholder={strings.placeholderReceiver} onChangeText={(text) => this.setState({receiver: text,isDisabled: !this.isNotEmpty()})} value={this.state.receiver}/>
                    </View>
                    <View style={styles.viewFieldContent}>
                        <Text style={styles.txtFieldName}>{strings.lblAddressPhone}</Text>
                        <TextInput style={styles.inputField} placeholder={strings.placeholderPhone} onChangeText={(text) => {
                          this.setState({phone: text});
                          this.setState({isDisabled: !this.isNotEmpty()});
                        }} value={this.state.phone}/>
                    </View>
                    <View style={styles.viewFieldContent}>
                        <Text style={styles.txtFieldName}>{strings.lblAddShippingAddress}</Text>
                        <TextInput style={styles.inputField} placeholder={strings.placeholderAddress}  onChangeText={(text) => this.setState({delivery_address: text,isDisabled: !this.isNotEmpty()})} value={this.state.delivery_address} />
                    </View>
                    <View style={styles.viewFieldContent}>
                        <Text style={styles.txtFieldName}>{strings.lblIsDefaultAddress}</Text>
                        <Switch style={{marginRight: 10}} onValueChange={(text) => this.setState({defaultAddress: text})} value={this.state.defaultAddress} onTintColor={'#baa071'}/>
                    </View>
                    <View style={styles.viewBtnContent}>
                        <Button onPress={this.btnSaveOrUpdate} style={styles.button} underlayColor='#baa071' isDisabled={this.state.isDisabled} textStyle={styles.buttonText} disabledStyle={[styles.button,{backgroundColor:'#CBCBCB'}]}>
                            {strings.btnSaveAndOk}
                        </Button>
                    </View>
                </View>
            </View>
        );
    }
});
module.exports = AddAddress;

var styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#F9F9F9'
    },
    toolbar:{
        backgroundColor: '#F3F3F4',
        height: 55,
    },
    viewField: {
        flexDirection: 'column',
        height: height - 81
    },
    viewFieldContent:{
        flexDirection: 'row',
        borderColor: '#E5E5E5',
        borderBottomWidth: 1,
        height: 40
    },
    txtFieldName:{
        color: '#212121',
        flex: 1,
        marginLeft: 10,
        marginTop: 11
    },
    inputField:{
        flex: 3,
        marginTop: 8,
        backgroundColor: 'transparent'
    },
    viewBtnContent:{
        position: 'absolute',
        backgroundColor: '#F8F8F8',
        borderColor: '#E0E0E0',
        borderTopWidth: 1,
        height: 48,
        width: width,
        bottom: 0,
        paddingTop: 5,
    },
    button: {
      height: 36,
      flexDirection: 'row',
      backgroundColor: '#1D1D1D',
      borderWidth: 0,
      marginBottom: 10,
      alignSelf: 'center',
      justifyContent: 'center',
      alignItems:'center',
      borderRadius: 0,
      paddingLeft: 48,
      paddingRight: 48,
    },
    buttonText: {
      fontSize: 16,
      color: 'white',
      alignSelf: 'center'
    },
});
