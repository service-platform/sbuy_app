var React = require('react-native');
var styles = require('./../styles/Favorites');
var Icon = require('react-native-vector-icons/Ionicons');
var {
    View,
    Text,
    Image,
    Alert,
    ListView,
    ScrollView,
    AsyncStorage,
    ToastAndroid,
    TouchableHighlight,
} = React;
import store from './../redux/store';
import { postsGoodsUrl,postsFavorites } from './../redux/actions/productsAction';
var strings = require('./../utils/LocalizedStrings');

var Favorites = React.createClass({
    getInitialState: function () {
        return {
            dataSource: new ListView.DataSource({
              rowHasChanged: (r1, r2) => r1 !== r2
            })
        };
    },
    refreshData: function(){
      var me = this;
      store.dispatch(postsGoodsUrl(ServerURL + "/get-wish/" + window.GLOBAL.user_id + "/")).then((json) =>{
        if(json.ServerData <= 0){
            this.setState({
                isFirstLoading: false,
                dataSource: this.getDataSource([])
            });
        }else{
            var arrays = json.ServerData
            this.setState({
                isFirstLoading: false,
                dataSource: this.getDataSource(arrays)
            });
        }
      }).catch((error)=>{
          ToastAndroid.show("对不起，数据请求失败：" + error.message, ToastAndroid.SHORT);
      }).done();
    },
    getDataSource: function(subjects: Array<any>): ListView.DataSource {
        return this.state.dataSource.cloneWithRows(subjects);
    },
    //删除收藏
    deleteFavorites: function (id) {
        var me = this;
        Alert.alert(
            strings.alertPrompt,
            strings.formatString(strings.alertDeleteFavorite,id),
            [
              {text: strings.alertCancel},
              {text: strings.alertOk, onPress: () => {
                  var obj = {
                    user_id: window.GLOBAL.user_id,
                    product_id: id,
                    follow: false
                  };
                  debugger;
                  store.dispatch(postsFavorites(obj)).then((json) =>{
                    if(json.ServerData.add_status == "delete"){
                        me.refreshData();
                        ToastAndroid.show("取消收藏", ToastAndroid.SHORT);
                    }
                  }).catch((error)=>{
                      ToastAndroid.show("对不起，数据请求失败：" + error.message, ToastAndroid.SHORT);
                  }).done();
              }},
            ]
        )
    },
    openDetail: function(id){
      this.props.openDetailByFavorites(id,true,1);
    },
    renderRow: function(data, sectionID, rowID, highlightRowFunc){
        var image =  ServerURL + data.image;
        var title = data.name;
        var rating = data.price;
        var desc = data.description;

        return (
          <TouchableHighlight
              onPress={()=>{this.openDetail(data.product)}}
              underlayColor="#E6E6E6aa"
              style={styles.touchableHighlight}>
                  <View key={data.id} style={styles.row}>
                      <Image
                          source={{uri: image}}
                          resizeMode="contain"
                          style={styles.cellImage} />
                      <View style={styles.textContainer}>
                          <Text style={styles.movieTitle} numberOfLines={1}>
                              {(title.length >= strings.splitStr) ? title.substr(0,strings.splitStr) + '...': title }
                          </Text>
                      </View>

                      <View style={styles.viewToDetail}>
                        <View style={styles.viewNum}>
                          <Text style={styles.movieRating} numberOfLines={1}>
                              {rating}
                          </Text>
                        </View>
                      </View>

                      <TouchableHighlight
                          onPress={()=>{this.deleteFavorites(data.product)}}
                          underlayColor="#E6E6E6"
                          style={styles.closeBtn}
                          onShowUnderlay={() => highlightRowFunc(sectionID, rowID)}
                          onHideUnderlay={() => highlightRowFunc(null, null)}>
                          <Icon  name={"close-circled"} size={25} style={styles.closeIcon} />
                      </TouchableHighlight>
                  </View>
              </TouchableHighlight>
        );
    },
    render: function () {
        return (
            <ScrollView style={styles.scrollView}>
            <ListView
                dataSource={this.state.dataSource}
                renderRow={this.renderRow}
                style={styles.listView}
                contentContainerStyle={styles.separator}
                automaticallyAdjustContentInsets={false}
                keyboardDismissMode="on-drag"
                keyboardShouldPersistTaps={true}
                showsVerticalScrollIndicator={false}
              />
            </ScrollView>
        );
    }
});
module.exports = Favorites;
