var React = require('react-native');
var {
  StyleSheet,
  Dimensions
} = React;
let { width, height } = Dimensions.get('window');

var localStyles = StyleSheet.create({
  container: {
    flex: 1,
    height: height,
    width: width,
    backgroundColor: 'rgb(240,239,245)',
  },
  toolbar: {
    backgroundColor: '#F3F3F4',
    height: 55,
  },
  viewDescription:{
    flexDirection:'row',
    marginTop: 10,
    marginLeft: 10
  },
  imgProduct:{
    width:35,
    height:50
  },
  subContainer:{
    flex: 1,
    alignItems: 'center',
    marginLeft: 20,
    marginTop: 15,
    flexDirection: 'row',
  },
  viewStarRating: {
    width: 100
  },
  txtInfo: {
    marginRight:5,
    marginTop:-2
  },
  viewText: {
     margin: 10,
     borderColor: '#DADADA',
     borderWidth: 1,
     borderRadius: 4
  },
  txtInput: {
    backgroundColor: 'transparent',
    textAlignVertical: 'top'
  },
  imgPicture:{
    width: 65,
    height: 85,
    marginRight: 5,
    marginBottom: 5,
    borderWidth: 2,
    borderColor: 'rgb(224,224,224)'
  },
  viewAdd: {
    borderColor: 'rgb(224,224,224)',
    borderWidth: 1,
    width: 65,
    height: 85,
    paddingLeft: 4,
    paddingTop: -13,
    backgroundColor: 'rgb(247,247,247)'
  },
  iconAdd:{
    color: 'rgb(194,194,194)'
  },
  button: {
    height: 36,
    width: (width - 20),
    marginTop: 0,
    flexDirection: 'row',
    backgroundColor: '#1AB394',
    borderColor: '#1AB394',
    borderWidth: 1,
    borderRadius: 2,
    justifyContent: 'center',
    marginBottom: 10
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
});
module.exports = localStyles;
