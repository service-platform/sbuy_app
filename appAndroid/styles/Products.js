var React = require('react-native');
var {
  StyleSheet,
  Dimensions
} = React;
let { width, height } = Dimensions.get('window');

var localStyles = StyleSheet.create({
  container: {
    paddingLeft: 10,
    paddingRight: 10,
    height: (height - 128),
    width: width,
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
  },
  //以下是 列表  样式－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－
  scrollView:{
    width: width,
    height: (height - 161),
    position:'absolute',
    top: 33,
    left: 0
  },
  separator:{
    paddingTop: 10,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  touchablelight: {
      backgroundColor: 'white',
      marginBottom: 10,
      marginLeft: parseInt((width-((parseInt(width/2)-20)*2))/3),

      shadowColor: '#666666',
      shadowOffset: {
          width: 0,
          height: 0
      },
      shadowOpacity: 0.5,
      shadowRadius: 6,
      elevation: 5
  },
  row: {
      alignItems: 'center',
      backgroundColor: 'white',
      borderColor: '#ECECEC',
      borderWidth: 1,
      height: 205,
  },
  cellImage: {
      backgroundColor: 'white',
      height: 160,
      width: parseInt(width/2)-20,
  },
  movieTitle: {
      fontSize: 15,
      fontWeight: '500',
      width:280,
      color: '#666'
  },
  viewToDetail:{
    position: 'absolute',
    marginTop: 20,
    marginLeft: 10,
  },
  viewNum:{
    backgroundColor: '#78767B',
    height: 20,
    borderRadius: 3,
  },
  textNum:{
    color: 'white',
    fontFamily: 'Arial',
    marginLeft:10,
    marginRight:10
  },
  textContainer:{
    position: 'absolute',
    backgroundColor:'transparent',
    marginTop: 0,
    paddingLeft: 10,
  },
  viewSearch: {
    width: width,
    height: 30,
    paddingRight: 6,
    paddingLeft: 8,
    flexDirection: 'row',
    marginTop: 3,
    borderBottomWidth: 2,
    borderColor: '#DEDEDE',
  },
  viewInput: {
    marginRight: 10,
    flex: 3,
    height: 30,
    paddingBottom: 5
  },
  txtInput: {
    backgroundColor: 'transparent',
    padding: 0,
    paddingLeft: 2
  },
  btnSearch: {
    flex: 1,
    borderRadius: 4,
    justifyContent: 'center',
  },
  searchIndex:{
    width:15,
    height:15,
    marginLeft:60
  },
  txtSearch: {
    color: 'white',
    textAlign: 'center'
  },
  viewTypes: {
    backgroundColor: 'white',
    position: 'absolute',
    left: width,
    top: 32,
    paddingTop: 5,
    paddingBottom: 5,
    borderColor: '#DEDEDE',
    borderBottomWidth: 2,
    width: width
  }
});
module.exports = localStyles;
