var React = require('react-native');
var {
  StyleSheet,
  Dimensions
} = React;
let { width, height } = Dimensions.get('window');

var localStyles = StyleSheet.create({
  container: {
    paddingLeft: 10,
    paddingRight: 10,
    height: height,
    width: width,
    alignItems: 'center',
    backgroundColor: 'rgb(255,255,255)',
  },
  imageBackground:{
    width:width,
    height:230,
    marginBottom:30
  },
  fieldView: {
    borderBottomWidth: 1,
    borderColor: 'rgb(228,228,228)',
    height:40,
    marginLeft: 25,
    marginRight: 25,
    marginBottom: 10
  },
  imageIcon:{
    position: 'absolute',
    width:32,
    height:32
  },
  searchInput: {
    marginTop: -40,
    marginLeft: 30,
    borderWidth: 0,
    textDecorationColor: 'rgb(228,228,228)',
    backgroundColor: 'transparent',
  },
  button: {
    height: 36,
    width: 340,
    flex: 1,
    marginTop:6,
    flexDirection: 'row',
    backgroundColor: '#48BBEC',
    borderColor: '#48BBEC',
    borderWidth: 1,
    borderRadius: 2,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
  facebookButton:{
    height: 36,
    width: 340,
    flex: 1,
    marginTop:6,
    flexDirection: 'row',
    backgroundColor: 'rgb(59,89,152)',
    borderColor: 'rgb(52, 84, 150)',
    borderWidth: 1,
    borderRadius: 2,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
  facebookView:{
    flexDirection:'row'
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  }
});
module.exports = localStyles;
