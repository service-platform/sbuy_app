var React = require('react-native');
var {
  StyleSheet,
  Dimensions
} = React;
let { width, height } = Dimensions.get('window');

var localStyles = StyleSheet.create({
  container:{
    flex: 1,
  },
  txtTip:{
    fontSize: 18,
    alignSelf:'center',
    marginTop: parseInt(height / 2)-50,
  }
});
module.exports = localStyles;
