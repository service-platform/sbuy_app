var React = require('react-native');
var {
  StyleSheet,
  Dimensions
} = React;
let { width, height } = Dimensions.get('window');

var localStyles = StyleSheet.create({
  container: {
    height: height,
    width: width,
    backgroundColor: 'rgb(240,239,245)',
  },
  toolbar: {
    backgroundColor: '#F3F3F4',
    height: 55,
  },
  rowSeparator: {
      backgroundColor: '#f5f5f5',
      height: 1,
      marginLeft: 10,
      marginRight: 10
  },
  rowSeparatorHide: {
      opacity: 0.0
  },
  cellImage:{
      width: 60,
      height: 80
  },
  row: {
      alignItems: 'center',
      backgroundColor: 'white',
      flexDirection: 'row',
      padding: 10
  },
  image: {
      backgroundColor: '#dddddd',
      height: 80,
      marginRight: 10,
      width: 55
  },
  movieTitle: {
      fontSize: 18,
      fontWeight: '500',
      marginBottom: 2,
      color: 'rgb(36,36,36)'
  },
  movieRating: {
      marginTop: 5,
      color: '#999999',
      fontSize: 14
  },
  imageHeader:{
      width:84,
      height:84,
      borderRadius:42,
      borderWidth:2,
      borderColor:'white'
  },
  viewToDetail:{
    position: 'absolute',
    top:45,
    right:8,
    flexDirection: 'row'
  },
  viewNum:{
    backgroundColor: '#BAA071',
    height: 20,
    paddingLeft: 3,
    paddingRight: 3,
    borderRadius: 10
  },
  textNum:{
    color: 'white',
    fontFamily: 'Arial',
    marginLeft:10,
    marginRight:10
  },
});
module.exports = localStyles;
