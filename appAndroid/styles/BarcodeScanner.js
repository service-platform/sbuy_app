var React = require('react-native');
var {
  StyleSheet,
  Dimensions
} = React;
let { width, height } = Dimensions.get('window');

var localStyles = StyleSheet.create({
  container: {
     flex: 1,
     backgroundColor: 'transparent'
  },
  viewToolbar:{
    backgroundColor: '#241f19',
    opacity: 0.9,
    position:'absolute',
    top: 0,
    left:0,
    width: width,
    height: 52,
    flexDirection: 'row',
    paddingRight: 10,
    paddingLeft: 10,
  },
  btnBack:{
    height: 30,
    width: 30,
    flex: 1,
    top:10,
    backgroundColor: 'transparent',
    borderRadius: 4,
    justifyContent: 'center',
  },
  btnFromPhoto:{
    height: 30,
    width: 30,
    flex: 1,
    top:10,
    backgroundColor: 'transparent',
    borderRadius: 4,
    justifyContent: 'center',
  },
  viewIcon:{
    color: '#BAA071',
    alignSelf: 'center'
  },
  txtToolbar:{
    color: 'white',
    flex: 8,
    textAlign: 'center',
    fontSize: 16,
    top: 16,
  },
  imgScanningStrip:{
    width: (width-20),
    height: 13,
    position: 'absolute',
    top: 190,
    left: 10,
  },
  viewInput:{
    backgroundColor: '#241f19',
    position:'absolute',
    top: 0,
    left:0,
    width: width,
    height: 40,
    flexDirection: 'row',
    paddingRight: 10,
    paddingLeft: 10,
  },
  txtInput:{
    height: 28,
    width: (width - 100),
    backgroundColor: '#ECECEC'
  }
});
module.exports = localStyles;
