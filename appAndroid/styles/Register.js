var React = require('react-native');
var {
  StyleSheet,
  Dimensions
} = React;
let { width, height } = Dimensions.get('window');

var localStyles = StyleSheet.create({
  container: {
    height: height,
    width: width,
    alignItems: 'center',
    backgroundColor: 'rgb(240,239,245)',
    marginTop: 0,
  },
  viewContainer:{
    borderTopWidth: 1,
    borderColor:'#D0D0D0',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10
  },
  facebookButton:{
    height: 36,
    width: 340,
    flex: 1,
    marginTop:6,
    flexDirection: 'row',
    backgroundColor: 'rgb(78,217,100)',
    borderColor: 'rgb(78,217,100)',
    borderWidth: 1,
    borderRadius: 2,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
  geographicalButton:{
    height: 36,
    width: 340,
    flex: 1,
    marginTop:6,
    flexDirection: 'row',
    backgroundColor: 'rgb(0,122,255)',
    borderColor: 'rgb(0,122,255)',
    borderWidth: 1,
    borderRadius: 2,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
  facebookView:{
    flexDirection:'row'
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
  viewToolbarIcon: {
    backgroundColor: '#F3F3F4',
    height: 55,
    width: width,
    borderColor: 'red',
    borderBottomWidth: 1
  },
});
module.exports = localStyles;
