var React = require('react-native');
var {
  StyleSheet,
  Dimensions
} = React;
let { width, height } = Dimensions.get('window');

var localStyles = StyleSheet.create({
  container: {
    paddingLeft: 10,
    paddingRight: 10,
    height: (height - 128),
    width: width,
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
  },
  toolbar: {
    backgroundColor: '#F3F3F4',
    height: 55,
  },
  viewToolbarIcon: {
    backgroundColor: '#F3F3F4',
    height: 55,
  },
  fieldView: {
    position: 'absolute',
    height:40,
    width:280,
    marginBottom: 10,
    top: 0,
    left: 0,
    flexDirection: 'row',
    backgroundColor: 'rgb(255,255,255)',
    borderColor: '#E5E6E7',
    borderBottomWidth: 1,
    borderRightWidth: 1
  },
  imageHighlight:{
    alignSelf: 'center',
    backgroundColor: 'rgb(255,255,255)'
  },
  imageIcon:{
    width:32,
    height:40,
    backgroundColor: 'rgb(255,255,255)'
  },
  searchInput: {
    alignSelf: 'center',
    marginLeft: 3,
    borderWidth: 0,
    width:240,
    backgroundColor: 'transparent',
  },
  button: {
    height: 40,
    width: (width-280),

    backgroundColor: '#1AB394',
    borderColor: '#1AB394',
    marginBottom: 10,

    justifyContent: 'center'
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
  tabView: {
    flex: 1,
    padding: 10,
    backgroundColor: 'rgba(0,0,0,0.01)',
  },
  card: {
    borderWidth: 1,
    backgroundColor: '#fff',
    borderColor: 'rgba(0,0,0,0.1)',
    margin: 5,
    height: 150,
    padding: 15,
    shadowColor: '#ccc',
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.5,
    shadowRadius: 3,
  },
  //------------------------------------------------------------
  floatView:{
    backgroundColor:'white',
    width: width,
    height: (height - 73),
    position: 'absolute',
    top: 0,
    left: 0,
  },
  subContainer:{
    alignItems:"center",
    borderColor: '#D2D2D2',
    borderLeftWidth: 1,
  },
  imgDetail:{
    width:width,
    height:300,
    marginTop:10,
    marginBottom:10,
    borderColor: '#D2D2D2',
    borderBottomWidth: 1,
    backgroundColor: 'white'
  },
  txtView:{
    padding: 5,
    borderColor: '#D2D2D2',
    borderLeftWidth: 1
  },
  txtName:{
    fontSize: 16
  },
  price:{
    paddingLeft: 0,
    fontSize: 16,
    color:'#4EA5F1'
  },
  desc:{
    color:'black',
    marginLeft: 15,
    marginRight: 15,
    marginBottom: 10
  },
  lblShowMoreDetails:{
    paddingLeft: 0,
    fontSize: 16,
    color:'#4EA5F1',
    marginBottom: 40,
    marginLeft: 15,
  },
  viewBottom:{
    position: 'absolute',
    top: (height - 113),
    left: 0,
    height:40,
    width: width,
    borderColor: 'rgb(208,208,208)',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    backgroundColor: 'rgb(247,247,247)',
    flexDirection: 'row',
    flexWrap: 'nowrap'
  },
  viewNum:{
    flex: 2
  },
  viewTxtNum:{
    alignSelf:'center',
    marginTop:10,
    color : 'black'
  },
  greenButton: {
    height: 27,
    width: 50,
    flex: 1,
    marginTop:6,
    backgroundColor: '#1AB394',
    borderColor: '#1AB394',
    borderWidth: 1,
    borderRadius: 4,
    marginBottom: 10,
    marginLeft: 7,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
  redButton: {
    height: 33,
    width: 50,
    flex: 1,
    marginTop:3,
    backgroundColor: '#baa071',
    borderColor: '#baa071',
    borderWidth: 1,
    marginBottom: 10,
    marginLeft: 7,
    marginRight: 7,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
  greenButtonText: {
    color: 'white',
    alignSelf: 'center'
  },
  animatedScrollView:{
    flex:1,
    height: (height-260)
  },
  subtractionBtn:{
    backgroundColor: '#F5F5F5',
    width:36,
    height:30,
    justifyContent:'center'
  },
  iconQuantity:{
    color: '#999999',
    alignSelf: 'center'
  },
  txtQuantity:{
    color: '#000000',
    fontSize:16,
    alignSelf: 'center'
  },
  viewQuantityNum:{
    backgroundColor: 'white',
    width:36,
    height:30,
    justifyContent:'center',
    borderLeftWidth:1,
    borderRightWidth:1,
    borderColor: 'white'
  },
  viewSubQuantity:{
    flex:1,
    flexDirection: 'row',
    borderColor: '#BDBDBD',
    borderWidth: 1
  },
  txtTitleQuantity:{
    flex:2,
    marginTop:10,
    marginLeft:10
  },
  viewQuantity:{
    flexDirection: 'row',
    position: 'absolute',
    right: 15,
    marginTop: -27,
  },
  //--------------------------------------------------------------------------------
  viewBtnEditLayout:{
    position: 'absolute',
    top: 14,
    right: 49,
  },
  viewBtnEdit:{
    borderWidth: 1,
    borderColor: '#577FF5',
    padding: 5,
    paddingTop: 2,
    paddingBottom: 2,
    borderRadius: 3
  },
  txtBtnEdit:{
    color:'#577FF5',
    fontSize: 16,
    // fontWeight: 'bold'
  },
  viewShare: {
    position:'absolute',
    right: 10,
    marginTop:-40
  }
});
module.exports = localStyles;
