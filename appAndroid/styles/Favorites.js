var React = require('react-native');
var {
  StyleSheet,
  Dimensions
} = React;
let { width, height } = Dimensions.get('window');

var localStyles = StyleSheet.create({
  separator:{
    paddingTop: 10,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  touchableHighlight: {
      backgroundColor: 'white',
      marginBottom:10,
      marginLeft: parseInt((width-((parseInt(width/2)-20)*2))/3),

      shadowColor: '#ECECEC',
      shadowOffset: {
          width: 0,
          height: 0
      },
      shadowOpacity: 0.5,
      shadowRadius: 6,
      elevation: 5
  },
  row: {
      alignItems: 'center',
      backgroundColor: 'white',
      borderColor: '#ECECEC',
      borderWidth: 1,
      height: 205,
  },
  cellImage: {
      backgroundColor: 'white',
      height: 160,
      width : parseInt(width/2)-20
  },
  movieTitle: {
      fontSize: 15,
      fontWeight: '500',
      width:280,
      color: '#666'
  },
  movieRating: {
      color: 'white',
      fontFamily: 'Arial',
      marginLeft:10,
      marginRight:10
  },
  closeBtn:{
    position:'absolute',
    top: 3,
    right:5,
  },
  closeIcon:{
    color:'#baa071'
  },
  viewToDetail:{
    position: 'absolute',
    marginTop: 20,
    marginLeft: 10,
  },
  viewNum:{
    backgroundColor: '#78767B',
    height: 20,
    borderRadius: 3
  },
  textContainer:{
    position: 'absolute',
    marginTop: 0,
    paddingLeft: 10,
  },
  listView:{
      borderTopWidth: 1,
      borderColor:'#E7EAEC'
  },
});
module.exports = localStyles;
