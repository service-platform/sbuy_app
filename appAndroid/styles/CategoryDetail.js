var React = require('react-native');
var {
  StyleSheet,
  Dimensions
} = React;
let { width, height } = Dimensions.get('window');

var localStyles = StyleSheet.create({
  container: {

  },
  toolbar: {
    backgroundColor: '#F3F3F4',
    height: 55,
  },
  rowSeparator: {
      backgroundColor: '#f5f5f5',
      height: 1,
      marginLeft: 10,
      marginRight: 10
  },
  rowSeparatorHide: {
      opacity: 0.0
  },
  row: {
      alignItems: 'center',
      backgroundColor: 'white',
      flexDirection: 'column',
      margin: 15,
      borderWidth: 3,
      borderColor: '#DADADA'
  },
  image: {
    width: (width - 30 - 6),
    marginLeft: 2,
    height: 219
  },
  txtRealPrice:{
    color: 'red'
  },
  textContainer: {
    height: 30,
    width: (width - 30),
    paddingTop: 6,
    paddingLeft: 7,
    flexDirection: 'row',
  },
  textRealPrice: {
    backgroundColor: 'rgb(5,121,255)',
    borderRadius: 4,
    paddingTop: 1,
    marginTop: -2,
    height: 21,
    position: 'absolute',
    right: 10
  },
  textNum: {
    color: 'white',
    fontSize: 14,
    marginLeft: 10,
    marginRight: 10,
    textAlignVertical: 'center'
  }
});
module.exports = localStyles;
