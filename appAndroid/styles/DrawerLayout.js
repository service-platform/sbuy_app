var React = require('react-native');
var {
  StyleSheet,
  Dimensions
} = React;
let { width, height } = Dimensions.get('window');

var localStyles = StyleSheet.create({
  container: {
    paddingLeft: 10,
    paddingRight: 10,
    height: height,
    width: width,
    alignItems: 'center',
    backgroundColor: 'rgb(255,255,255)',
  },
  button: {
    borderWidth: 0,
    borderRadius: 30,
    alignSelf: 'center',
    justifyContent: 'center'
  }
});
module.exports = localStyles;
