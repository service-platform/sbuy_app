'use strict';
require ('react');
require ('./utils/Constant');
require ('./utils/General');
import * as Sqlite from './utils/Sqlite';
var React = require('react-native');
var TimerMixin = require('react-timer-mixin');
var strings = require('./utils/LocalizedStrings');
var {
    AppRegistry,
    Platform,
    BackAndroid,
    Navigator,
    Alert,
    ToastAndroid
} = React;

import store from './redux/store';
import { Provider } from 'react-redux';

import index from './pages/index';

var _navigator; var isExit = true;
var App = React.createClass({
  mixins: [TimerMixin],
  componentWillMount:function() {
      BackAndroid.addEventListener('hardwareBackPress', this.onBackAndroid);
  },
  componentWillUnmount:function() {
      BackAndroid.removeEventListener('hardwareBackPress', this.onBackAndroid);
  },
  onBackAndroid: () => {
    if (_navigator.getCurrentRoutes().length > JSON.backIndex) {
      window.GLOBAL.isOpenCameraView = false;
      _navigator.pop();
      return true;
    }else{
        if(isExit == true){
           var toast = ToastAndroid.show(strings.toastTipExit, ToastAndroid.SHORT);

           isExit = false;
           this.setTimeout(
               () => {
                 isExit = true;
               },
               1500
           );
           return true;
        }else{
           BackAndroid.exitApp();
        }
    }
  },
  initialRoute: {
    component: index,
    name: 'index'
  },
  configureScene: function() {
    if (Platform.OS === 'ios') {
      return Navigator.SceneConfigs.PushFromRight;
    }
    return Navigator.SceneConfigs.FloatFromBottomAndroid;
  },
  renderScene: function(route, navigator) {
    _navigator = navigator;
    const Component = route.component;
    return (
      <Component {...route.params}  navigator={_navigator}/>
    );
  },

  render: function() {
    return (
      <Provider store={store} key="provider">
        <Navigator
          initialRoute={this.initialRoute}
          configureScene={() => this.configureScene()}
          renderScene={(route, navigator) => this.renderScene(route, navigator)}
        />
      </Provider>
    );
  }
});

AppRegistry.registerComponent('SBuy', () => App);
