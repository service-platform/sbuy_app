package com.sbuy.mykit;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

//DeviceEventManagerModule 这里需要用到这么多导入包
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.react.bridge.WritableMap;  //下面 Key - Value
import com.facebook.react.bridge.Arguments;  //事件回调，传递参数 WritableMap
import javax.annotation.Nullable;

import java.util.*;
import java.lang.Boolean;
import android.content.Intent;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.content.Context;

public class ShakeReact extends ReactContextBaseJavaModule implements SensorEventListener {
  private ReactApplicationContext reactContext = null;
  private SensorManager sensorManager = null;
  private long first = 0;
  private long second = 0;

  public ShakeReact(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;

    sensorManager = (SensorManager)reactContext.getSystemService(Context.SENSOR_SERVICE);
  }

  @Override
  public String getName() {
    return "ShakeReact";
  }

  //关闭摇一摇监听
  @ReactMethod
  public void stopShake() {
      sensorManager.unregisterListener(this);
  }

  //启动摇一摇监听
  @ReactMethod
  public void startUpShake() {
      sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
  }

  @Override
  public void onSensorChanged(SensorEvent event)
  {
      int sensorType = event.sensor.getType();
      //values[0]:X轴，values[1]：Y轴，values[2]：Z轴
      float[] values = event.values;
      if (sensorType == Sensor.TYPE_ACCELEROMETER)
      {
          if ((Math.abs(values[0]) > 13 || Math.abs(values[1]) > 13 || Math.abs(values[2]) > 13))
          {
              if(first == 0){
                first = System.currentTimeMillis();
                WritableMap params = Arguments.createMap();
                // params.putString("Number", incomingNumber);
                this.sendEvent(this.reactContext, "ShakeEvent",params);
              }else{
                second = System.currentTimeMillis();
                if((second - first) > 1000){
                    first = second;
                    WritableMap params = Arguments.createMap();
                    // params.putString("Number", incomingNumber);
                    this.sendEvent(this.reactContext, "ShakeEvent",params);
                }
              }
          }
      }
  }

  @Override
  public void onAccuracyChanged(Sensor sensor, int accuracy)
  {
      //当传感器精度改变时回调该方法，Do nothing.
  }

  //向前端发起，Javascript 回调
  private void sendEvent(ReactContext reactContext,String eventName,@Nullable WritableMap params) {
      reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit(eventName, params);
  }
}
