package com.sbuy.mykit;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import javax.annotation.Nullable;

import com.facebook.react.bridge.Callback;

import java.util.*;
import java.lang.Boolean;
import android.content.Intent;

//解析二维码，依赖其它类库
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.util.Hashtable;
import android.text.TextUtils;
import com.sbuy.mykit.decoding.RGBLuminanceSource;

//解析二维码信息
import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;

public class QRFormPhotoReact extends ReactContextBaseJavaModule{
  private ReactApplicationContext reactContext = null;
  public Bitmap scanBitmap;

  public QRFormPhotoReact(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;
  }

  @Override
  public String getName() {
    return "QRFormPhotoReact";
  }

  @ReactMethod
  public void QRScanningImage(String path, Callback scanResult) {
      if (TextUtils.isEmpty(path)) {
          scanResult.invoke(4, "对不起，路径不能为空");
          return;
      }

      // DecodeHintType 和 EncodeHintType
      Hashtable<DecodeHintType, String> hints = new Hashtable<DecodeHintType, String>();
      hints.put(DecodeHintType.CHARACTER_SET, "utf-8"); // 设置二维码内容的编码
      BitmapFactory.Options options = new BitmapFactory.Options();
      options.inJustDecodeBounds = true; // 先获取原大小
      scanBitmap = BitmapFactory.decodeFile(path,options);

      options.inJustDecodeBounds = false; // 获取新的大小

      int sampleSize = (int) (options.outHeight / (float) 200);

      if (sampleSize <= 0){
          sampleSize = 1;
      }

      options.inSampleSize = sampleSize;
      scanBitmap = BitmapFactory.decodeFile(path, options);

      RGBLuminanceSource source = new RGBLuminanceSource(scanBitmap);
      BinaryBitmap bitmap1 = new BinaryBitmap(new HybridBinarizer(source));
      QRCodeReader reader = new QRCodeReader();

      try {
          Result result = reader.decode(bitmap1, hints);
          if (result == null) {
              scanResult.invoke(3, "二维码格式错误");
          }else{
              scanResult.invoke(0, result.toString());
          }
      } catch (NotFoundException e) {
          scanResult.invoke(1, "二维码格式错误");
      } catch (ChecksumException e) {
          scanResult.invoke(2, "校验异常");
      } catch (FormatException e) {
          scanResult.invoke(3, "二维码格式错误");
      }
      return;
  }
}
