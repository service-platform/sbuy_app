package com.sbuy;

import com.facebook.react.ReactActivity;
import com.facebook.react.ReactPackage;
import com.sbuy.MainReactPackage; //解决双击返回，ToastAndroid 显示的问题

import java.util.Arrays;
import java.util.List;
/*更多第三方扩展包（用于系统调用）*/
import com.eguma.barcodescanner.BarcodeScanner;  // <--- import
import cl.json.RNSharePackage;  // <--- import
import org.thequestionmark.rnsimplemenu.SimpleMenuPackage;  // <--- import
import com.remobile.imagePicker.*;  // <--- import
import com.remobile.camera.*;  // <--- import
import com.dieam.reactnativepushnotification.ReactNativePushNotificationPackage;  // <--- Import
import cn.reactnative.httpcache.HttpCachePackage;
import com.remobile.sqlite.*;  // <--- import
import com.sbuy.mykit.ShakeReactPackage;  // <--- import
import com.babisoft.ReactNativeLocalization.ReactNativeLocalizationPackage; // <--- import
//（react-native-vector-icons）字体图标
import com.oblador.vectoricons.VectorIconsPackage;
import com.alipay.AlipayPackage;  // <--- import
import com.sbuy.mykit.WeChatPackage;  // <--- import
//android 内部传值  类型
import android.content.Intent;

public class MainActivity extends ReactActivity {
	private RCTImagePickerPackage mImagePickerPackage; // <--- declare package
    private RCTCameraPackage mCameraPackage; // <--- declare package
    private ReactNativePushNotificationPackage mReactNativePushNotificationPackage; // <------ Add Package Variable
    private RCTSqlitePackage mSqlitePackage; // <--- declare package
		private AlipayPackage alipay;
		private WeChatPackage wechat;

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "SBuy";
    }

    /**
     * Returns whether dev mode should be enabled.
     * This enables e.g. the dev menu.
     */
    @Override
    protected boolean getUseDeveloperSupport() {
        return BuildConfig.DEBUG;
    }

    /**
     * A list of packages used by the app. If the app uses additional views
     * or modules besides the default ones, add more packages here.
     */
    @Override
    protected List<ReactPackage> getPackages() {
				mImagePickerPackage = new RCTImagePickerPackage(this);// <--- alloc package
			 	mCameraPackage = new RCTCameraPackage(this);// <--- alloc package
			 	mReactNativePushNotificationPackage = new ReactNativePushNotificationPackage(this); // <------ Initialize the Package
			 	mSqlitePackage = new RCTSqlitePackage(this);// <--- alloc package
				alipay = new AlipayPackage(this);
				wechat = new WeChatPackage();

        return Arrays.<ReactPackage>asList(
            new MainReactPackage(),

            new BarcodeScanner(),
		        new RNSharePackage(),
		        new SimpleMenuPackage(this),
		        mImagePickerPackage,
		        mCameraPackage,
		        new VectorIconsPackage(),
		        mReactNativePushNotificationPackage,
		        new HttpCachePackage(),
		        mSqlitePackage,
						alipay,
						wechat,
						new ReactNativeLocalizationPackage(),
            new ShakeReactPackage()
        );
    }

    // <----- add start
    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mImagePickerPackage.onActivityResult(requestCode, resultCode, data);
        mCameraPackage.onActivityResult(requestCode, resultCode, data);
    }
    // <----- add end

    // Add onNewIntent
    @Override
    protected void onNewIntent (Intent intent) {
       super.onNewIntent(intent);

       mReactNativePushNotificationPackage.newIntent(intent);
    }

    @Override
   protected  void onDestroy() {
       super.onDestroy();
       mSqlitePackage.onDestroy(); // <------ add here
   }
}
